echo "================================================"
echo "              Running Search page test cases .....          "
echo "================================================"
set PATH=%PATH%;C:\Users\ralugunti\apache-maven-3.8.4\bin
set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_281
echo "Using browser: " %1
echo "Using browser stack: " %2
echo "Using browser address: " %3
mvn test -Dtest="TestContactUsTCRunner"  -Dbrowser_name=edge -Dbrowser_stack=false -Dbrowser_address=prod