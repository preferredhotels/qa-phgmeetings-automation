echo "================================================"
echo "    Start Contact Us page test cases .....            "
echo "================================================"
set PATH=%PATH%;C:\Users\ralugunti\apache-maven-3.8.4\bin
set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_281
echo "================================================"
echo "Running Contact Us page test cases on Chrome browser.."
echo "================================================"
call runContactUsPage-Chrome.bat
echo "================================================"
echo "Running Contact Us page test cases on Edge browser...."
echo "================================================"
call runContactUsPage-Edge.bat
echo "================================================"
echo "            Contact Us Page Test Cases completed      "
echo "================================================"
