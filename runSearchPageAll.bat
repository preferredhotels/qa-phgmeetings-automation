echo "================================================"
echo "    Start Search page test cases .....            "
echo "================================================"
set PATH=%PATH%;C:\Users\ralugunti\apache-maven-3.8.4\bin
set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_281
echo "================================================"
echo "Running Search page test cases on Chrome browser.."
echo "================================================"
call runSearchPage-Chrome.bat
echo "================================================"
echo "Running Search page test cases on Edge browser...."
echo "================================================"
call runSearchPage-Edge.bat
echo "================================================"
echo "            Search Page Test Cases completed      "
echo "================================================"
