package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import pagefactory.TestBase;
import utils.GlobalUtils;

import java.io.IOException;

public class Hook extends TestBase {
    private TestBase base;

    public Hook(){

    }
    public Hook(TestBase base){
        this.base = base;
    }

    @Before
    public void beforeScenario(Scenario scenario) throws IOException {
        //System.out.println("init called before scenario: " + scenario.getName());
        GlobalUtils.hardWait(2000);
        openBrowser(scenario.getName());
    }

    @After
    public void afterScenario(Scenario scenario) {
        //System.out.println("after scenario method called: " + scenario.getName());
        if (base.driver != null) {
            GlobalUtils.hardWait(2000); // to view the last state of the page
            base.driver.close();
            //            try {
            //                base.getBsLocal().stop();
            //            } catch (Exception e) {
            //                System.out.println("error stopping local: " + e.getMessage());
            //            }
            base. driver.quit();
            GlobalUtils.hardWait(3000); // allow browser clean up  after quit
        }
    }

}
