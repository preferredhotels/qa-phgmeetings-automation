package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pagefactory.GlobalLinksPageFactory;
import pagefactory.SearchPageFactory;
import utils.GlobalUtils;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class SearchStepsPhg  {
    // pageFactories
    GlobalLinksPageFactory globalPage;
    SearchPageFactory searchPage;

    public SearchStepsPhg() {
        globalPage = new GlobalLinksPageFactory();
        searchPage = new SearchPageFactory();
    }

    @Given("user is on PHG Meetings page to begin search")
    public void user_is_on_phg_meetings_page_to_begin_search() {
        searchPage.verifyUserIsOnPhgMeetingsPage();
    }

    @And("click search link from header")
    public void click_search_link_from_header() {
//        searchPage.clickSearchLinkHeader();
        GlobalUtils.hardWait(500);
        globalPage.clickLink("Search");
    }

    @Then("search page is displayed")
    public void search_page_is_displayed() {
        try {
            GlobalUtils.hardWait(5000);
            searchPage.driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@class='btn-submit']")));
            assertTrue(searchPage.driver.findElement(By.xpath("//input[@class='btn-submit']")).isDisplayed());
        } catch (StaleElementReferenceException se) {
            GlobalUtils.hardWait(1000);
            assertTrue(searchPage.driver.findElement(By.xpath("//input[@class='btn-submit']")).isDisplayed());
        }
    }

    @Then("search page displays fields to enter search criteria")
    public void search_page_displays_fields_to_enter_search_criteria() {
        searchPage.enterSearchCriteria();
    }

    @When("click search button in keyword search section")
    public void click_search_button_in_keyword_search_section() {
        System.out.println("before search button click");
        GlobalUtils.hardWait(5000); // need to wait for the page to load, otherwise the following code will use the button on the previous page
        searchPage.driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@class='btn-submit']"))).click();
        GlobalUtils.hardWait(1000);
    }

    @When("^user enters (.*) location or hotelname in keyword search$")
    public void user_enters_hotelname_in_keyword_search(String locationorhotelname) {
        GlobalUtils.hardWait(2000);
        searchPage.enterSearchData(locationorhotelname);
        GlobalUtils.hardWait(2000);
    }

    @Then("^search results page has (.*) location or hotelname match$")
    public void search_results_page_has_location_or_hotelname_match(String locationorhotelname) {
        GlobalUtils.hardWait(1500);
        searchPage.verifySearchPageContainsMatch(locationorhotelname);
    }

    @When("^user selects (.*) number of rooms from guestrooms dropdown$")
    public void user_selects_number_of_rooms_from_guestrooms_dropdown(String numRooms) {
        GlobalUtils.hardWait(1000);
        searchPage.selectFromGuestRoomsDropDown(numRooms);
    }

    @When("^user selects (.*) room size from largest room dropdown$")
    public void user_selects_meeting_room_size_from_largest_room_dropdown(String roomSize) {
        GlobalUtils.hardWait(1000);
        searchPage.selectFromLargestRoomSizeDropDown(roomSize);
    }

    @When("^user selects (.*) from ceiling height dropdown$")
    public void user_selects_ceiling_height_from_celing_height_dropdown(String ceilingHeight) {
        GlobalUtils.hardWait(1000);
        searchPage.selectFromCeilingHeightDropDown(ceilingHeight);
    }

    @When("^user enters (.*) in amenity text search$")
    public void user_enters_amenity_in_text_search(String amenity) {
        GlobalUtils.hardWait(1000);
        searchPage.enterTextInAmenityTextField(amenity);
    }

    @Then("search results page is displayed")
    public void search_results_page_is_displayed() {
        searchPage.verifySearchResults();
    }

    @And("map results button is visible")
    public void map_results_button_is_visible() {
        //System.out.println("before verify map results is visible");
        GlobalUtils.hardWait(1000);
        searchPage.verifyMapResultsIsVisible();
    }

    @And("every property has a map link that is clickable")
    public void every_property_has_a_map_link_that_is_clickable() {
        assertTrue(searchPage.getPropertyMapLinkElements().size() > 0);
        List<WebElement> webElements = searchPage.getPropertyMapLinkElements();
        WebElement element = webElements.get(0);
        assertTrue(element.isEnabled());
    }

    @And("user clicks on map results button")
    public void user_clicks_on_map_results_button() {

    }

    @Then("map view is displayed")
    public void map_view_is_displayed() {

    }

    @And("user clicks on street map button")
    public void user_clicks_on_street_map_button() {

    }

    @Then("street map view is displayed")
    public void street_map_view_is_displayed() {

    }

    @And("user clicks on satellite button")
    public void user_clicks_on_satellite_button() {

    }

    @Then("satellite map view is displayed")
    public void satellite_map_view_is_displayed() {

    }

    @And("user clicks on View in Feet button")
    public void user_clicks_on_view_in_feet_button() {

    }

    @Then("properties are listed with details in feet")
    public void properties_are_listed_with_details_in_feet() {

    }

    @And("user click view in meters button in results section")
    public void user_click_view_in_meters_button() {
        searchPage.clickViewInMetersButtonInResults();
    }

    @Then("properties are listed with details in square meters")
    public void properties_are_listed_with_details_in_square_meters() {
        // loop thru the list of all the elements
        // get the first element text
        // search for single letter m and return true
        GlobalUtils.hardWait(1500);
        searchPage.verifyPropertyListedInSquareMeter();
    }

    @And("^user selects (.*) city from the dropdown$")
    public void user_selects_city_from_the_dropdown(String optionName) {
        // loop thru all the option elements of this select
        // select the optionValue and select it
        GlobalUtils.hardWait(1500);
        searchPage.selectSortOptionFromDropDown(optionName);
    }

    @And("^user selects (.*) country from the dropdown$")
    public void user_selects_country_from_the_dropdown(String optionName) {
        // loop thru all the option elements of this select
        // select the optionValue and select it
        GlobalUtils.hardWait(1500);
        searchPage.selectSortOptionFromDropDown(optionName);
    }

    @And("^user selects (.*) number of rooms high to low from the dropdown$")
    public void user_selects_number_of_rooms_from_the_dropdown(String optionName) {
        // loop thru all the option elements of this select
        // select the optionValue and select it
        GlobalUtils.hardWait(1500);
        searchPage.selectSortOptionFromDropDown(optionName);
    }

    @And("^user selects (.*) number of rooms low to high from the dropdown$")
    public void user_selects_number_of_rooms_low_to_high_from_the_dropdown(String optionName) {
        GlobalUtils.hardWait(1500);
        searchPage.selectSortOptionFromDropDown(optionName);
    }

    @And("^user selects (.*) largest meeting rooms high to low from the dropdown$")
    public void user_selects_largest_meeting_rooms_high_to_low_from_the_dropdown(String optionName) {
        GlobalUtils.hardWait(1500);
        searchPage.selectSortOptionFromDropDown(optionName);
    }

    @And("^user selects (.*) largest meeting rooms low to high from the dropdown$")
    public void user_selects_largest_meeting_rooms_low_to_high_from_the_dropdown(String optionName) {
        GlobalUtils.hardWait(1500);
        searchPage.selectSortOptionFromDropDown(optionName);
    }

    @And("^user selects (.*) best match from the dropdown$")
    public void user_selects_best_match_from_the_dropdown(String optionName) {
        GlobalUtils.hardWait(1500);
        searchPage.selectSortOptionFromDropDown(optionName);
    }

    @And("^user selects (.*) property from the dropdown$")
    public void user_selects_property_from_the_dropdown(String optionName) {
        GlobalUtils.hardWait(1500);
        searchPage.selectSortOptionFromDropDown(optionName);
    }

    @And("user clicks on the request proposal button")
    public void user_clicks_on_the_request_proposal_button() {
        // loop thru list of all the request proporal button
        // select the first one and click it
        GlobalUtils.hardWait(1500);
        searchPage.userClicksOnRequestProposalButton();
    }

    @Then("request proposal page is displayed")
    public void request_proposal_page_is_displayed() {
        GlobalUtils.hardWait(1000);
        searchPage.verifyRequestProposalPageDisplayed();
    }

    @And("^user selects (.*) feature type in refine search$")
    public void user_selects_feature_type_in_refine_search(String featureType) {
        GlobalUtils.hardWait(1500);
        searchPage.selectFeatureTypeFromRefineSearch(featureType);
    }

    @And("user selects {string} and {string} features in refine search")
    public void user_selects_multi_features_in_refine_search(String type1, String type2) {
        GlobalUtils.hardWait(1500);
        searchPage.selectMultipleFeatureTypeInRefineSearch(type1, type2);
    }

    @And("user selects number of guest rooms in refine search")
    public void user_selects_number_of_guest_rooms_in_refine_search() {
        GlobalUtils.hardWait(1500);
        searchPage.selectNumOfGuestRoomsInRefineSearch();
    }

    @And("user selects largest meeting room size in refine search")
    public void user_selects_largest_meeting_room_size_in_refine_search() {
        GlobalUtils.hardWait(1500);
        searchPage.selectLargestMeetingRoomInRefineSearch();
    }

    @And("user selects ceiling height in refine search")
    public void user_selects_ceiling_height_in_refine_search() {
        GlobalUtils.hardWait(1500);
        searchPage.selectCeilingHeightInRefineSearch();
    }

    @And("user selects region in refine search")
    public void user_selects_region_in_refine_search() {
        GlobalUtils.hardWait(1500);
        searchPage.selectRegionInRefineSearch();
    }
}
