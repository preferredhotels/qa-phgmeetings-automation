package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pagefactory.ContactUsPageFactory;
import pagefactory.GlobalLinksPageFactory;

import static org.junit.Assert.assertTrue;

public class ContactUsStepsPhg {
    // pageFactories
    GlobalLinksPageFactory globalPage;
    ContactUsPageFactory contactUsPage;


    public ContactUsStepsPhg(){
        globalPage = new GlobalLinksPageFactory();
        contactUsPage = new ContactUsPageFactory();
    }
    @Given("user is on PHG Meetings page to test contactus")
    public void user_is_on_phg_meetings_page() {
        contactUsPage.navigateToPage();

    }
    @Given("user clicks on contactus link  from header")
    public void user_clicks_on_contactus_link_from_header() {
        //System.out.println("User clicks ContactUs link");
        globalPage.clickLink("Contact Us");
    }

    @Then("contactus page is displayed")
    public void contactus_page_is_displayed() {
       // System.out.println("ContactUs page is displayed");
        contactUsPage.isPageDisplayed();
    }

    @And("leadership section is present")
    public void leadership_section_is_present() {
        //System.out.println("ContactUs page displays Leadership section");
        contactUsPage.isLeadershipSectionPresent();
    }

    @And("Sales Directors Europe is present")
    public void sales_directors_europe_is_present() {
        //System.out.println("ContactUs page displays Sales Directors Europe section");
        assertTrue("Sales Directors Europe section is displayed", contactUsPage.isSalesEuropeHeaderPresent());
    }

    @And("Sales Directors North America is present")
    public void sales_directors_north_america_is_present() {
        //System.out.println("ContactUs page displays Sales Directors North America section");
        assertTrue("Sales Directors North America section is displayed", contactUsPage.isSalesAmericaHeaderPresent());
    }

    @And("Sales Directors Asia Pacific Middle East is present")
    public void sales_directors_asia_pacific_middle_east_is_present() {
        //System.out.println("ContactUs page displays Sales Directors Asia Pacific Middle East section");
        assertTrue("Sales Directors Asia Pacific Middle East section is displayed", contactUsPage.isSalesAsiaPacificHeaderPresent());
    }

    @And("Group Sales Support Asia Pacific Europe & North America is present")
    public void group_sales_support_asia_pacific_europe_north_america_is_present() {
        //System.out.println("ContactUs page displays Group Sales Support Asia Pacific Europe & North America section");
        assertTrue("Group Sales Support Asia Pacific Europe & North America section is displayed", contactUsPage.isgroupSalesHeaderPresent());
    }

    @And("emails are present")
    public void emails_are_present() {

    }

    @And("phone numbers are present")
    public void phone_numbers_are_present() {

    }
}
