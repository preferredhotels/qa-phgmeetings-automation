package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pagefactory.GlobalLinksPageFactory;
import pagefactory.IPreferPlannerPageFactory;
import pagefactory.TestBase;

import static org.junit.Assert.assertTrue;

public class IPreferPlannerStepsPhg extends TestBase {
    // pageFactories
    GlobalLinksPageFactory globalPage;
    IPreferPlannerPageFactory iPreferPlannerPage;

    public IPreferPlannerStepsPhg() {
        globalPage = new GlobalLinksPageFactory();
        iPreferPlannerPage = new IPreferPlannerPageFactory();
    }

    @Given("user is on PHG Meetings page to test iPreferPlanner")
    public void user_is_on_phg_meetings_page_to_test_ipreferplanner() {
        iPreferPlannerPage.verifyUserIsOnPrefrPlannerPage();
    }

    @When("user clicks I prefer planner link from header")
    public void user_clicks_i_prefer_planner_link_from_header() {
       // System.out.println("User clicks IpreferPlanner link");
        globalPage.clickLink("I Prefer Planner");
    }

    @Then("I Prefer Planner page is displayed")
    public void i_prefer_planner_page_is_displayed() {
        iPreferPlannerPage.verifyPlannerPageIsDisplayed();
    }

    @And("I Prefer Planner page displays an image")
    public void i_prefer_planner_page_displays_an_image() {
        iPreferPlannerPage.verifyPlannerPageImageIsDisplayed();
    }

    @And("I Prefer Planner page displays rewards section")
    public void i_prefer_planner_page_displays_rewards_section() {
        //System.out.println("I PreferPlanner displays rewards section");
        assertTrue("Rewards section is displayed", iPreferPlannerPage.isRewardsSectionPresent());
    }

    @And("rewards section body text is displayed")
    public void rewards_section_body_text_is_displayed() {
        //System.out.println("I PreferPlanner displays rewards section body");
        assertTrue("Rewards body is displayed", iPreferPlannerPage.isRewardsBodyPresent());
    }

    @And("I Prefer Planner displays recognition section")
    public void i_prefer_planner_displays_recognition_section() {
        //System.out.println("I PreferPlanner displays recognition section");
        assertTrue("Recognition section is displayed", iPreferPlannerPage.isRecognitionSectionPresent());
    }

    @And("recognition section body text is displayed")
    public void recognition_section_body_text_is_displayed() {
        //System.out.println("I PreferPlanner displays recognition section body");
        assertTrue("Recognition body is displayed", iPreferPlannerPage.isRecognitionBodyPresent());
    }

    @And("I Prefer Planner displays membership section")
    public void i_prefer_planner_displays_membership_section() {
        //System.out.println("I PreferPlanner displays membership section");
        assertTrue("Membership section is displayed", iPreferPlannerPage.isMembershipSectionPresent());
    }

    @And("Membership section body text is displayed")
    public void membership_section_body_text_is_displayed() {
        //System.out.println("I PreferPlanner displays membership section body");
        assertTrue("Member body is displayed", iPreferPlannerPage.isMembershipBodyPresent());
    }
}
