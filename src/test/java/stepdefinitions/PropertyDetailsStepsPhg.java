package stepdefinitions;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pagefactory.*;
import utils.GlobalUtils;

import java.util.List;

public class PropertyDetailsStepsPhg extends TestBase {

    private String selectedHotelName;

    HomePageFactory homePage;
    GlobalLinksPageFactory globalPage;
    PropertyDetailsPageFactory pdp;
    NewsPageFactory newsPage;
    HotDatesPageFactory hotDatesPage;

    public PropertyDetailsStepsPhg() {
        homePage = new HomePageFactory();
        globalPage = new GlobalLinksPageFactory();
        pdp = new PropertyDetailsPageFactory();
        newsPage = new NewsPageFactory();
        hotDatesPage = new HotDatesPageFactory();
    }

    //Background
    @Given("user is on PHG Meetings page to view PDP")
    public void user_is_on_phg_meetings_page_to_view_pdp() {
        hotDatesPage.verifyUserIsOnPhgMeetingsPage();
    }

    @And("user clicks on hotdates link from header")
    public void user_clicks_on_hotdates_link_from_header() {
        GlobalUtils.hardWait(3500);
        globalPage.clickLink("Hot Dates");
    }

    private void getSelectedHotelName() {
        System.out.println("getSelectedHotelName");
        try {
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='card-title-container']/a/h3")));
            List<WebElement> elmts = driver.findElements(By.xpath("//div[@class='card-title-container']/a/h3"));
            selectedHotelName = elmts.get(0).getText();
        } catch (Exception e) {
            System.out.println("Unable to get the selected hotel name: " + e.getMessage());
        }
    }

    @Then("property details page is displayed")
    public void property_details_page_is_displayed() {
        GlobalUtils.hardWait(1500);
        pdp.verifyPageIsDisplayed();
    }


    //Scenario 1 :
    @When("user is on PHG Meetings property details page")
    public void user_is_on_phg_meetings_property_details_page() {
        pdp.verifyPageIsDisplayed();
    }

    @When("user is on property details page")
    public void user_is_on_property_details_page() {
        pdp.verifyPageIsDisplayed();
    }

    @Then("page contains hotel demographics")
    public void page_contains_hotel_demographics() {
        GlobalUtils.hardWait(1500);
        pdp.verifyHotelDemographics();
    }

    @And("image carousel is being displayed")
    public void image_carousel_is_being_displayed() {
        GlobalUtils.hardWait(1500);
        pdp.verifySlider();
    }

    @And("page contains at a glance section")
    public void page_contains_at_a_glance_section() {
        GlobalUtils.hardWait(1500);
        pdp.atAGlancesection();
    }

    @And("page contains hotel description")
    public void page_contains_hotel_description() {
        GlobalUtils.hardWait(1500);
        pdp.hotelDescriptionIsPresent();

    }

    @And("page displays airport proximity")
    public void page_displays_airport_proximity() {
        GlobalUtils.hardWait(1500);
        pdp.airportProximity();
    }

    @And("page displays social media links")
    public void page_displays_social_media_links() {
        GlobalUtils.hardWait(1500);
        pdp.verifySocialMediaLinks();
    }

    @And("page displays amenities")
    public void page_displays_amenities() {
        GlobalUtils.hardWait(1500);
        pdp.verifyAmenitiesIsPresent();
    }

    @And("page displays dining info")
    public void page_displays_dining_info() {
        GlobalUtils.hardWait(1500);
        pdp.displayDiningInfo();
    }

    @And("page displays meeting space info")
    public void page_displays_meeting_space_info() {
        GlobalUtils.hardWait(1500);
        pdp.displayMeetingingInfo();
    }

    @And("page displays latest news")
    public void page_displays_latest_news() {
        GlobalUtils.hardWait(1500);
        pdp.displayLatestNews();
    }

    @And("page displays footer links section")
    public void page_displays_footer_links() {
        GlobalUtils.hardWait(1500);
        pdp.displayFooterLinks();
    }

    @Then("hotel name is displayed in bold")
    public void hotel_name_is_displayed_in_bold() {
        pdp.verifyElementFontWeightIsBold();
    }

    @And("displays hotel address")
    public void displays_hotel_address() {
        GlobalUtils.hardWait(1500);
        pdp.verifyPropertyTitle();
        pdp.verifypropertyShortAddress();
        pdp.verifypropertyFullAddress();
    }

    @When("page displays request proposal link")
    public void page_displays_request_proposal_link() {
        GlobalUtils.hardWait(1500);
        pdp.verifyReqProposal();
    }

    @Then("contains a logo")
    public void contains_a_logo() {
        GlobalUtils.hardWait(1500);
        pdp.verifyLogoIsPresent();
    }

    @And("contains transportation section")
    public void contains_transportation_section() {
        GlobalUtils.hardWait(1500);
        pdp.verifyTransportIsPresent();
    }

    @And("social media logos or links")
    public void social_media_logos_or_links() {
        GlobalUtils.hardWait(1500);
        pdp.verifySocialMediaLinks();
    }

    @Then("contains three links")
    public void contains_three_links() {
        GlobalUtils.hardWait(2500);
        pdp.verifyPropertyCodeMenuElements();
    }

    @And("first sentence is bold")
    public void first_sentence_is_bold() {
        pdp.verifyElementFontWeightIsBold();
    }

    @And("page contains map view of hotel")
    public void page_contains_map_view_of_hotel() {
        GlobalUtils.hardWait(1500);
        pdp.verifyViewMapPresent();

    }

    @And("footer links displays logos")
    public void footer_links_displays_logos() {
        pdp.verifyFooterLogos();
    }

    @And("footer links displays address")
    public void footer_links_displays_address() {
        pdp.verifyFooterAddress();
    }

    @And("footer links displays phone number")
    public void footer_links_displays_phone_number() {
        pdp.verifyFooterPhoneNumber();
    }

    @And("footer links displays cookies and privacy link")
    public void footer_links_displays_cookies_and_privacy_link() {
        pdp.verifyFooterCookiesAndPrivacy();
    }

    @And("footer links displays profile editor link")
    public void footer_links_displays_profile_editor_link() {
        pdp.verifyProfileEditor();
    }

    @And("footer links displays personal info link")
    public void footer_links_displays_personal_info_link() {
        pdp.verifyPersonalInfo();
    }

// .................REQUEST PROPOSAL ..................
    @When("user clicks on request proposal button")
    public void user_clicks_on_request_proposal_button() {
        pdp.clickRequestProposal();

    }
    @Then("user lands on request proposal page")
    public void user_lands_on_request_proposal_page() {
        pdp.verifyReqProposalPage();
    }

    @And("hotel name is prepopulated")
    public void hotelNameIsPrepopulated() {
        pdp.hotelNamePrepopulated();
    }

    @And("user enters the form details and submits the form")
    public void user_enters_the_form_details_and_submits_the_form() {
        pdp.enterFormDetails();

    }
    @Then("displays thank you message")
    public void displays_thank_you_message() {
        pdp.displaysThankyouMessage();

    }
}