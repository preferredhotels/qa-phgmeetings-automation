package stepdefinitions;

import constants.PHGConstants;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pagefactory.*;
import utils.GlobalUtils;

import static org.junit.Assert.assertEquals;


public class HomeStepsPhg {

	HomePageFactory homePage;
	GlobalLinksPageFactory globalPage;
	SearchPageFactory searchPage;
	HotelInfoPageFactory hotelInfoPage;
	AdminPageFactory adminPage;

	public HomeStepsPhg(){
		globalPage = new GlobalLinksPageFactory();
		homePage = new HomePageFactory();
		searchPage = new SearchPageFactory();
		adminPage = new AdminPageFactory();
		hotelInfoPage = new HotelInfoPageFactory();
	}
	
	@Given("user is on PHG Meetings page to test home page")
	public void user_is_on_phg_meetings_page() {
		GlobalUtils.hardWait(1500);
	    globalPage.navigateToPage();
	}
	
	@And("user clicks home link from header")
	public void user_clicks_home_link_from_header() {
		GlobalUtils.hardWait(500);
		globalPage.clickLink("Home");
	}
	
	@Then("home page is displayed")
	public void home_page_is_displayed() {
		GlobalUtils.hardWait(1500);
	   homePage.isPageDisplayed();
    }
	
	@When("user clicks on search button")
	public void user_clicks_on_search_button() {
		//System.out.println("user clicked on search button");
		GlobalUtils.hardWait(1500);
		homePage.clickSearchButton();
	}

	@And("user clicks on search button with hotelname")
	public void user_clicks_on_search_button_with_hotelname() {
		//System.out.println("user clicked on search button with hotel name");
		GlobalUtils.hardWait(5000);
		homePage.clickSearchButton();
	}

	@And("user clicks on search button for guest rooms")
	public void user_clicks_on_search_button_for_guest_rooms() {
		//System.out.println("user clicked on search button for guest rooms");
		GlobalUtils.hardWait(1500);
		homePage.clickSearchButton();
	}

	@And("user clicks on search button for room size")
	public void user_clicks_on_search_button_for_room_size() {
		//System.out.println("user clicked on search button for room size");
		GlobalUtils.hardWait(1500);
		homePage.clickSearchButton();
	}

	@And("user clicks on search button for room in meters")
	public void user_clicks_on_search_button_for_room_in_meters() {
		//System.out.println("user clicked on search button for room in meters");
		GlobalUtils.hardWait(1500);
		homePage.clickSearchButton();
	}

	@Then("search results is displayed")
	public void search_results_is_displayed() throws InterruptedException {
		GlobalUtils.hardWait(3500);
	   searchPage.verifySearchResults();
	}

	@Then("^search results with (.*) is displayed$")
	public void search_results_with_hotelname_is_displayed(String hotelName) throws InterruptedException {
		GlobalUtils.hardWait(2500);
		searchPage.verifySearchResultsByHotelName(hotelName);
	}

	@Then("^minimum guest rooms results with text (.*) and (.*) is displayed$")
	public void minimum_guest_rooms_results_with_text_is_displayed(String searchType, String numberofrooms) throws InterruptedException {
		GlobalUtils.hardWait(2500);
		searchPage.verifySearchResultsBySearchTypeAndNumberOfRooms(searchType, numberofrooms);
	}

	@Then("^largest meeting room results with text (.*) and (.*) is displayed$")
	public void largest_meeting_room_results_with_text_is_displayed(String searchType, String roomSize) throws InterruptedException {
		GlobalUtils.hardWait(2500);
		searchPage.verifySearchResultsBySearchTypeAndRoomSize(searchType, roomSize);
	}

	@Then("^largest meeting room in meters with text (.*) and (.*) is displayed$")
	public void largest_meeting_room_results_in_meters_with_text_is_displayed(String searchType, String roomSize) throws InterruptedException {
		GlobalUtils.hardWait(2500);
		searchPage.verifySearchResultsBySearchTypeAndRoomSize(searchType, roomSize);
	}

	@When("^user enters (.*) to search$")
	public void user_enters_hotelname_to_search(String hotelName) {
		GlobalUtils.hardWait(1500);
	   homePage.enterSearchData(hotelName);
	}
	
	@When("^user chooses (.*) in Guest Rooms list box$")
	public void user_chooses_numberofrooms_in_guest_rooms_list_box(String numRooms) {
	  // System.out.println("user chooses number of rooms: " + numRooms);
		homePage.pickOptionBySelectDropDown(numRooms, homePage.getNumberOfRoomsElement());
	}
	
	@When("^user chooses (.*) in meeting room list box$")
	public void user_chooses_squarefootage_in_meeting_room_list_box(String squareFootage) {
		System.out.println("user chooses squarefootage: " + squareFootage);
		homePage.pickOptionBySelectDropDown(squareFootage, homePage.getLargestRoomsElement());
	}
	
	@When("user clicks on switch to meters link")
	public void user_clicks_on_switch_to_meters_link() {
	   homePage.clickSwitchToMetersLink();
	}
	
	@And("^user selects (.*) in meeting room list box$")
	public void user_selects_squaremeters_in_meeting_room_list_box(String squareMeters) {
		System.out.println("user chooses squareMeters: " + squareMeters);
		homePage.pickOptionBySelectDropDown(squareMeters, homePage.getLargestRoomsElement());
	}
	
	@When("user search by map feature is visible")
	public void user_search_by_map_feature_is_visible() {
		homePage.getSearchByMapElement().isDisplayed();
	}
	
	@Then("user clicks on search by map")
	public void user_clicks_on_search_by_map() {
	    homePage.clickSearchByMap();
	}
	
	@And("verify all the world map images are displayed")
	public void verify_all_the_world_map_images_are_displayed() {
	    homePage.allMapsAvailable();
	}
	
	@And("^user clicks on the (.*) image")
	public void user_clicks_on_the_usa_map(String mapName) {
		homePage.clickOnImage(mapName);
	}

	@Then("^search results by (.*) is displayed")
	public void search_results_by_selected_map_is_displayed(String mapName) {
		searchPage.verifySearchResultsBySelectedMap(mapName);
	}

	@Then("verify hero hotel image is displayed")
	public void verify_hero_hotel_image_is_displayed() {
	   assertEquals(homePage.checkValidHeroImageExists(), true);
	}
	
	@When("user clicks the hero hotel image")
	public void user_clicks_the_hero_hotel_image() {
		homePage.findHeroHotelTitle();
		GlobalUtils.hardWait(4000);
		homePage.clickHeroImage();

	}
	
	@Then("verify hotel information displayed matches hero hotel")
	public void verify_hotel_information_displayed_matches_hero_hotel() {

		hotelInfoPage.verifyHotelInfoDisplayed(homePage.getHeroHotelTitle());
	}
	
	@And("verify three preferred property images are displayed")
	public void verify_three_preferred_property_images_are_displayed() {
		//div[@class="featured_item"]
		assertEquals(homePage.getFeaturedPropertyCount(), 3);
	}
	
	@And("verify the images are not broken")
	public void verify_the_images_are_not_broken() {
	    assertEquals(homePage.isFeaturedPropertyImageValid(0), true);
	    assertEquals(homePage.isFeaturedPropertyImageValid(1), true);
	    assertEquals(homePage.isFeaturedPropertyImageValid(2), true);
	}
	
	@And("user clicks on preferred hotel image")
	public void user_clicks_on_preferred_hotel_image() {
		GlobalUtils.hardWait(1500);
		homePage.findFeaturedPropertyTitle(0);
		homePage.getFeaturedPropertyElement(0).click();    
	}
	
	@And("verify hotel information displayed matches preferred hotel")
	public void verify_hotel_information_displayed_matches_preferred_hotel() {
		GlobalUtils.hardWait(1500);
		hotelInfoPage.verifyPreferredHotelMatches(homePage.mFeaturedHotelTitle.toLowerCase());
	}
	
	@And("verify top destinations is displayed")
	public void verify_top_destinations_is_displayed() {
		GlobalUtils.hardWait(1000);

		assertEquals(homePage.getTopDestinationsElement().isDisplayed(), true);
	}
	
	@When("^user clicks on (.*) as top destination$")
	public void user_clicks_on_destinationcity_as_top_destination(String destinationcity) {
		GlobalUtils.hardWait(1000);

		homePage.clickOnSelectedTopDestination(destinationcity);
	}
	
	@Then("verify preferred hotels in top destination is displayed")
	public void verify_preferred_hotels_in_top_destination_is_displayed() {
	  searchPage.verifyHotelTopDestIsDisplayed(homePage.mTopDestinationCity.toLowerCase());
	}

	@When("user clicks on preferred meetings link")
	public void user_clicks_on_preferred_meetings_link() {
	   
	    
	}
	
	@Then("verify the web page link display phgmeetings")
	public void verify_the_web_page_link_display_phgmeetings() {
	   assertEquals(homePage.getHomePageURL().toLowerCase(), PHGConstants.getInstance().getPageAddress());
	}

	@When("user clicks on home link on header")
	public void user_clicks_on_home_link_on_header() {
		globalPage.clickLink("Home");
	}
	
	@Then("welcome section is displayed")
	public void welcome_section_is_displayed() {
		homePage.isWelcomeSectionDisplayed();
	}
	
	@When("user clicks on the profile editor link")
	public void user_clicks_on_the_profile_editor_link() {
	   homePage.clickProfileEditorLink();
	}
	
	@Then("verify admin login page is displayed")
	public void verify_admin_login_page_is_displayed() throws InterruptedException {
		homePage.verifyAdminLoginPageDisplayed();
	}

	@When("user clicks on preferred meetings link on top of page")
	public void user_clicks_on_preferred_meetings_link_on_top_of_page() {
		homePage.clickPreferredMeetingsLink();
	}

}
