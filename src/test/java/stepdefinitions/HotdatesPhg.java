package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pagefactory.GlobalLinksPageFactory;
import pagefactory.HomePageFactory;
import pagefactory.HotDatesPageFactory;
import pagefactory.TestBase;
import utils.GlobalUtils;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class HotdatesPhg extends TestBase {

    /*
    //div[@class="card search-card"]
     */



    HomePageFactory homePage;
    GlobalLinksPageFactory globalPage;
    HotDatesPageFactory hotDatesPage;
    public HotdatesPhg() {
        homePage = new HomePageFactory();
        globalPage = new GlobalLinksPageFactory();
        hotDatesPage = new HotDatesPageFactory();
    }

    @Given("user is on PHG Meetings page to view hotdates")
    public void user_is_on_phg_meetings_page_to_view_hotdates() {
        hotDatesPage.verifyUserIsOnPhgMeetingsPage();
    }

    @And("user clicks hotdates link from header")
    public void user_clicks_hotdates_link_from_header() {
        GlobalUtils.hardWait(1500);
        globalPage.clickLink("Hot Dates");
    }

    @Then("hotdates page is displayed")
    public void hotdates_page_is_displayed() {
        GlobalUtils.hardWait(1500);
        hotDatesPage.verifyPageIsDisplayed();
    }

    @And("verify hotdates property results is displayed")
    public void verify_hotdates_property_results_is_displayed() {
        GlobalUtils.hardWait(1500);
        hotDatesPage.verifyHotdatesResults();
    }

    @And("hotdates page displays region dropdown")
    public void hotdatesPageDisplaysRegionDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='region']", hotDatesPage.driverWait));
    }

    @And("hotdates page displays country dropdown")
    public void hotdatesPageDisplaysCountryDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='country']", hotDatesPage.driverWait));
    }

    @And("hotdates page displays state dropdown")
    public void hotdatesPageDisplaysStateDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='state']", hotDatesPage.driverWait));
    }

    @And("hotdates page displays city dropdown")
    public void hotdatesPageDisplaysCityDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='city']", hotDatesPage.driverWait));
    }
    @And("hotdates page displays hotel dropdown")
    public void hotdatesPageDisplaysHotelDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='hotel']", hotDatesPage.driverWait));
    }

    @And("hotdates page displays date dropdown")
    public void hotdatesPageDisplaysDateDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='date']", hotDatesPage.driverWait));
    }

    @And("hotdates page displays sort dropdown")
    public void hotdatesPageDisplaysSortDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='sort']", hotDatesPage.driverWait));
    }

    @And("hotdates page displays search text box")
    public void hotdatesPageDisplaysSearchTextBox() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//input[@class='hot-dates-container-search']", hotDatesPage.driverWait));
    }

    @And("^select (.*) region from region dropdown$")
    public void select_value_from_region_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        hotDatesPage.selectValueFromDropDown(hotDatesPage.getRegionDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("^select (.*) country from country dropdown$")
    public void select_value_from_country_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        hotDatesPage.selectValueFromDropDown(hotDatesPage.getCountryDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("^select (.*) state from state dropdown$")
    public void select_value_from_state_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        hotDatesPage.selectValueFromDropDown(hotDatesPage.getStateDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("^select (.*) city from city dropdown$")
    public void select_value_from_city_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        hotDatesPage.selectValueFromDropDown(hotDatesPage.getCityDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("^select (.*) hotelname from hotelname dropdown$")
    public void select_value_from_hotelname_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        hotDatesPage.selectValueFromDropDown(hotDatesPage.getHotelDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("^select (.*) date from date dropdown$")
    public void select_value_from_date_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        hotDatesPage.selectValueFromDropDown(hotDatesPage.getDateDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("^enter (.*) freetext to filter hotdates$")
    public void enter_freetext_to_filter_hotdates(String mValue) {
        GlobalUtils.hardWait(1000);
        hotDatesPage.getSearchField().sendKeys(mValue);
        GlobalUtils.hardWait(1000);
        hotDatesPage.getSearchField().sendKeys(Keys.ENTER);
    }

    @And("^sort hotdates by (.*) value$")
    public void select_value_from_sort_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        hotDatesPage.selectValueFromDropDown(hotDatesPage.getSortDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("link to property details is displayed")
    public void link_to_property_details_is_displayed() {
        System.out.println("link to property details is displayed");
        GlobalUtils.hardWait(2000);
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='propert-details-link' and contains(text(), 'Property Details')]")));
            List<WebElement> elementList =  driver.findElements(By.xpath("//div[@class='propert-details-link' and contains(text(), 'Property Details')]"));
            assertTrue(elementList.get(0).isDisplayed());
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the property details link: " + se.getMessage());
        }
    }

    @And("link to property name is displayed")
    public void link_to_property_name_is_displayed() {
        System.out.println("link to property name is displayed");
        GlobalUtils.hardWait(2000);
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='card-title-container']/a")));
            List<WebElement> elementList =  driver.findElements(By.xpath("//div[@class='card-title-container']/a"));
            assertTrue(elementList.get(0).isDisplayed());
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the property name link: " + se.getMessage());
        }
    }

    @And("link to property image is displayed")
    public void link_to_property_image_is_displayed() {
        System.out.println("link to property image is displayed");
        GlobalUtils.hardWait(1000);
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='card-horizontal']/a")));
            List<WebElement> elementList =  driver.findElements(By.xpath("//div[@class='card-horizontal']/a"));
            assertTrue(elementList.get(0).isDisplayed());
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the property image link: " + se.getMessage());
        }
    }

    @And("user selects property details link")
    public void user_selects_property_details_link() {
        System.out.println("link to property image is displayed");
       // getSelectedHotelName();
        GlobalUtils.hardWait(1000);
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='propert-details-link' and contains(text(), 'Property Details')]")));
            List<WebElement> elementList =  driver.findElements(By.xpath("//div[@class='propert-details-link' and contains(text(), 'Property Details')]"));
            int randomOption = getRandomOption(0, elementList.size());
            if(randomOption == elementList.size())
                randomOption = randomOption-1;
            WebElement childElement = elementList.get(randomOption);
            WebElement parent = (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].parentNode;", childElement);
            List<WebElement> elmts = driver.findElements(By.xpath("//div[@class='card-title-container']/a/h3"));
            selectedHotelName = elmts.get(randomOption).getText();
            parent.click();
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the property image link: " + se.getMessage());
        }
    }

    @And("user selects property name link")
    public void user_selects_property_name_link() {
        System.out.println("select property name link");
        getSelectedHotelName();
        GlobalUtils.hardWait(1000);
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='card-title-container']/a")));
            List<WebElement> elementList =  driver.findElements(By.xpath("//div[@class='card-title-container']/a"));
            elementList.get(0).click();
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the property name link: " + se.getMessage());
        }
    }

    @And("user selects property image link")
    public void user_selects_property_image_link() {
        System.out.println("select property image link");
        GlobalUtils.hardWait(1000);
        getSelectedHotelName();
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='card-horizontal']/a")));
            List<WebElement> elementList =  driver.findElements(By.xpath("//div[@class='card-horizontal']/a"));
            elementList.get(0).click();
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to click the property image link: " + se.getMessage());
        }
    }

    private void getSelectedHotelName() {
        System.out.println("getSelectedHotelName");
        try {
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='card-title-container']/a/h3")));
            List<WebElement> elmts = driver.findElements(By.xpath("//div[@class='card-title-container']/a/h3"));
            selectedHotelName = elmts.get(0).getText();
        } catch (Exception e ) {
            System.out.println("Unable to get the selected hotel name: " + e.getMessage());
        }
    }

    @Then("hotdates property details page is displayed")
    public void hotdates_property_details_page_is_displayed() {
        //div[@class='card-title-container']/a/h3
        GlobalUtils.hardWait(1500);
        try {
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='property-title-containter']/div/h1")));
            WebElement element =  driver.findElement(By.xpath("//div[@class='property-title-containter']/div/h1"));

            System.out.println("selected hotel / actual hotel: " + element.getText() + " / " + selectedHotelName);
            assertTrue(isElementExist(element));
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the property name link: " + se.getMessage());
        }
    }
}
