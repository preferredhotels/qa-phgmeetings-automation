package stepdefinitions;

import constants.PHGConstants;
import io.cucumber.java.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import pagefactory.*;
import utils.GlobalUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NewsStepsPhg extends TestBase {
    // pageFactories
    HomePageFactory homePage;
    GlobalLinksPageFactory globalPage;
    NewsPageFactory newsPage;

    public NewsStepsPhg() {
        globalPage = new GlobalLinksPageFactory();
        homePage = new HomePageFactory();
        newsPage = new NewsPageFactory();
    }

    @Given("user is on PHG Meetings page to view hotel news")
    public void user_is_on_phg_meetings_page_to_view_hotel_news() {
        newsPage.verifyUserIsOnPhgMeetingsPage();
    }

    @And("user clicks on news link from header")
    public void user_clicks_news_link_from_header() {
        GlobalUtils.hardWait(1500);
        globalPage.clickLink("News");
    }

    @Then("news page is displayed")
    public void news_page_is_displayed() {
        GlobalUtils.hardWait(1500);
        newsPage.verifyPageIsDisplayed();
    }

    @And("news page displays date filter")
    public void newsPageDisplaysDateFilter() {
        GlobalUtils.hardWait(1500);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='date']", newsPage.driverWait));
    }

    @And("news page displays hotel filter")
    public void newsPageDisplaysHotelFilter() {
        GlobalUtils.hardWait(1500);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='hotel']", newsPage.driverWait));
    }

    @And("news page displays search text field")
    public void newsPageDisplaysSearchTextField() {
        GlobalUtils.hardWait(1500);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//input[@class='news-container-search']", newsPage.driverWait));
    }

    @And("news page displays hotel image")
    public void pageDisplaysImagesOfHotel() {
        GlobalUtils.hardWait(1500);
        newsPage.verifyNewsPageDisplaysHotelImage();
    }

    @And("news page displays hotel news headings")
    public void pageDisplaysHeadingsOfThePage() {
        GlobalUtils.hardWait(1500);
        newsPage.verifyNewsPageDisplaysHotelNewsHeading();
    }

    @And("news page displays link to the hotel news")
    public void pageDisplaysLinkToTheHotelPage() {
        GlobalUtils.hardWait(1500);
        newsPage.verifyNewsPageDisplaysHotelLink();
    }

    @When("user selects option in the date filter dropdown")
    public void selectDateFilterOption() {
        GlobalUtils.hardWait(1500);
        assertTrue(newsPage.selectFilterByDateDropDown());
    }

    @When("user selects option in the hotel filter dropdown")
    public void selectHotelFilterOption() {
        GlobalUtils.hardWait(1500);
        assertTrue(newsPage.selectFitlerByHotelDropDown());
    }

    @Then("news page displays results by date")
    public void newsPageDisplaysResultsByDate() {
        GlobalUtils.hardWait(1500);
        newsPage.verifySearchResults();
    }

    @Then("news page displays results by hotel")
    public void newsPageDisplaysResultsByHotel() {
        GlobalUtils.hardWait(1500);
        newsPage.verifySearchResults();
    }

    @When("user enters search text in search box")
    public void userEntersSearchTextInSearchBox() {
        GlobalUtils.hardWait(3500);
        newsPage.enterSearchText("hotel"); // enter generic work hotel
    }

    @Then("news page displays results by search text")
    public void newsPageDisplaysResultsBySearchText() {
        GlobalUtils.hardWait(1500);
        newsPage.verifySearchResults();
    }

    @When("^user chooses (.*) last six months from date filter$")
    public void userSelectsSixMonthsFromDateFilter(String selectDate) {
        GlobalUtils.hardWait(2500);
        newsPage.chooseSelectedDate(selectDate);
    }

    @And("^user chooses (.*) from hotel filter$")
    public void userSelectsBostonHarborFromHotelFilter(String selectHotel) {
        GlobalUtils.hardWait(1500);
        newsPage.chooseSelectedHotel(selectHotel);
    }

    @Then("displays hotel news")
    public void displaysHotelNews() {
        GlobalUtils.hardWait(1500);
        newsPage.verifySearchResults();
    }

    @When("user selects link to the hotel news")
    public void userSelectsLinkToTheHotelNews() {
        GlobalUtils.hardWait(1500);
        newsPage.clickLinkToHotelNews();
    }

    @Then("property page is displayed with news section")
    public void propertyPageIsDisplayedWithNewsSection() {
        GlobalUtils.hardWait(2000);
        newsPage.verifyNewsSectionDisplayedInPropertyPage();
    }


}

