package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pagefactory.AboutUsPageFactory;
import pagefactory.GlobalLinksPageFactory;
import pagefactory.TestBase;
import utils.GlobalUtils;

import static org.junit.Assert.assertTrue;

public class AboutUsStepsPhg {
    // pageFactories
    GlobalLinksPageFactory globalPage;
    AboutUsPageFactory aboutusPage;
    private TestBase base;
    public AboutUsStepsPhg(){
        globalPage = new GlobalLinksPageFactory();
        aboutusPage = new AboutUsPageFactory();
    }

    public AboutUsStepsPhg(TestBase base){
        this.base=base;
    }

    @Given("user is on PHG Meetings page to test aboutus")
    public void user_is_on_phg_meetings_page() {
      globalPage.navigateToPage();
    }

    @And("user clicks AboutUs link from header")
    public void user_clicks_about_us_link_from_header() {
        //System.out.println("User clicks AboutUs link");
        GlobalUtils.hardWait(500);
        globalPage.clickLink("About Us");
    }

    @Then("about us page is displayed")
    public void about_us_page_is_displayed() {
        GlobalUtils.hardWait(500);
        aboutusPage.isPageDisplayed();
    }

    @And("aboutus header is displayed")
    public void aboutus_header_is_displayed() {
        //System.out.println("Aboutus page displays header text");
        assertTrue("AboutUs page header is displayed", aboutusPage.isAboutusHeaderPresent());

    }

    @And("preferred hotels logo and text is displayed")
    public void preferred_hotels_logo_and_text_is_displayed() {
        //System.out.println("Aboutus page displays preferred hotels logo and text");
        assertTrue("preferred hotels logo and text is displayed", aboutusPage.isPHRSectionPresent());

    }

    @And("legend logo and text is displayed")
    public void legend_logo_and_text_is_displayed() {
        //System.out.println("Aboutus page displays Legend logo and text");
        assertTrue("Legend hotels logo and text is displayed", aboutusPage.isLegendSectionPresent());

    }

    @And("lvx logo and text is displayed")
    public void lvx_logo_and_text_is_displayed() {
        //System.out.println("Aboutus page displays LVX logo and text");
        assertTrue("LVX hotels logo and text is displayed", aboutusPage.isLVXSectionPresent());

    }

    @And("lifestyle logo and text is displayed")
    public void lifestyle_logo_and_text_is_displayed() {
        //System.out.println("Aboutus page displays Lifestyle logo and text");
        assertTrue("Lifestyle hotels logo and text is displayed", aboutusPage.isLifestyleSectionPresent());

    }

    @And("connect logo and text is displayed")
    public void connect_logo_and_text_is_displayed() {
        //System.out.println("Aboutus page displays Connect logo and text");
        assertTrue("Connect hotels logo and text is displayed", aboutusPage.isConnectSectionPresent());

    }

    @And("residences logo and text is displayed")
    public void residences_logo_and_text_is_displayed() {
        //System.out.println("Aboutus page displays Residences logo and text");
        assertTrue("Residences hotels logo and text is displayed", aboutusPage.isResidenceLogoSectionPresent());

    }

}
