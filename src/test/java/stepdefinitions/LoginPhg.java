package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pagefactory.TestBase;

public class LoginPhg extends TestBase {
    private TestBase base;

    public LoginPhg(TestBase base) {
        this.base = base;
    }

    public LoginPhg() {

    }

    @And("user is on the login page for PHG Meetings")
    public void user_is_on_the_login_page_for_phg_meetings() {
    }

    @Then("login page has email textbox")
    public void login_page_has_email_textbox() {

    }

    @Then("login page has password textbox")
    public void login_page_has_password_textbox() {

    }

    @Then("login page has optional remember me checkbox")
    public void login_page_has_optional_remember_me_checkbox() {

    }

    @Then("Submit button is displayed")
    public void submit_button_is_displayed() {

    }

    @Then("forgot Password Link is displayed")
    public void forgot_password_link_is_displayed() {

    }

    @When("user enters ralugunti@preferredhotels")
    public void user_enters_ralugunti_preferredhotels() {

    }

    @When("user enters xxxxxx")
    public void user_enters_xxxxxx() {

    }

    @When("user clicks on the Login button")
    public void user_clicks_on_the_login_button() {

    }

    @Then("I verify the success in step")
    public void i_verify_the_success_in_step() {

    }

    @When("user enters admin2@phgdev.com")
    public void user_enters_admin2_phgdev_com() {

    }

    @When("user enters {string} with {string}")
    public void user_enters_with(String string, String string2) {

    }

    @Then("error message displayed")
    public void error_message_displayed() {

    }

    @Then("user returns back on login page")
    public void user_returns_back_on_login_page() {

    }

    @When("user clicks on forgot passsword link")
    public void user_clicks_on_forgot_passsword_link() {

    }

    @Then("user is redirected to the retrieve password page")
    public void user_is_redirected_to_the_retrieve_password_page() {

    }

}
