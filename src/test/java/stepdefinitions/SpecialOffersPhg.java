package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pagefactory.*;
import utils.GlobalUtils;

import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class SpecialOffersPhg extends TestBase {

    /*
    //div[@class="card search-card"]
     */

    private String selectedHotelName;

    HomePageFactory homePage;
    GlobalLinksPageFactory globalPage;
    SpecialOffersPageFactory specialOffersPage;
    public SpecialOffersPhg() {
        homePage = new HomePageFactory();
        globalPage = new GlobalLinksPageFactory();
        specialOffersPage = new SpecialOffersPageFactory();
    }

    @Given("user is on PHG Meetings page to view specialoffers")
    public void user_is_on_phg_meetings_page_to_view_specialoffers() {
        specialOffersPage.verifyUserIsOnPhgMeetingsPage();
    }

    @And("user clicks specialoffers link from header")
    public void user_clicks_specialoffers_link_from_header() {
        GlobalUtils.hardWait(1500);
        globalPage.clickLink("Special Offers");
    }

    @Then("specialoffers page is displayed")
    public void specialoffers_page_is_displayed() {
        GlobalUtils.hardWait(1500);
        specialOffersPage.verifyPageIsDisplayed();
    }

    @And("verify specialoffers property results is displayed")
    public void verify_specialoffers_property_results_is_displayed() {
        GlobalUtils.hardWait(1500);
        specialOffersPage.verifySpecialOffersResults();
    }

    @And("specialoffers page displays region dropdown")
    public void specialOffersPageDisplaysRegionDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='region']", specialOffersPage.driverWait));
    }

    @And("specialoffers page displays country dropdown")
    public void specialOffersPageDisplaysCountryDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='country']", specialOffersPage.driverWait));
    }

    @And("specialoffers page displays state dropdown")
    public void specialOffersPageDisplaysStateDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='state']", specialOffersPage.driverWait));
    }

    @And("specialoffers page displays city dropdown")
    public void specialOffersPageDisplaysCityDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='city']", specialOffersPage.driverWait));
    }
    @And("specialoffers page displays hotel dropdown")
    public void specialOffersPageDisplaysHotelDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='hotel']", specialOffersPage.driverWait));
    }

    @And("specialoffers page displays date dropdown")
    public void specialOffersPageDisplaysDateDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='date']", specialOffersPage.driverWait));
    }

    @And("specialoffers page displays sort dropdown")
    public void specialOffersPageDisplaysSortDropdown() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//select[@id='sort']", specialOffersPage.driverWait));
    }

    @And("specialoffers page displays search text box")
    public void specialOffersPageDisplaysSearchTextBox() {
        GlobalUtils.hardWait(1000);
        assertTrue(GlobalUtils.verifyDropdownElementExists("//input[@class='hot-dates-container-search']", specialOffersPage.driverWait));
    }

    @And("^select (.*) region from specialoffers region dropdown$")
    public void select_value_from_region_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        specialOffersPage.selectValueFromDropDown(specialOffersPage.getRegionDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("^select (.*) country from specialoffers country dropdown$")
    public void select_value_from_country_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        specialOffersPage.selectValueFromDropDown(specialOffersPage.getCountryDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("^select (.*) state from specialoffers state dropdown$")
    public void select_value_from_state_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        specialOffersPage.selectValueFromDropDown(specialOffersPage.getStateDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("^select (.*) city from specialoffers city dropdown$")
    public void select_value_from_city_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        specialOffersPage.selectValueFromDropDown(specialOffersPage.getCityDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("^select (.*) hotelname from specialoffers hotelname dropdown$")
    public void select_value_from_hotelname_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        specialOffersPage.selectValueFromDropDown(specialOffersPage.getHotelDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("^select (.*) date from specialoffers date dropdown$")
    public void select_value_from_date_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        specialOffersPage.selectValueFromDropDown(specialOffersPage.getDateDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("^enter (.*) freetext to filter specialoffers$")
    public void enter_freetext_to_filter_specialoffers(String mValue) {
        GlobalUtils.hardWait(1000);
        specialOffersPage.getSearchField().sendKeys(mValue);
        GlobalUtils.hardWait(1000);
        specialOffersPage.getSearchField().sendKeys(Keys.ENTER);
    }

    @And("^sort specialoffers by (.*) value$")
    public void select_value_from_sort_dropdown(String mValue) {
        GlobalUtils.hardWait(1000);
        specialOffersPage.selectValueFromDropDown(specialOffersPage.getSortDropdown(), mValue);
        GlobalUtils.hardWait(1000);
    }

    @And("link to specialoffers property details is displayed")
    public void link_to_property_details_is_displayed() {
        System.out.println("link to property details is displayed");
        GlobalUtils.hardWait(2000);
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='special-offers-card-propert-details-link' and contains(text(), 'Property Details')]")));
            List<WebElement> elementList =  driver.findElements(By.xpath("//div[@class='special-offers-card-propert-details-link' and contains(text(), 'Property Details')]"));
            assertTrue(elementList.get(0).isDisplayed());
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the property details link: " + se.getMessage());
        }
    }

    @And("link to specialoffers property name is displayed")
    public void link_to_property_name_is_displayed() {
        System.out.println("link to property name is displayed");
        GlobalUtils.hardWait(2000);
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='special-offers-card-card-title-container']/a")));
            List<WebElement> elementList =  driver.findElements(By.xpath("//div[@class='special-offers-card-card-title-container']/a"));
            assertTrue(elementList.get(0).isDisplayed());
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the property name link: " + se.getMessage());
        }
    }

    @And("link to specialoffers property image is displayed")
    public void link_to_property_image_is_displayed() {
        System.out.println("link to property image is displayed");
        GlobalUtils.hardWait(1000);
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='special-offers-card-card-horizontal']/a")));
            List<WebElement> elementList =  driver.findElements(By.xpath("//div[@class='special-offers-card-card-horizontal']/a"));
            assertTrue(elementList.get(0).isDisplayed());
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the property image link: " + se.getMessage());
        }
    }

    @And("user selects specialoffers property details link")
    public void user_selects_property_details_link() {
        System.out.println("link to property image is displayed");

        GlobalUtils.hardWait(1000);
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='special-offers-card-propert-details-link' and contains(text(), 'Property Details')]")));
            List<WebElement> elementList =  driver.findElements(By.xpath("//div[@class='special-offers-card-propert-details-link' and contains(text(), 'Property Details')]"));
            int hotelIdx = getRandomOption(1,5); //assuming list size is atleast 5
            System.out.println("idx is: " + hotelIdx );
            WebElement childElement = elementList.get(hotelIdx);
            getSelectedHotelName(hotelIdx);
            WebElement parent = (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].parentNode;", childElement);
            parent.click();
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the property image link: " + se.getMessage());
        }
    }

    @And("user selects specialoffers property name link")
    public void user_selects_property_name_link() {
        System.out.println("select property name link");
        GlobalUtils.hardWait(1000);
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='special-offers-card-card-title-container']/a")));
            List<WebElement> elementList =  driver.findElements(By.xpath("//div[@class='special-offers-card-card-title-container']/a"));
            int hotelIdx = 0; //getRandomOption(1,5);
            System.out.println("hotel idx selected " + hotelIdx  );
            List<WebElement> h3ElmtList = driver.findElements(By.xpath("//div[@class='special-offers-card-card-title-container']/a/h3"));
            selectedHotelName =  h3ElmtList.get(hotelIdx).getText();
            System.out.println("hotel idx selected - 2 " + selectedHotelName + " / " + hotelIdx  + " / element list size:  " + elementList.size());
            WebElement selElement =elementList.get(hotelIdx);
            GlobalUtils.focusElement(driver ,selElement);
            GlobalUtils.unFocusElement(driver ,selElement);
            selElement.click();
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the property name link: " + se.getMessage());
        }
    }

    @And("user selects specialoffers property image link")
    public void user_selects_property_image_link() {
        System.out.println("select property image link");
        GlobalUtils.hardWait(1000);
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='special-offers-card-card-horizontal']/a")));
            List<WebElement> elementList =  driver.findElements(By.xpath("//div[@class='special-offers-card-card-horizontal']/a"));
            int hotelIdx = getRandomOption(1,5);
            getSelectedHotelName(hotelIdx);
            elementList.get(hotelIdx).click();
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to click the property image link: " + se.getMessage());
        }
    }

    private void getSelectedHotelName(int idx) {
        System.out.println("getSelectedHotelName: " + idx);
        try {
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='special-offers-card-card-title-container']/a/h3")));
            List<WebElement> elmts = driver.findElements(By.xpath("//div[@class='special-offers-card-card-title-container']/a/h3"));
            selectedHotelName = elmts.get(idx).getText();
            System.out.println("selected hotel name: " + selectedHotelName);
        } catch (Exception e ) {
            System.out.println("Unable to get the selected hotel name: " + e.getMessage());
        }
    }
    private void getSelectedHotelName() {
        getSelectedHotelName(0);
    }

    @Then("specialoffers property details page is displayed")
    public void specialoffers_property_details_page_is_displayed() {

        //div[@class='card-title-container']/a/h3
        GlobalUtils.hardWait(1500);
        try {
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='property-title-containter']/div/h1")));
            WebElement element =  driver.findElement(By.xpath("//div[@class='property-title-containter']/div/h1"));

            System.out.println("selected hotel / actual hotel: " + element.getText() + " / " + selectedHotelName);
            if (element.getText().equalsIgnoreCase(selectedHotelName)) {
                assertTrue(true);
            } else {
                fail();
            }
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the property name link: " + se.getMessage());
        }
    }
}
