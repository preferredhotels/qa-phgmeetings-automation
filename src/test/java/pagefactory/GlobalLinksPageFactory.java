package pagefactory;

import constants.PHGConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.time.Duration;

public class GlobalLinksPageFactory extends TestBase{

	@FindBy(linkText=PHGConstants.PHG_LINKS.HOME_LINK_TEXT)
	WebElement homeLink;
	
	@FindBy(linkText=PHGConstants.PHG_LINKS.NEW_SEARCH_LINK_TEXT)
	WebElement searchLink;
	
	@FindBy(linkText=PHGConstants.PHG_LINKS.ABOUT_US_LINK_TEXT)
	WebElement aboutusLink;
	
	@FindBy(linkText=PHGConstants.PHG_LINKS.I_PREFER_PLANNER_LINK_TEXT)
	WebElement iPreferPlannerLink;
	
	@FindBy(linkText=PHGConstants.PHG_LINKS.SPECIAL_OFFERS_LINK_TEXT)
	WebElement specialOffersLink;
	
	@FindBy(linkText=PHGConstants.PHG_LINKS.HOT_DATES_LINK_TEXT)
	WebElement hotDatesLink;
	
	@FindBy(linkText=PHGConstants.PHG_LINKS.NEWS_LINK_TEXT)
	WebElement newsLink;
	
	@FindBy(linkText=PHGConstants.PHG_LINKS.CONTACT_US_LINK_TEXT)
	WebElement contactUsLink;
	
	@FindBy(linkText=PHGConstants.PHG_LINKS.PRIVACY_LINK_TEXT)
	WebElement cookiesPrivacyPolicyLink;
	
	@FindBy(linkText=PHGConstants.PHG_LINKS.PROFILE_EDITOR_LINK_TEXT)
	WebElement profileEditorLink;
	
	@FindBy(id="logo")
	WebElement preferredMeetingsLogo;

	@FindBy(xpath= "//div[@class='propert-details-link']")
	WebElement propertydetailsLink;



	// constructor
	public GlobalLinksPageFactory(){
		try {
			if (driver == null) {
				openBrowser("");
			}
		}catch (IOException e) {
			e.printStackTrace();
		}
		PageFactory.initElements(driver, this);
	}


	public void clickLink(String link) {
		String xpath = (browserName.contains("iphone") || browserName.contains("android")) ? "(//a[text()='"+link+"'])[2]" : "//a[text()='"+link+"']";
		if ((browserName.contains("iphone") || browserName.contains("android"))) {
			WebElement threeDots = driver.findElement(By.xpath("//span[@class='header__hamburger']"));
			if (threeDots.isDisplayed()) {
				threeDots.click();
			}
		}
		WebElement elm =  driver.findElement(By.xpath(xpath));
		if (null != elm) {
			System.out.println("clicking link: " +link);
			elm.click();
		}
	}
	
	public void clickCookiesAndPrivacyLink() {
		cookiesPrivacyPolicyLink.click();
	}
	
	public void clickProfileEditorLink() {
		profileEditorLink.click();
	}
	
	public void clickPreferredMeetingsLogo() {
		preferredMeetingsLogo.click();
	}

	public void navigateToPage() {
		//System.out.println("user is on PHG Meetings page to test page");
		driver.navigate().to(phgConstantsInstance.getPageAddress());
		try {
			//Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='onetrust-accept-btn-handler']")));
			driver.findElement(By.xpath("//*[@id='onetrust-accept-btn-handler']")).click();
		} catch (Exception e){
			System.out.println("Unable to find the cookie element: " + e.getMessage());
		}
	}
}
