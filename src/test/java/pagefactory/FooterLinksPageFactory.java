package pagefactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FooterLinksPageFactory {

    //Add all your elements FindBy annotations here
    @FindBy(xpath="//img[@alt = 'Preferred Logo' and @src= '/assets/images/footer_logo.png']")
    WebElement PHLogo;

    @FindBy(xpath="//img[@alt = 'Beyond Green Logo' and @src= '/assets/images/footer_logo_bg.png']")
    WebElement BGLogo;

    @FindBy(xpath="//p[@class='footer__link' and contains(text(), '311 South Wacker Drive, Suite 1900, Chicago, IL 60606-6620')]")
    WebElement PHAddress;

    @FindBy(xpath="//a[@class='footer__link footer__link__url' and contains(text(), 'Telephone: +1 312 542 9275')]")
    WebElement PHPhone;

    @FindBy(xpath="//a[@class='footer__link footer__link__url' and contains(text(), 'Cookies & Privacy Policy')]")
    WebElement cookiesnPrivacy;

    @FindBy(xpath="//a[@class='footer__link footer__link__url' and contains(text(), '//a[@class='footer__link footer__link__url' and contains(text(), 'Profile Editor')]")
    WebElement profileEditor;

    @FindBy(xpath="//button[@id='ot-sdk-btn']")
    WebElement DNSPersonalInfo;

    public boolean isPHLogoPresent() {
        return PHLogo.isDisplayed();
    }

    public boolean isBGLogoPresent() {
        return BGLogo.isDisplayed();
    }

    public boolean isPHAddressPresent() {
        return PHAddress.isDisplayed();
    }

    public boolean isPHPhonePresent() {
        return PHPhone.isDisplayed();
    }

    public boolean iscookiesnPrivacyPresent() {
        return cookiesnPrivacy.isDisplayed();
    }

    public boolean isprofileEditorPresent() {
        return profileEditor.isDisplayed();
    }

    public boolean isDNSPersonalInfoPresent() {
        return DNSPersonalInfo.isDisplayed();
    }


}
