package pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.GlobalUtils;

import java.time.Duration;

import static org.junit.Assert.assertTrue;

public class IPreferPlannerPageFactory extends TestBase{

    //Add all your elements FindBy annotations here
    //@FindBy(xpath="//img[@alt='planner logo']")
    @FindBy(xpath="//img[@alt='I Prefer Planner black on white logo']")
    WebElement mainLogo;

    @FindBy(xpath="//img[@alt='masthead']")
    WebElement mastheadImage;

    @FindBy(xpath="//div[contains(text(), 'THE REWARDS')]")
    WebElement rewardsSection;

    @FindBy(xpath="//div[contains(@class, 'planner-category-body') and contains(text(), 'Points can be redeemed for ')]")
    WebElement rewardsBody;

    @FindBy(xpath="//div[contains(text(), 'MEMBERSHIP IS EASY')]")
    WebElement membershipSection;

    @FindBy(xpath="//div[contains(text(), 'information or to enroll')]")
    WebElement membershipBody;

    @FindBy(xpath="//div[contains(text(), 'THE RECOGNITION')]")
    WebElement recognitionSection;

    @FindBy(xpath="//div[contains(@class, 'planner-category-body')]")
    WebElement recognitionBody;

    @FindBy(xpath="//div[contains(text(), '1 844 874 4357')]")
    WebElement PHPmembershipphonenumber;


    public IPreferPlannerPageFactory() {
        PageFactory.initElements(driver, this);
    }

    public boolean isIpreferLogoPresent() {
        return mainLogo.isDisplayed();
    }

    public boolean isIpreferImagePresent() {
        return mastheadImage.isDisplayed();
    }

    public boolean isRewardsSectionPresent() {
        return rewardsSection.isDisplayed();
    }

    public boolean isRewardsBodyPresent() {
        return rewardsBody.isDisplayed();
    }

    public boolean isMembershipSectionPresent() {
        return membershipSection.isDisplayed();
    }

    public boolean isMembershipBodyPresent() {
        return membershipBody.isDisplayed();
    }

    public boolean isRecognitionSectionPresent() { return recognitionSection.isDisplayed(); }

    public boolean isRecognitionBodyPresent() { return recognitionBody.isDisplayed(); }

    public boolean isPHPmembershipphonenumberPresent() {
        return PHPmembershipphonenumber.isDisplayed();
    }


    public void verifyUserIsOnPrefrPlannerPage() {
        //System.out.println("user is on PHG Meetings page to test iPreferPlanner");
        driver.navigate().to(phgConstantsInstance.getPageAddress());
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10)  );
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='onetrust-accept-btn-handler']")));
            driver.findElement(By.xpath("//*[@id='onetrust-accept-btn-handler']")).click();
        } catch (Exception e){
            System.out.println("Unable to find the cookie element: " + e.getMessage());
        }
    }

    public void verifyPlannerPageIsDisplayed() {
        //System.out.println("I PreferPlanner page is displayed");
        String imgSrc="";
        String locatorLogo = "//img[contains(@alt, 'I Prefer Planner black on white logo')]";
        WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath(locatorLogo));
        if (null != element) {
            imgSrc = element.getAttribute("src");
            //System.out.println("Image src: " + imgSrc);
        }
        assertTrue("Planner logo image exists",GlobalUtils.verifyimgActive(imgSrc));
    }

    public void verifyPlannerPageImageIsDisplayed() {
        //System.out.println("I PreferPlanner displays an image");

        String imgSrc="";
        String mainLogo = "//img[@alt='masthead']";
        WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath(mainLogo));
        if (null != element) {
            imgSrc = element.getAttribute("src");
            //System.out.println("Image src: " + imgSrc);
        }
        assertTrue("Main logo image exists",GlobalUtils.verifyimgActive(imgSrc));
        //assertTrue("Planner Page Main image displayed",iPreferPlannerPage.isIpreferImagePresent());
    }
}
