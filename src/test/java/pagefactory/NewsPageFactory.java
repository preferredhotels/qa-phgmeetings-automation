package pagefactory;

import constants.PHGConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.GlobalUtils;

import java.time.Duration;
import java.util.List;

import static org.junit.Assert.*;

public class NewsPageFactory  extends TestBase  {
    //Add all your elements FindBy annotations here
    @FindBy(xpath="//select[@id=\"date\"]")
    WebElement dateDropdown;

    @FindBy(xpath="//select[@id=\"hotel\"]")
    WebElement hotelDropdown;

    @FindBy(xpath="//input[@class=\"news-container-search\"]")
    WebElement searchTextField;

    @FindBy(xpath="//h3[@class = \'news-title\']")
    WebElement newsPageTitle;

    @FindBy(className="news-container-search")
    WebElement searchTextBox;


    public NewsPageFactory() {
        driverWait =  new WebDriverWait(driver,  Duration.ofSeconds(20));
        PageFactory.initElements(driver, this);
    }

    public String getPageTitle() {
        //System.out.println("getTitle called: " + driver.getTitle());
        return driver.getTitle();
    }
    public WebElement getDateDropdownTxtElement() {
        return dateDropdown;
    }

    public WebElement getHotelDropdownTxtElement() {
        return hotelDropdown;
    }

    public WebElement getSearchTextField() {
        return searchTextField;
    }


    public void verifyUserIsOnPhgMeetingsPage() {
        //System.out.println("User is on phg meetings page to begin search");
        GlobalUtils.hardWait(1500);
        driver.navigate().to(phgConstantsInstance.getPageAddress());
        GlobalUtils.hardWait(2000);
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='onetrust-accept-btn-handler']")));
            driver.findElement(By.xpath("//*[@id='onetrust-accept-btn-handler']")).click();
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the cookie element: " + se.getMessage());
            GlobalUtils.hardWait(2000);
            driver.findElement(By.xpath("//*[@id='onetrust-accept-btn-handler']")).click();
        }
    }

    public void verifyPageIsDisplayed() {
        //System.out.println("news page is displayed");
        try {
            GlobalUtils.hardWait(1500);
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@class='news-title' and contains(text(),'News & Bulletins')]")));
            WebElement element = driver.findElement(By.xpath("//h3[@class='news-title' and contains(text(),'News & Bulletins')]"));
            if (null != element) {
                String actualTitle = getPageTitle();
                String expectedTitle = PHGConstants.PHG_PAGE_TITLES.NEWS_PAGE_TITLE;
                assertEquals(expectedTitle,actualTitle);
            }
        } catch (StaleElementReferenceException e) {
            System.out.println("unable to verify news page");
        }
    }

    public boolean selectFilterByDateDropDown() {
        GlobalUtils.focusElement(driver, dateDropdown);
        int index;
        if(browserName.contains("iphone") || browserName.contains("android")) {
            dateDropdown.click();
            index = getRandomOption(0, driver.findElements(By.xpath("//select[@id='date']//option")).size());
            System.out.println("index is: " + index);
            driver.findElement(By.xpath("//select[@id='date']//option[3]")).click();
        }else {
            Select dateDropDowndd = new Select(dateDropdown);
            index = getRandomOption(0, dateDropDowndd.getOptions().size());
            dateDropDowndd.selectByIndex(index - 1);
            GlobalUtils.unFocusElement(driver, dateDropdown);
        }
        if (index > -1) return true;
        return false;
    }

    public boolean selectFitlerByHotelDropDown() {
        GlobalUtils.focusElement(driver, hotelDropdown);
        int index;
        if(browserName.contains("iphone") || browserName.contains("android")){
            hotelDropdown.click();
            index = getRandomOption(0, driver.findElements(By.xpath("//select[@id='hotel']//option")).size());
            driver.findElement(By.xpath("//select[@id='hotel']//option["+index+"]")).click();
        }
        else {
            Select hotelDropDown = new Select(hotelDropdown);
            index = getRandomOption(0, hotelDropDown.getOptions().size());
            hotelDropDown.selectByIndex(index);
            GlobalUtils.unFocusElement(driver, hotelDropdown);
        }

        if (index > -1) return true;
        return false;
    }

    public void enterSearchText(String text){
        GlobalUtils.hardWait(1500);
       // GlobalUtils.focusElement(driver, searchTextBox);
        WebElement searchElement = null;
        System.out.println("brosername is : " + browserName);
        if (browserName.contains("iphone") || browserName.contains("android")) {
            searchElement = driver.findElement(By.xpath("//input[@aria-label='Search']"));
        } else {
            //searchElement = driver.findElement(By.xpath("news-container-search"));news-container-search
            searchElement = driver.findElement(By.xpath("//input[@class= 'news-container-search']"));
        }
        System.out.println("search element: " + searchElement);
        if (null != searchElement) {
            System.out.println("sending keys to search element text");
            searchElement.sendKeys(text);
        }
        //GlobalUtils.unFocusElement(driver, searchTextBox);
    }

    public void verifySearchResults(){
        System.out.println("verifySearchResults called");
        List<WebElement> results = driver.findElements(By.xpath("//div[contains(@class, 'news-listing-container')]"));
        for(WebElement ele : results){
            System.out.println("ele name: " + ele.getTagName());
            assertTrue(ele.findElement(By.cssSelector("div[class='news-listing-day']")).isDisplayed());
            assertTrue(ele.findElement(By.cssSelector("div[class='news-listing-month']")).isDisplayed());
            assertTrue(ele.findElement(By.cssSelector("div[class='news-listing-year']")).isDisplayed());
           assertTrue(ele.findElement(By.cssSelector("div[class='news-listing-title']")).isDisplayed());
        }
    }

    public boolean verifyNewsFilterDropdownElementExists(String newsElement) {
        boolean elmtExists = false;
        try {
            GlobalUtils.hardWait(2000); // need to wait for the page to load, otherwise the following code will use the button on the previous page
            WebElement element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(newsElement)));
            if (null != element) {
                elmtExists =  element.isDisplayed();
            }
        } catch (StaleElementReferenceException se) {
            GlobalUtils.hardWait(1000);
            WebElement element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(newsElement)));
            if (null != element) {
                elmtExists =  element.isDisplayed();
            }
        }
        return elmtExists;
    }

    public boolean verifyNewsFilterSearchTextElementExists(String newsElement) {
        boolean elmtExists = false;
        try {
            GlobalUtils.hardWait(2000); // need to wait for the page to load, otherwise the following code will use the button on the previous page
            WebElement element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(newsElement)));
            if (null != element) {
                elmtExists =  element.isDisplayed();
            }
        } catch (StaleElementReferenceException se) {
            GlobalUtils.hardWait(1000);
            WebElement element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(newsElement)));
            if (null != element) {
                elmtExists =  element.isDisplayed();
            }
        }
        return elmtExists;
    }

    public void verifyNewsPageDisplaysHotelImage() {
        System.out.println("verifyNewsPageDisplaysHotelImage");
        try {
            GlobalUtils.hardWait(2000); // need to wait for the page to load, otherwise the following code will use the button on the previous page
            WebElement element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='news-listing-img']/img")));
            List<WebElement> elmList = driver.findElements(By.xpath("//div[@class='news-listing-img']/img"));
            for (WebElement elm : elmList) {
                String imgSrc = elm.getAttribute("src");
                boolean imgExists = GlobalUtils.verifyimgActive(imgSrc);
                assertTrue(imgExists);
                break;
            }
        } catch (Exception e) {
            System.out.println("Unable to verify news page hotel image: " + e.getMessage());
        }
    }

    public void verifyNewsPageDisplaysHotelNewsHeading() {
        System.out.println("verifyNewsPageDisplaysHotelNewsHeading");
        try {
            GlobalUtils.hardWait(2000); // need to wait for the page to load, otherwise the following code will use the button on the previous page
            WebElement element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='news-listing-title']")));
            List<WebElement> elmList = driver.findElements(By.xpath("//div[@class='news-listing-title']"));
            for (WebElement elm : elmList) {
                String newsText = elm.getText();
                if (!newsText.isEmpty()) {
                    assertTrue(true);
                } else {
                    fail();
                }
                break;
            }
        } catch (Exception e) {
            System.out.println("Unable to verify news hotel heading: " + e.getMessage());
        }
    }

    public void verifyNewsPageDisplaysHotelLink() {
        System.out.println("verifyNewsPageDisplaysHotelLink");
        try {
            GlobalUtils.hardWait(2000); // need to wait for the page to load, otherwise the following code will use the button on the previous page
            WebElement element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='news-listing-hotel']/a")));
            List<WebElement> elmList = driver.findElements(By.xpath("//div[@class='news-listing-hotel']/a"));
            for (WebElement elm : elmList) {
                String hrefText = elm.getAttribute("href");
                if (!hrefText.isEmpty()) {
                    GlobalUtils.focusElement(driver, elm);
                    assertTrue(true);
                } else {
                    fail();
                }
                GlobalUtils.unFocusElement(driver, elm);
                break;
            }
        } catch (Exception e) {
            System.out.println("Unable to verify news hotel link: " + e.getMessage());
        }
    }

    public void chooseSelectedDate(String sDate) {
        GlobalUtils.focusElement(driver, dateDropdown);
        if(browserName.contains("iphone") || browserName.contains("android")){
            dateDropdown.click();
            driver.findElement(By.xpath("//select[@id='date']//option[text()='"+sDate+"']")).click();
        }
        else {
            Select dropdown = new Select(dateDropdown);
            dropdown.selectByVisibleText(sDate);
            GlobalUtils.unFocusElement(driver, dateDropdown);
        }
    }

    public void chooseSelectedHotel(String sHotel) {
        GlobalUtils.focusElement(driver, hotelDropdown);
        if(browserName.contains("iphone") || browserName.contains("android")){
            hotelDropdown.click();
            driver.findElement(By.xpath("//select[@id='hotel']//option[text()='"+sHotel+"']")).click();
        }
        else {
            Select hotelDropDown = new Select(hotelDropdown);
            hotelDropDown.selectByVisibleText(sHotel);
            GlobalUtils.unFocusElement(driver, hotelDropdown);
        }
    }

    public void clickLinkToHotelNews() {
        System.out.println("clickLinkToHotelNews");
        try {
            GlobalUtils.hardWait(2000); // need to wait for the page to load, otherwise the following code will use the button on the previous page
            WebElement element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='news-listing-hotel']/a")));
            List<WebElement> elmList = driver.findElements(By.xpath("//div[@class='news-listing-hotel']/a"));
            GlobalUtils.focusElement(driver, elmList.get(0));
            elmList.get(0).click();
            GlobalUtils.unFocusElement(driver, elmList.get(0));
        } catch (Exception e) {
            System.out.println("clickLinkToHotelNews: " + e.getMessage());
        }
    }

    public void verifyNewsSectionDisplayedInPropertyPage() {
        System.out.println("verifyNewsSectionDisplayedInPropertyPage");
        try {
            GlobalUtils.hardWait(2000); // need to wait for the page to load, otherwise the following code will use the button on the previous page
            WebElement element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='property-glance-title' and contains(text(),'NEWS')]")));
            WebElement elm = driver.findElement(By.xpath("//div[@class='property-glance-title' and contains(text(),'NEWS')]"));
            GlobalUtils.focusElement(driver, elm);
            assertTrue(elm.isDisplayed());
            GlobalUtils.unFocusElement(driver, elm);
        } catch (Exception e) {
            System.out.println("verifyNewsSectionDisplayedInPropertyPage: " + e.getMessage());
        }

    }

    public boolean verifyValueExistInPropertyCard(String value){
        boolean isExist = false;
        List<WebElement> elmList = driver.findElements(By.cssSelector("div[class='special-offers-card-card-header-location']"));
        for (WebElement ele : elmList) {
            isExist = ele.getText().contains(value);
            if(!isExist)
                break;
        }
        return isExist;
    }

    public boolean navigateToHotel(){
        List<WebElement> elmList = driver.findElements(By.cssSelector("div[class='card search-card']"));
        int randomNumber = getRandomOption(1, elmList.size());
        WebElement webElement = driver.findElement(By.xpath("(//div[@class='news-listing-hotel'])["+randomNumber+"]//a"));
        String hotel = webElement.getText();
        webElement.click();
        webElement = driver.findElement(By.cssSelector("h1[class='property-title']"));
        return webElement.getText().toUpperCase().contains(hotel.toUpperCase());
    }

    public boolean navigateToHotelUsingPropertyLink(){
        List<WebElement> elmList = driver.findElements(By.cssSelector("div[class='card search-card']"));
        int randomNumber = getRandomOption(1, elmList.size());
        WebElement webElement = driver.findElement(By.xpath("(//div[@class='news-listing-hotel'])["+randomNumber+"]//a"));
        String hotel = webElement.getText();
        webElement = driver.findElement(By.xpath("(//div[@class='propert-details-link'])["+randomNumber+"]"));
        webElement.click();
        webElement = driver.findElement(By.cssSelector("h1[class='property-title']"));
        return webElement.getText().toUpperCase().contains(hotel.toUpperCase());
    }

    public boolean navigateToHotelUsingImage(){
        List<WebElement> elmList = driver.findElements(By.cssSelector("div[class='card search-card']"));
        int randomNumber = getRandomOption(1, elmList.size());
        WebElement webElement = driver.findElement(By.xpath("(//div[@class='news-listing-hotel'])["+randomNumber+"]//a"));
        String hotel = webElement.getText();
        webElement = driver.findElement(By.xpath("(//div[@class='homepage-property-image'])["+randomNumber+"]"));
        webElement.click();
        webElement = driver.findElement(By.cssSelector("h1[class='property-title']"));
        return webElement.getText().toUpperCase().contains(hotel.toUpperCase());
    }

    public boolean verifyHotelNavigateTONewSection(){
        List<WebElement> elmList = driver.findElements(By.cssSelector("div[class='card search-card']"));
        int randomNumber = getRandomOption(1, elmList.size());
        WebElement webElement = driver.findElement(By.xpath("(//div[@class='news-listing-hotel'])["+randomNumber+"]//a"));
        String hotel = webElement.getText();
        webElement.click();
        webElement = driver.findElement(By.xpath("//div[@class='property-news-body']//h2"));
        return webElement.getText().toUpperCase().contains(hotel.toUpperCase());
    }
}
