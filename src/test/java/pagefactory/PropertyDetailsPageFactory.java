package pagefactory;

import constants.PHGConstants;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.GlobalUtils;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class   PropertyDetailsPageFactory  extends TestBase {

    @FindBy(xpath="(//title[contains( text(), '| PHG Meetings | PHG Meetings')]")
    WebElement pageTitle;
// .............. REQUEST PROPOSAL ......................

    //required field
    // needs to be prepopulated from the previous page
    @FindBy(id="hotel_name")
    WebElement hotelName;

    //required field
    @FindBy(id="company_event_name")
    WebElement companyEventName;

    @FindBy(id="dates_of_event")
    WebElement companyEventdatesOfEvent;

    @FindBy(id="first_name")
    WebElement firstName;

    @FindBy(id="last_name")
    WebElement lastName;

    //required field
    @FindBy(id="email_address")
    WebElement emailAddress;

    @FindBy(id="phone_number")
    WebElement phoneNumber;

    //required field
    @FindBy(id="question_and_comments")
    WebElement questionsComments;

    @FindBy(name="submit")
    WebElement submitButton;

    //recaptcha
    @FindBy(xpath="//div[@class=\"recaptcha-checkbox-checkmark\"]")
    WebElement recaptchaCB;

    @FindBy(xpath = "//div[ contains ( text(), \"Thank you for contacting Preferred Hotels & Resorts\")]")
    WebElement confirmThankyouMessage;

//...........DEMOGRAPHICS SECTION.............................

    @FindBy(xpath = "//div[@class='property-title-containter']")
    WebElement demographicsSection;

    @FindBy(xpath = "//h1[@class='property-title']")
    WebElement propertyTitle;

    @FindBy(xpath = "//div[@class='property-header-short-location']")
    WebElement propertyShortAddress;

    @FindBy(xpath = "//div[@class='property-header-full-location']")
    WebElement propertyFullAddress;

    @FindBy(xpath = "//a[ contains(text(), \"preferredhotels\")]")
    WebElement propertyURL;

    @FindBy(xpath = "//button[@class='search-property-cta property-cta']")
    WebElement btnReqProposal;


    @FindBy(xpath="(//button[contains(@aria-label, 'next slide')])[1]")
    WebElement nextArrowButton;

    @FindBy(xpath="//img[contains(@src,'googleapis')]")
    WebElement viewMapSection;

    @FindBy(css="div[id='property-special-offers-section'] div[class='property-glance-title']")
    WebElement specialOfferHeader;

    @FindBy(css="div[id='property-hotdates-section'] div[class='property-glance-title']")
    WebElement hotDatesHeader;

    @FindBy(xpath="//img[contains(@src,'twitter')]")
    WebElement imgTwitter;

    @FindBy(xpath="//img[contains(@src,'facebook')]")
    WebElement imgFacebook;

    @FindBy(xpath="//img[contains(@src,'linkedin')]")
    WebElement imgLinkedin;

    @FindBy(xpath="//div[contains(text(),'Amenities')]")
    WebElement amenitiesSectionHeader;


    // AT A GLANCE SECTION

    @FindBy(xpath="//div[@class='property-glance-title' and contains(text(),'At a glance')]")
    WebElement atAGlanceSectionHeader;

    @FindBy(xpath = "//img[contains(@src,'img-logo')]")
    WebElement atAGlanceLogo;
    //.......................................
    @FindBy(xpath="//div[contains(text(),'Dining, Food')]")
    WebElement diningSectionHeader;

    @FindBy(xpath="//div[@class = 'property-glance-title' and contains(text(),'Meeting Space')]")
    WebElement meetingsSectionHeader;

    @FindBy(xpath="//div[@class = 'property-glance-title' and contains(text(),'NEWS')]")
    WebElement latestNewsSectionHeader;

    @FindBy(xpath="//div[@class = 'property-glance-title' and contains(text(),'NEWS')]")
    WebElement healthAndSaftey;

// FOOTER LINKS SECTION ..........................

    @FindBy(xpath="//footer[@class=\"footer__container\"]")
    WebElement footerSection;

    @FindBy(xpath="//img[contains(@src,'preferred') and contains(@class, 'footer')]")
    WebElement footerPHLogo;

    @FindBy(xpath="//img [@class ='footer__logo footer__logo--desktop']")
    WebElement footerLogos;

    @FindBy(xpath="//img[contains(@src,'bg') and contains(@class, 'footer')]")
    WebElement footerBGLogo;

    @FindBy(xpath="//p[contains(text(), '311 South Wacker Drive, Suite 1900, Chicago, IL 60606-6620')]")
    WebElement footerAddress;

    @FindBy(xpath="//a[contains(text(), 'Telephone: +1 312 542 9275')]")
    WebElement footerTelephone;

    @FindBy(xpath="//a[contains(text(), 'Cookies & Privacy Policy')]")
    WebElement footerCookiesPrivacy;

    @FindBy(xpath="//a[contains(text(), 'Profile Editor')]")
    WebElement footerProfileEditor;

    @FindBy(xpath="//button[@id=\"ot-sdk-btn\"]")
    WebElement footerPersonalInfo;

    @FindBy(xpath="//button[contains( text(), 'Confirm My Choices')]")
    WebElement alertButton;

    @FindBy(xpath="//div[@class='property-main-body-copy']")
    WebElement hotelDescription;

    @FindBy(xpath="//div[contains(text(), \"Nearby Transportation & Convention Centers\")]")
    WebElement airportProximity;
// ................REQUEST PROPOSAL..............



    public PropertyDetailsPageFactory() {
        driverWait =  new WebDriverWait(driver,  Duration.ofSeconds(20));
        PageFactory.initElements(driver, this);
        String currentURL = driver.getCurrentUrl();
    }

    public void verifyPageIsDisplayed() {
        //System.out.println("news page is displayed");
        try {
            JavascriptExecutor js = (JavascriptExecutor)driver;
            js.executeScript("window.scrollBy(0,-3000)","");
            GlobalUtils.hardWait(1500);
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='property-title']")));
            WebElement element = driver.findElement(By.xpath("//h1[@class='property-title']"));
            if (null != element) {
                //String actualTitle = element.getText();
                //assertTrue(selectedHotelName.toLowerCase().contains(actualTitle.toLowerCase()));
                assertTrue(isElementExist(element));
            }
        } catch (StaleElementReferenceException e) {
            System.out.println("unable to verify PDP page");
        }
    }

    public void verifyHotelDemographics() {
        // check whether the hotel name,address and url are not null
        GlobalUtils.focusElement(driver, demographicsSection);
        assertTrue(demographicsSection.isDisplayed());
        GlobalUtils.unFocusElement(driver, demographicsSection);
    }
    public void verifyPropertyTitle() {
        GlobalUtils.focusElement(driver, propertyTitle);
        assertTrue(propertyTitle.isDisplayed());
        GlobalUtils.unFocusElement(driver, propertyTitle);
    }

    public void verifypropertyShortAddress() {
        GlobalUtils.focusElement(driver, propertyShortAddress);
        assertTrue(propertyShortAddress.isDisplayed());
        GlobalUtils.unFocusElement(driver, propertyShortAddress);
    }


    public void verifypropertyFullAddress() {
        GlobalUtils.focusElement(driver, propertyFullAddress);
        assertTrue(propertyFullAddress.isDisplayed());
        GlobalUtils.unFocusElement(driver, propertyFullAddress);
    }
    public void verifypropertyURL() {
        GlobalUtils.focusElement(driver, propertyURL);
        assertTrue(propertyURL.isDisplayed());
        GlobalUtils.unFocusElement(driver, propertyURL);
    }

    public void verifyReqProposal() {
        GlobalUtils.focusElement(driver, btnReqProposal);
        assertTrue(btnReqProposal.isDisplayed());
        GlobalUtils.unFocusElement(driver, btnReqProposal);
    }

    public void hotelDescriptionIsPresent(){
        if (hotelDescription != null) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", hotelDescription);
            GlobalUtils.focusElement(driver, hotelDescription);
            assertTrue(hotelDescription.isDisplayed());
            GlobalUtils.unFocusElement(driver, hotelDescription);

        }
    }

    public void atAGlancesection(){
        GlobalUtils.focusElement(driver, atAGlanceSectionHeader);
        atAGlanceSectionHeader.isDisplayed();
        GlobalUtils.unFocusElement(driver, atAGlanceSectionHeader);
    }

    public void verifyLogoIsPresent() {
        GlobalUtils.focusElement(driver, atAGlanceLogo);
        atAGlanceLogo.isDisplayed();
        GlobalUtils.unFocusElement(driver, atAGlanceLogo);
    }

    public void airportProximity(){
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", airportProximity);
        GlobalUtils.focusElement(driver, airportProximity);
        airportProximity.isDisplayed();
        GlobalUtils.unFocusElement(driver, airportProximity);
    }


    public void verifySlider(){
        List<WebElement> elementList = driver.findElements(By.cssSelector("ul[class='slider animated'] li"));
        for(int i =1 ; i< elementList.size(); i++){
            assertTrue(elementList.get(i).getAttribute("class").equalsIgnoreCase("slide selected"));
            nextArrowButton.click();
        }
    }

    public void verifyViewMapPresent(){
        //((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", viewMapSection);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, -document.body.scrollHeight)");
        GlobalUtils.focusElement(driver, viewMapSection);
        assertTrue(viewMapSection.isDisplayed());
        GlobalUtils.unFocusElement(driver, viewMapSection);
    }

    public void verifyPropertyCodeMenuElements(){
        // navigate to the top section
        System.out.println("BEfore get menu section and scroll to the view");
        WebElement menuElement = driver.findElement(By.xpath("//div[@class='property-body-select-menu']"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", menuElement);
        GlobalUtils.hardWait(1000);
        WebElement specialOffersElement = null;
        WebElement hotdatesElement = null;
        System.out.println("get all the menu items and try to navigate them");
        List<WebElement> elementList = driver.findElements(By.xpath("//div[@class='property-body-select-menu']//div[contains(@class, 'item')]"));
        for(WebElement ele : elementList){
            if(ele.getAttribute("class").equalsIgnoreCase("item-true")){
                WebElement link = ele.findElement(By.tagName("a"));
                GlobalUtils.hardWait(1500);
//                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", link);
//                GlobalUtils.hardWait(3500);
                switch(link.getText()){
                    case "Special Offers":
                        driverWait.until(ExpectedConditions.elementToBeClickable(link));//.click();
                        specialOffersElement = link;
                        GlobalUtils.hardWait(1500);
                        //assertEquals("special offers", specialOfferHeader.getText().toLowerCase());
                        break;
                    case "Hot Dates":
                        //link.click();
                        driverWait.until(ExpectedConditions.elementToBeClickable(link));//.click();
                        hotdatesElement = link;
                        GlobalUtils.hardWait(1500);
                        //assertEquals("hot dates", hotDatesHeader.getText().toLowerCase());
                        break;
                }
            }
        }
        WebElement carouselElement = driver.findElement(By.xpath("//div[@class='carousel-root']"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", carouselElement);
        GlobalUtils.hardWait(1500);
        System.out.println("specialOffers / hotdatesElement: " + specialOffersElement + " / " + hotdatesElement+ " / " + carouselElement);

        if (null != specialOffersElement) {
            driverWait.until(ExpectedConditions.elementToBeClickable(specialOffersElement)).click();
        }
        GlobalUtils.hardWait(1500);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", carouselElement);
        GlobalUtils.hardWait(1500);
        if (null != hotdatesElement) {
            driverWait.until(ExpectedConditions.elementToBeClickable(hotdatesElement)).click();
        }
    }

    public void verifySocialMediaLinks() {
        if(isElementExist(imgTwitter)) {
            GlobalUtils.focusElement(driver, imgTwitter);
            imgTwitter.isDisplayed();
            GlobalUtils.unFocusElement(driver, imgTwitter);
        }
        if(isElementExist(imgLinkedin)) {
            GlobalUtils.focusElement(driver, imgLinkedin);
            imgLinkedin.isDisplayed();
            GlobalUtils.unFocusElement(driver, imgLinkedin);
        }
        if(isElementExist(imgFacebook)) {
            GlobalUtils.focusElement(driver, imgFacebook);
            imgFacebook.isDisplayed();
            GlobalUtils.unFocusElement(driver, imgFacebook);
        }
            }


    public void verifyAmenitiesIsPresent() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", amenitiesSectionHeader);
        GlobalUtils.focusElement(driver, amenitiesSectionHeader);
        amenitiesSectionHeader.isDisplayed();
        GlobalUtils.unFocusElement(driver, amenitiesSectionHeader);
    }


    public void displayDiningInfo() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", diningSectionHeader);
        GlobalUtils.focusElement(driver, diningSectionHeader);
        diningSectionHeader.isDisplayed();
        GlobalUtils.unFocusElement(driver, diningSectionHeader);
    }


    public void displayMeetingingInfo() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", meetingsSectionHeader);
        GlobalUtils.focusElement(driver, meetingsSectionHeader);
        meetingsSectionHeader.isDisplayed();
        GlobalUtils.unFocusElement(driver, meetingsSectionHeader);
    }

//    public void displayLatestNews() {
//        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", latestNewsSectionHeader);
//       // WebElement latestNewsSectionHeaderElmt = driver.findElement(By.xpath("//div[@class = 'property-glance-title' and contains(text(),'NEWS')]"));
//        GlobalUtils.focusElement(driver, latestNewsSectionHeaderElmt);
//        latestNewsSectionHeaderElmt.isDisplayed();
//        GlobalUtils.unFocusElement(driver, latestNewsSectionHeaderElmt);
//    }
    public void displayLatestNews() {
        GlobalUtils.hardWait(1500);
        if (!browserName.contains("android") && !browserName.contains("iphone")) {
            try {
                WebElement newsElement = driver.findElement(By.xpath("//div[@class = 'property-glance-title' and contains(text(),'NEWS')]"));
                if (null != newsElement) {
                    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", newsElement);
                    GlobalUtils.focusElement(driver, newsElement);
                    newsElement.isDisplayed();
                    GlobalUtils.unFocusElement(driver, newsElement);
                } else {
                    WebElement nElement = driver.findElement(By.xpath("//div[@class = 'property-glance-title' and contains(text(),'NEWS')]"));
                    if (nElement != null && nElement.isDisplayed()) {
                        assertTrue(true);
                    }
                }
            } catch (Exception e) {
                System.out.printf(" no news element on page: " + e.getMessage());
            }
        }
    }
    public void displayFooterLinks() {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
        GlobalUtils.focusElement(driver, footerSection);
        footerSection.isDisplayed();
        GlobalUtils.unFocusElement(driver, footerSection);


    }

    public void verifyTransportIsPresent() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", footerSection);
        GlobalUtils.focusElement(driver, airportProximity);
        airportProximity.isDisplayed();
        GlobalUtils.unFocusElement(driver, airportProximity);
    }

    public void verifyFooterLogos() {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
        GlobalUtils.hardWait(1000);
        System.out.println(" In Verify Logos section: " + footerPHLogo);

        GlobalUtils.focusElement(driver, footerLogos);
        footerLogos.isDisplayed();
        GlobalUtils.unFocusElement(driver, footerLogos);

        GlobalUtils.focusElement(driver, footerPHLogo);
        footerPHLogo.isDisplayed();
        GlobalUtils.unFocusElement(driver, footerPHLogo);

        GlobalUtils.focusElement(driver, footerBGLogo);
        footerBGLogo.isDisplayed();
        GlobalUtils.unFocusElement(driver, footerBGLogo);
    }

    public void verifyFooterAddress() {
        GlobalUtils.hardWait(1000);
        System.out.println("verify footer address");
        GlobalUtils.focusElement(driver, footerAddress);
        footerAddress.isDisplayed();
        GlobalUtils.unFocusElement(driver, footerAddress);
    }

    public void verifyFooterPhoneNumber() {
        GlobalUtils.focusElement(driver, footerTelephone);
        footerTelephone.isDisplayed();
        GlobalUtils.unFocusElement(driver, footerTelephone);
    }


    public void verifyFooterCookiesAndPrivacy() {
        GlobalUtils.focusElement(driver, footerCookiesPrivacy);
        footerCookiesPrivacy.isDisplayed();
        GlobalUtils.unFocusElement(driver, footerCookiesPrivacy);
        String clicklnk = Keys.chord(Keys.CONTROL,Keys.ENTER);
        // open the link in new tab, Keys.Chord string passed to sendKeys
        footerCookiesPrivacy.sendKeys(clicklnk);

        if (!useBrowserStack)    {
            ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
            System.out.println("tabs count: " + tabs.size() ) ;
            GlobalUtils.hardWait(250);
            driver.switchTo().window(tabs.get(1));
            String actualTitle = driver.getTitle();
            String expectedTitle = PHGConstants.PHG_PAGE_TITLES.COOKIES_AND_PRIVACY_TITLE;
            System.out.println("expected / actual: " + expectedTitle + " / " + actualTitle);
            assertEquals(expectedTitle, actualTitle);
            driver.close();
            System.out.println("tabs count after close: " + tabs.size() ) ;
            driver.switchTo().window(tabs.get(0));
            //driver.switchTo().defaultContent();
            //driver.close();
        } else if (browserName.equalsIgnoreCase("safari")) { // safari only
            GlobalUtils.hardWait(250);
            System.out.println("privacy and cookies: " + driver.getCurrentUrl());
            assertEquals(driver.getCurrentUrl(),"https://phgmeetings.com/privacy-policy-all-languages");
            GlobalUtils.hardWait(250);
            driver.navigate().back();
        } else {
                //ToDo fix click and navigation of privacy and cookie policy on mobile

        }
    }

    public void verifyProfileEditor() {
        //GlobalUtils.focusElement(driver, footerProfileEditor);
        if (!useBrowserStack) {
            footerProfileEditor.isDisplayed();
            //GlobalUtils.unFocusElement(driver, footerProfileEditor);
            String clicklnk = Keys.chord(Keys.CONTROL,Keys.ENTER);
            // open the link in new tab, Keys.Chord string passed to sendKeys
            footerProfileEditor.sendKeys(clicklnk);
            ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
            driver.switchTo().window(tabs.get(1));
            String actualTitle = driver.getTitle();
            String expectedTitle = PHGConstants.PHG_PAGE_TITLES.PROFILE_EDITOR;
            assertEquals(expectedTitle, actualTitle);
            driver.close();
            driver.switchTo().window(tabs.get(0));
            GlobalUtils.hardWait(1500);
        } else {
            GlobalUtils.hardWait(1500);
            // Scroll down the page
            JavascriptExecutor js = (JavascriptExecutor) driver;
            WebElement elmt = driver.findElement(By.xpath("//a[contains(text(), 'Profile Editor')]"));
            js.executeScript("arguments[0].scrollIntoView();", elmt);
            // Then click on the profile editor link
            GlobalUtils.hardWait(1500);
            if (null != elmt) elmt.click();
            System.out.println("prof editor url: " + driver.getCurrentUrl());
            assertEquals(driver.getCurrentUrl(),"https://phgmeetings.com/dashboard");
            GlobalUtils.hardWait(1500);
            driver.navigate().back();
        }
    }

    public void verifyPersonalInfo() {
        GlobalUtils.focusElement(driver, footerPersonalInfo);
        footerPersonalInfo.isDisplayed();
        GlobalUtils.unFocusElement(driver, footerPersonalInfo);
        String clicklnk = Keys.chord(Keys.CONTROL,Keys.ENTER);
        footerPersonalInfo.sendKeys(clicklnk);
        GlobalUtils.focusElement(driver, alertButton);
        GlobalUtils.unFocusElement(driver, alertButton);
        alertButton.click();
       }

    public void verifyElementFontWeightIsBold() {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        WebElement ele = driver.findElement(By.xpath("//h1[@class='property-title']"));
        String fontWeight = (String)js.executeScript("return getComputedStyle(arguments[0]).getPropertyValue('font-Weight');",ele);
    }

    public void clickRequestProposal() {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, -document.body.scrollHeight)");
        System.out.println(".........clickRequestProposal CODE BLOCK...........");
        GlobalUtils.hardWait(1500);
        //GlobalUtils.focusElement(driver, btnReqProposal);
        //GlobalUtils.unFocusElement(driver, btnReqProposal);
        WebElement btnElement = driver.findElement(By.xpath("//button[@class='search-property-cta property-cta']//a"));
        if (null != btnElement) {
            System.out.println("Clicking on request button click " + btnElement);
            btnElement.click();
        }
    }

    public void verifyReqProposalPage() {
        GlobalUtils.hardWait(3500);
        WebElement element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(" //h3[contains(text(), 'Thanks for your interest')]")));
        if (null != element) {
            System.out.println("verifyRequestProposalPage CODE BLOCK");
            String currentURL = driver.getCurrentUrl();
            //GlobalUtils.focusElement(driver, demographicsSection);
            //GlobalUtils.unFocusElement(driver, demographicsSection);
            System.out.println("currentUrl: " + currentURL);
            assertTrue(currentURL.contains("proposal-form"));
            System.out.println("User is on request proposal page");
        }
    }
    public void hotelNamePrepopulated() {
        GlobalUtils.hardWait(1500);
        System.out.println("hotelNamePrepopulated CODE BLOCK");
        String currentURL = driver.getCurrentUrl();
       // GlobalUtils.focusElement(driver, hotelName);
        GlobalUtils.hardWait(1500);
       // GlobalUtils.unFocusElement(driver, hotelName);
        WebElement hotelNameElmt = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("hotel_name")));

        String hotelNameValue = hotelNameElmt.getText();
        System.out.println("Selected Hotel is:  " +hotelNameValue);
        assertTrue(currentURL.contains(hotelNameValue));
    }


    public void enterFormDetails() {
        GlobalUtils.hardWait(1500);
        System.out.println(".....formDetails CODE BLOCK......");
        WebElement companyEventNameElmt = driver.findElement(By.id("company_event_name"));
        companyEventNameElmt.sendKeys("My Test Company");

        WebElement companyEventdatesOfEventElmt = driver.findElement(By.id("dates_of_event"));
        companyEventdatesOfEventElmt.sendKeys("1-21-2022");

        WebElement firstNameElmt = driver.findElement(By.id("first_name"));
        firstNameElmt.sendKeys("TestRoss");

        WebElement lastNameElmt = driver.findElement(By.id("last_name"));
        lastNameElmt.sendKeys("TestOliver");

        WebElement emailAddressElmt = driver.findElement(By.id("email_address"));
         if (browserName.contains("iphone") || browserName.contains("android")) {
             ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", emailAddressElmt);
         }
        emailAddressElmt.sendKeys("test@test.com");
        WebElement phoneNumberElmt = driver.findElement(By.id("phone_number"));
        if (browserName.contains("iphone") || browserName.contains("android")) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", emailAddressElmt);
        }
        phoneNumberElmt.sendKeys("1234567890");

        WebElement questionsCommentsElmt = driver.findElement(By.id("question_and_comments"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", questionsCommentsElmt);

        questionsCommentsElmt.sendKeys("This is a test from the QA department");

        GlobalUtils.hardWait(1500);

        if (!browserName.contains("iphone") && !browserName.contains("android")) {
            WebDriverWait wait = new WebDriverWait(driver, 100);
            //
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[starts-with(@name,'a-')]")));
            WebElement captchaElement = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span#recaptcha-anchor")));
            if (null != captchaElement) {
                System.out.println("Before check recaptcha element for click");
                captchaElement.click();
            }

            GlobalUtils.hardWait(1500);
        }

    }


    public void displaysThankyouMessage() {
        GlobalUtils.hardWait(1500);
        System.out.println(".....confirmThankyouMessage CODE BLOCK......");
        GlobalUtils.focusElement(driver, confirmThankyouMessage);
        GlobalUtils.unFocusElement(driver, confirmThankyouMessage);
        confirmThankyouMessage.isDisplayed();

    }
}
