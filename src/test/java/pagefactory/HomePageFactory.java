package pagefactory;

import constants.PHGConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import utils.GlobalUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class HomePageFactory extends TestBase {

	//Add all your elements FindBy annotations here
	@FindBy(xpath = "//input[@type='submit' and @value='Search']")
	@CacheLookup
	WebElement searchButton;

	@FindBy(how = How.CSS, using="//div[@class='container-fluid body-container']")
	@CacheLookup
	WebElement homePageContainer;

	String homePageBodyContainer = "//div[@class='container-fluid body-container']";

	String homePageElementHeader = "//div[@class='home-header']";

	@FindBy(xpath="//input[@aria-label='Search']")
	@CacheLookup
	WebElement searchTxtInput;

	String searchTextAriaLabel = "//input[@aria-label='Search']";

	@FindBy(id="min-guest-rooms")
	@CacheLookup
	WebElement selectNumRooms;
	
	@FindBy(id="min-meeting-room-size")
	@CacheLookup
	WebElement selectLargestRoom;
	
	@FindBy(xpath="//select[@name=\"largest_room_m\"]")
	@CacheLookup
	WebElement selectLargestRoomMeters;
	
	@FindBy(xpath="//a[@class='btn-tertiary']")
	@CacheLookup
	WebElement switchToMetersLink;
	
	@FindBy(xpath="//a[contains(text(), 'Search By Map')]")
	@CacheLookup
	WebElement searchByMapIcon;
	
	@FindBy(xpath="//img[@alt='North America']")
	@CacheLookup
	WebElement usCanadaMap;
	
	@FindBy(xpath="//img[@alt=\"Caribbean\"]")
	@CacheLookup
	WebElement caribbeanMap;
	
	@FindBy(xpath="//img[@alt=\"Central & South America\"]")
	@CacheLookup
	WebElement centralSouthAmericaMap;
	
	@FindBy(xpath="//img[@alt=\"Europe\"]")
	@CacheLookup
	WebElement europeMap;
	
	@FindBy(xpath="//img[@alt=\"Middle East\"]")
	@CacheLookup
	WebElement middleEastMap;

	@FindBy(xpath="//img[@alt=\"Africa\"]")
	@CacheLookup
	WebElement africaMap;

	@FindBy(xpath="//img[@alt=\"Asia & Pacific\"]")
	@CacheLookup
	WebElement asiaPacificMap;
	
	@FindBy(xpath="//div[@class='homepage-masthead']")
	@CacheLookup
	WebElement heroImage;
	
	@FindBy(xpath="//h2[contains(text(), \"Top Destinations\")]")
	@CacheLookup
	WebElement topDestinations;
	
	@FindBy(xpath="//h2[contains(text(), 'WELCOME')]")
	@CacheLookup
	WebElement welcomeSection;
	
	@FindBy(xpath="//a[contains(text(), \"Preferred Meetings\")]")
	@CacheLookup
	WebElement preferredMeetingsLink;
	
	@FindBy(id="last_search_tab")
	@CacheLookup
	WebElement returnToSearchResultsLink;

	
	public String mHeroHotelTitle = "";
	public String mFeaturedHotelTitle = "";
	public String mTopDestinationCity = "";
	
	public HomePageFactory() {
		PageFactory.initElements(driver, this);
	}
	
	public String getPageTitle() {
		//System.out.println("getTitle called: " + driver.getTitle());
		return driver.getTitle();
	}
	
	public void clickSearchButton() {
		//System.out.println("clickSearchButton called");
		driver.findElement(By.xpath("//input[@type='submit' and @value='Search']")).click();
		//searchButton.click();
		//searchTxtInput.sendKeys(Keys.ENTER);
	}
	
	public void clickSearchTxtInput() {
		searchTxtInput.click();
	}
	
	public void enterSearchData(String data) {
		searchTxtInput.sendKeys(data);
		GlobalUtils.hardWait(2000);
		GlobalUtils.fluentWaitForElement(driver, By.xpath("//h1[text()='Find Hotels']")).click();
		//searchTxtInput.sendKeys(Keys.ENTER);
	}
	
	public void clickEnterOnSearchText() {
		GlobalUtils.hardWait(3000);
		searchButton.click();
	}
	
	public void clickSwitchToMetersLink() {
		switchToMetersLink.click();
	}
	
	public void clickSearchByMap() {
		searchByMapIcon.click();
	}
	
	public void clickUsCanadaMap() {
		usCanadaMap.click();
	}
	
	public void clickMexicoCaribbeanMap() {
		caribbeanMap.click();
	}
	
	public void clickCentralSouthAmericaMap() {
		centralSouthAmericaMap.click();
	}
	
	public void clickEuropeMap() {
		europeMap.click();
	}
	
	public void clickMiddleEastAfricaMap() {
		middleEastMap.click();
	}
	
	public void clickAsiaAustraliaMap() {
		asiaPacificMap.click();
	}

	public void clickOnSelectedTopDestination(String destCity) {
		mTopDestinationCity = destCity;
		//((JavascriptExecutor) driver).executeScript("window.scrollBy(0,200)");
		WebElement element = driver.findElement(By.xpath("//h2[contains(text(),'Top Destinations')]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		System.out.println("city id: " + element.getText());
		//GlobalUtils.hardWait(4000);

		WebElement elementCity = GlobalUtils.fluentWaitForElement(driver,By.linkText(destCity));
		System.out.println("element city: " + elementCity);
		Actions actions = new Actions(driver);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",elementCity );
		//actions.moveToElement(elementCity).click(); //.perform();
	}
	
	public boolean checkValidHeroImageExists() {
		List<WebElement> elementList = driver.findElements(By.cssSelector(PHGConstants.HERO_IMAGE.HERO_IMAGE_SELECTOR));
		String imgSrc = "";
		for (int i=0; i< elementList.size(); i++) {
			WebElement elmt = elementList.get(i);
			//System.out.println("elmt: " + elmt.getTagName());
			if (elmt.getTagName().equalsIgnoreCase("img")) {
				imgSrc = elmt.getAttribute("src");
				break;
			}
		}
		return GlobalUtils.verifyimgActive(phgConstantsInstance.getPageAddress()+imgSrc);
	}

	public WebElement getSearchByMapElement() {
		return searchByMapIcon;
	}
	
	public WebElement getNumberOfRoomsElement() {
		return selectNumRooms;
	}
	
	public WebElement getLargestRoomsElement() {
		return selectLargestRoom;
	}
	
	public WebElement getLargestRoomsMetersElement() {
		return selectLargestRoomMeters;
	}
	
	public WebElement getHeroImageElement() {
		return heroImage;
	}
	
	public WebElement getTopDestinationsElement() {
		return topDestinations;
	}
	
	public void clickHeroImage() {
		heroImage.click();
	}

	public String findHeroHotelTitle() {
		List<WebElement> elementList = driver.findElements(By.xpath("//a[@class='homepage-masthead-link']"));
		mHeroHotelTitle = "";
		//System.out.println("elements size: " + elementList.size());
		for (int i=0; i< elementList.size(); i++) {
			WebElement elmt = elementList.get(i);
			//System.out.println("elmt: " + elmt.getTagName());
			if (elmt.getTagName().equalsIgnoreCase("a")) {
				mHeroHotelTitle = elmt.getAttribute("href");
				break;
			}
		}
		//System.out.println("hero hotel title: " + mHeroHotelTitle);
		mHeroHotelTitle = mHeroHotelTitle.replace(phgConstantsInstance.getPageAddress(), "").replace("hotels", "").replace("/","").replace("-", " ");
		System.out.println("Hero Hotel selected: " + mHeroHotelTitle);
		return mHeroHotelTitle;	
	}
	
	public boolean allMapsAvailable() {
		GlobalUtils.hardWait(4000);
		return usCanadaMap.isDisplayed() && caribbeanMap.isDisplayed() && centralSouthAmericaMap.isDisplayed() &&
				europeMap.isDisplayed() && middleEastMap.isDisplayed() && asiaPacificMap.isDisplayed()
				&& africaMap.isDisplayed();
	}

	public void pickOptionBySelectDropDown(String optionValue, WebElement dropDown) {
		System.out.println("selectValueFromDropDown: " + dropDown + " / " + optionValue);
		dropDown.click();
		List<WebElement> options = dropDown.findElements(By.tagName("option"));
		for (WebElement option : options) {
			if (option.getText().trim().contains(optionValue)) {
				option.click();
				break;
			}
		}
//		Select dropDown = new Select(dropDown);
//		List<WebElement> optionList = dropDown.getOptions();
//		int idx = 0;
//		for(WebElement option : optionList) {
//        	System.out.println("option value: " + option.getText());
//            if(option.getText().trim().contains(optionValue)) {
//              System.out.println("idx value: " + idx);
//              break;
//            }
//            idx++;
//		}
//		dropDown.selectByIndex(idx);
	}
	
	public String findFeaturedPropertyTitle(int imgNo) {
		List<WebElement> elementList = driver.findElements(By.xpath("//div[@class=\"card homepage-property-card\"]/a"));
		mFeaturedHotelTitle = "";
		System.out.println("elements size: " + elementList.size());
		for (int i=0; i < elementList.size(); i++) {
			WebElement elmt = elementList.get(i);
			System.out.println("imgno / i : " + i + " / " + imgNo);
			if (imgNo == i) {
				mFeaturedHotelTitle =  elmt.getAttribute("href");
				break;
			}
			
		}
		System.out.println("featured hotel title: " + mFeaturedHotelTitle);
		mFeaturedHotelTitle = mFeaturedHotelTitle.replace(phgConstantsInstance.getPageAddress(), "").replace("hotels", "").replace("/","").replace("-", " ");
		System.out.println("featured hotel title: " + mFeaturedHotelTitle);
		return mFeaturedHotelTitle;	
	}
	
	public boolean isFeaturedPropertyImageValid(int imgNo) {
		String imgSrc="";
		List<WebElement> elmts = driver.findElements(By.xpath("//div[@class=\"card homepage-property-card\"]/a/img"));
		
		for (int x=0; x < elmts.size(); x++ ) {
			WebElement elmt = elmts.get(x);
			if (imgNo == x) {
				imgSrc = elmt.getAttribute("src");
				System.out.println("Imge src: " + imgSrc);
			}
		}
		return GlobalUtils.verifyimgActive(imgSrc);
	}
	
	public WebElement getFeaturedPropertyElement(int imgNo) {
		
		WebElement elmt = null;
		List<WebElement> elementList = driver.findElements(By.xpath("//div[@class=\"card homepage-property-card\"]/a"));
		
		for (int x=0; x < elementList.size(); x++ ) {
			WebElement elm = elementList.get(x);
			if (imgNo == x) {
				elmt = elm;
				break;
			}
		}
		return elmt;
	}
	
	public List<WebElement> getFeaturedPropertyElements() {
		return driver.findElements(By.xpath("//div[@class=\"featured_item\"]"));
	}
	
	public int getFeaturedPropertyCount() {
		
		List<WebElement> elmts = driver.findElements(By.xpath("//div[@class=\"card homepage-property-card\"]"));
		
		return elmts.size();
	}
	
	public WebElement getHomeSectionElement() {
		return welcomeSection;
	}
		
	public WebElement getPreferredMeetingsLink() {
		return preferredMeetingsLink;
	}
	
	public void clickPreferredMeetingsLink() {
		preferredMeetingsLink.click();
	}
	
	public String getHomePageURL() {
		return driver.getCurrentUrl();
	}
	
	public WebElement getReturnToSearchResultsLink() {
		return returnToSearchResultsLink;
	}
	
	public void clickReturnToSearchResultsLink() {
		returnToSearchResultsLink.click();
	}
	
	public void clickProfileEditorLink() {
		WebElement elmt = driver.findElement(By.xpath("//a[contains(text(), 'Profile Editor')]"));
		if ( elmt != null) {
			elmt.click();
		}
	}
	
	public void switchToNextTab() {
        ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tab.get(1));
    }
    
    public void closeAndSwitchToNextTab() {
        driver.close();
        ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tab.get(1));
    }

    public void switchToPreviousTab() {
        ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tab.get(0));
    }

    public void closeTabAndReturn() {
        driver.close();
        ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tab.get(0));
    }

    public void switchToPreviousTabAndClose() {
        ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tab.get(1));
        driver.close();
    }
    
    public String getHeroHotelTitle() {
		System.out.println("hero hotel title: "  + mHeroHotelTitle);
		return mHeroHotelTitle;
	}

	public void navigateToPage() {
		//System.out.println("User is on phg meetings page");
		driver.navigate().to(phgConstantsInstance.getPageAddress());
	}

	public void verifyHomePageIsDisplayed() {
		//System.out.println("home page is displayed");
		WebElement element = GlobalUtils.fluentWaitForElement(driver, By.cssSelector(homePageBodyContainer));
		if (null != element) {
			String actualTitle = getPageTitle();
			String expectedTitle = PHGConstants.PHG_PAGE_TITLES.HOME_PAGE_TITLE;
			assertEquals(expectedTitle,actualTitle);
		}
		//assertEquals(true, homePageContainer.isDisplayed());
	}

	public void isPageDisplayed() {
		//System.out.println("home page is displayed");
		WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath(homePageElementHeader));
		if (null != element) {
			String actualTitle = getPageTitle();
			String expectedTitle = PHGConstants.PHG_PAGE_TITLES.HOME_PAGE_TITLE;
			assertEquals(expectedTitle, actualTitle);
		}
	}

	public void isWelcomeSectionDisplayed() {
		WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath("//h2[contains(text(), 'WELCOME')]"));
		if (null != element) {
			assertEquals(element.isDisplayed(), true);
		}
	}

	public void verifyAdminLoginPageDisplayed() throws InterruptedException {
		GlobalUtils.hardWait(4000);
		WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath("//a[contains(text(), 'log in again')]"));
		if (null != element) {
			System.out.println("Login element present");
			assertEquals(true, true);
		} else {
			System.out.println("Login element not present");
			assertEquals(true, false);
		}
	}

	public void clickOnImage(String mapName) {
		//ToDo, parameterize find by mapName
		WebElement mapElement = GlobalUtils.fluentWaitForElement(driver, By.xpath("//img[@alt=\"North America\"]"));
		if (mapElement.isDisplayed()) {
			mapElement.click();
		}
	}

	public void verifyPhgMeetingHomePage() {
		System.out.println("User is on phg meetings page");
		driver.navigate().to(phgConstantsInstance.getPageAddress());
	}
}
