package pagefactory;

import constants.PHGConstants;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.GlobalUtils;

import java.time.Duration;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SearchPageFactory  extends TestBase {


	@FindBy(xpath="//a[@class=\"crumb\"]")
	WebElement topDestinationCityCrumb;

	@FindBy(xpath="//input[@placeholder='Enter location or hotel name']")
	WebElement searchLocationTxt;

	@FindBy(xpath="//select[@name='min-guest-rooms']")
	WebElement numOfGuestRooms;

	@FindBy(xpath="//select[@name='min-meeting-room-size']")
	WebElement largestMeetingRoom;

	@FindBy(xpath="//select[@id='min-ceiling']")
	WebElement minCeilingHeight;

	@FindBy(xpath="//a[@class='btn-tertiary new-search-button']")
	WebElement viewInMetersFeet;

	@FindBy(id = "Amenity/Activity:(separatew/comma)")
	WebElement searchByAminityTxt;

	@FindBy(xpath="//input[@class='btn-submit']")
	WebElement searchButton;

	@FindBy(id = "Golf")
	WebElement golfTxt;

	@FindBy(id = "Spa")
	WebElement spaTxt;

	@FindBy(id = "Beach")
	WebElement beachTxt;

	@FindBy(id = "CityCenter")
	WebElement citycenterTxt;

	@FindBy(id = "Ski")
	WebElement skiTxt;

	@FindBy(xpath="//title")
	WebElement searchTitleElement;

	@FindBy(xpath="//span[contains(text(), 'Minimum Guest Rooms: ')]")
	WebElement minGuestRoomsSearchTxt;

	@FindBy(xpath="//span[contains(text(), 'London')]")
	WebElement londonTopDestinationTxt;

	@FindBy(xpath="//span[contains(text(), 'Largest Meeting Room: ')]")
	WebElement largetMeetingRoomSearchTxt;

	@FindBy(xpath="//span[contains(text(), 'North America')]")
	WebElement northAmericaMapSearch;

	@FindBy(xpath="//input[@aria-label='Search']")
	WebElement searchTxtInput;

	public SearchPageFactory() {
		driverWait =  new WebDriverWait(driver,  Duration.ofSeconds(30));
		PageFactory.initElements(driver, this);
	}

	public String getPageTitle() {
		//System.out.println("getTitle called: " + driver.getTitle());
		return driver.getTitle();
	}

	public WebElement getTopDestinationCityElement() {
		return topDestinationCityCrumb;
	}

	public List<WebElement> getPropertyMapLinkElements() {
		return driver.findElements(By.xpath("//div/a[text()=\"map\"]"));
	}

	public WebElement getSearchLocationTxtElement() {
		return searchLocationTxt;
	}

	public WebElement getNumOfGuestRoomsElement() {
		return numOfGuestRooms;
	}

	public WebElement getMinCeilingHeightElement() {
		return minCeilingHeight;
	}

	public WebElement getViewInMetersFeetElement() {
		return viewInMetersFeet;
	}

	public WebElement getSearchByAminityTxtElement() {
		return searchByAminityTxt;
	}

	public WebElement getLargestMeetingRoomElement() {
		return largestMeetingRoom;
	}

	public WebElement getSearchButton() {
		return searchButton;
	}

	public void verifySearchResults() {
		//System.out.println("search results is displayed");
		WebElement element = null;
		try {
			GlobalUtils.hardWait(1500);
			driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PHGConstants.PHG_WAIT_ELEMENT.START_NEW_SEARCH_ELEMENT)));
			element = driver.findElement(By.xpath("//h1[contains(text(), 'Search Results')]"));
		} catch (StaleElementReferenceException se) {
			GlobalUtils.hardWait(1500);
			element = driver.findElement(By.xpath("//h1[contains(text(), 'Search Results')]"));
		}
		if (null != element) {
			//System.out.println("Before match search title and expected title");
			String actualTitle = getPageTitle();
			String expectedTitle = PHGConstants.PHG_PAGE_TITLES.SEARCH_RESULTS_TITLE;
			assertEquals(expectedTitle,actualTitle);
		} else {
			Assert.fail();
		}
	}

	public void verifySearchPageContainsMatch(String matchName) {

		try {
			WebElement element =  driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='search-term']")));
			assertTrue(element.getText().equalsIgnoreCase(matchName));
		} catch (StaleElementReferenceException se) {
			GlobalUtils.hardWait(1000);
			WebElement element =  driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='search-term']")));
			assertTrue(element.getText().equalsIgnoreCase(matchName));
		}
	}

	public void verifySearchResultsByHotelName(String searchText) {
		//System.out.println("search results with searchText is displayed");
		GlobalUtils.hardWait(1500);
		List<WebElement> elements = driver.findElements(By.xpath("//div[@class='search-property-location']"));
		for(WebElement ele : elements){
			assertTrue(ele.getText().toLowerCase().contains(searchText.toLowerCase()));
		}
	}

	public void verifySearchResultsBySearchTypeAndNumberOfRooms(String searchType, String searchText) {
		//GlobalUtils.hardWait(5000);
		//System.out.println("search results with min guest rooms is displayed");
		WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath(PHGConstants.PHG_WAIT_ELEMENT.START_NEW_SEARCH_ELEMENT));
		if (null != element) {
			GlobalUtils.hardWait(1500);
			//System.out.println("Before check largetMeetingRoomSearchTxt is displayed");
			WebElement searchRoomsElement = driver.findElement(By.xpath("//span[contains(text(), 'Minimum Guest Rooms: ')]"));
			//WebElement searchRoomsElement = driver.findElement(By.xpath("//label[.='Number of Guest Rooms:']"));

			if (searchRoomsElement.isDisplayed()) {
				System.out.println("Min guest rooms text is: " + searchType + " " + searchText.replace("+",""));
			}
		}
	}

	public void verifySearchResultsBySearchTypeAndRoomSize(String searchType, String roomSize) {
		//System.out.println("search results with room size is displayed");
		WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath(PHGConstants.PHG_WAIT_ELEMENT.START_NEW_SEARCH_ELEMENT));
		if (null != element) {
			GlobalUtils.hardWait(1500);
			//System.out.println("Before check largest meeting room is displayed");
			WebElement largeMeetingRoom = driver.findElement(By.xpath("//span[contains(text(), 'Largest Meeting Room: ')]"));
			if (largeMeetingRoom.isDisplayed()) {
				System.out.println("Largest meeting room size text is: " + searchType + " " + roomSize.replace("+",""));
			}
		}
	}

	public void verifySearchResultsBySelectedMap(String mapName) {
		//System.out.println("search results by selectedmap is displayed");
		WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath(PHGConstants.PHG_WAIT_ELEMENT.START_NEW_SEARCH_ELEMENT));
		if (null != element) {
			GlobalUtils.hardWait(1500);
			//System.out.println("Before check North America Map search is displayed");
			WebElement mapDetails = GlobalUtils.fluentWaitForElement(driver, By.xpath("//span[@class=\"search-term\"]"));
			if (mapDetails !=null ) {
				System.out.println("North America map search text displayed: " + mapDetails.getText());
			}
		}
	}

	public void verifyHotelTopDestIsDisplayed(String expectedDestinationCity) {
		GlobalUtils.hardWait(1500);

		WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath(PHGConstants.PHG_WAIT_ELEMENT.START_NEW_SEARCH_ELEMENT));
		if (null != element) {
			//System.out.println("Before verifyHotelTopDestIsDisplayed");
			if (londonTopDestinationTxt.isDisplayed()) {
				System.out.println("London Top Destination is displayed: " + londonTopDestinationTxt.getText());
			}
		}
	}

	public void clickSearchLinkHeader() {
		WebElement element = GlobalUtils.fluentWaitForElement(driver,By.xpath("//a[contains(text(),'Search')]"));
		if (null != element) {
			System.out.println("before click on search link");
		}
	}

	public void verifyUserIsOnPhgMeetingsPage() {
		//System.out.println("User is on phg meetings page to begin search");
		GlobalUtils.hardWait(1500);
		driver.navigate().to(phgConstantsInstance.getPageAddress());
		GlobalUtils.hardWait(1500);
		try {
			//Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
			driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='onetrust-accept-btn-handler']")));
			driver.findElement(By.xpath("//*[@id='onetrust-accept-btn-handler']")).click();
		} catch (StaleElementReferenceException se){
			System.out.println("Unable to find the cookie element: " + se.getMessage());
			GlobalUtils.hardWait(1500);
			driver.findElement(By.xpath("//*[@id='onetrust-accept-btn-handler']")).click();
		}
	}

	public void clickViewInMetersButtonInResults() {
		try {
			//Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
			driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@class='btn-tertiary switch-units-btn new-search-button']")));
			driver.findElement(By.xpath("//a[@class='btn-tertiary switch-units-btn new-search-button']")).click();
		} catch (StaleElementReferenceException se){
			System.out.println("Unable to find the viewinmeters element: " + se.getMessage());
			GlobalUtils.hardWait(1500);
			driver.findElement(By.xpath("//a[@class='btn-tertiary switch-units-btn new-search-button']")).click();
		}
	}

	public void verifyPropertyListedInSquareMeter() {
		boolean containsMeter = false;
		try {
			//Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
			driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='card-homepage-meeting-rooms-details']")));
			List<WebElement> elementList = driver.findElements(By.xpath("//div[@class='card-homepage-meeting-rooms-details']"));
			for (int i=0; i< elementList.size(); i++) {
				WebElement elmt = elementList.get(i);
				//System.out.println("elmt: " + elmt.getText());
				if (elmt.getText().contains(" m")) {
					containsMeter = true;
					break;
				}
			}
			assertTrue(containsMeter);
		} catch (StaleElementReferenceException se){
			System.out.println("Unable to find property in meters " + se.getMessage());
		}
	}

	public void selectSortOptionFromDropDown(String optionName) {
		try {
			GlobalUtils.hardWait(1000);
			driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("select")));
			WebElement sortSelectElement = driver.findElement(By.tagName("select"));
			Select dropDown = new Select(sortSelectElement);
			List<WebElement> optionList = dropDown.getOptions();
			//System.out.println("options list size: " + optionList.size());
			int idx = 0;
			for(WebElement option : optionList) {
				//System.out.println("option value: " + option.getText());
				if(option.getText().trim().contains(optionName)) {
					//System.out.println("idx value: " + idx);
					break;
				}
				idx++;
			}
			dropDown.selectByIndex(idx);
		} catch (StaleElementReferenceException e ){
			System.out.println("could not select option " + optionName + " from dropdown " + e.getMessage());
		}
	}

	public void verifySearchPageIsDisplayed() {
		//System.out.println("SEARCH page is displayed");
		WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath("//input[@aria-label='Search']"));
		//WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath("//h1[contains(text(), 'Keyword Search']"));
		if (null != element) {
			//System.out.println("Element text: " + element.getText() + " / " + driver.getTitle());
			String actualTitle = getPageTitle();
			String expectedTitle = PHGConstants.PHG_PAGE_TITLES.SEARCH_PAGE_TITLE;
			//assertEquals(expectedTitle,actualTitle);
		}
	}

	public boolean checkIfElementIsDisplayed(String elementPath, String byType) {
		//System.out.println("checkIfElementIsDisplayed called");
		WebElement element = null;
		try {

			if (byType.equalsIgnoreCase("ID")){
				driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id(elementPath)));
				element = driver.findElement(By.id(elementPath));
			} else if (byType.equalsIgnoreCase("XPATH")) {
				driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementPath)));
				element = driver.findElement(By.xpath(elementPath));
			}
			return(element.isDisplayed());
		} catch (StaleElementReferenceException se) {
			GlobalUtils.hardWait(1000);
			if (byType.equalsIgnoreCase("ID")){
				element = driver.findElement(By.id(elementPath));
			} else if (byType.equalsIgnoreCase("XPATH")) {
				element = driver.findElement(By.xpath(elementPath));
			}
			return(element.isDisplayed());
		}
	}

	public void enterSearchCriteria() {
		assertTrue(checkIfElementIsDisplayed("//select[@id='min-guest-rooms']","XPATH"));
		assertTrue(checkIfElementIsDisplayed("//input[@placeholder='Enter location or hotel name']","XPATH"));
		assertTrue(checkIfElementIsDisplayed("//select[@name='min-meeting-room-size']", "XPATH"));
		assertTrue(checkIfElementIsDisplayed("//select[@id='min-ceiling']", "XPATH"));
		assertTrue(checkIfElementIsDisplayed("Amenity/Activity:(separatew/comma)", "ID"));
 		assertTrue(checkIfElementIsDisplayed("//a[@class='btn-tertiary new-search-button']", "XPATH"));
	}

	public void verifySearchResultsPageIsDispalyed() {
		GlobalUtils.hardWait(1500);
		WebElement searchResultElement = GlobalUtils.fluentWaitForElement(driver, By.xpath("//h1[contains(text(),'Search Results')]"));
		if (null != searchResultElement) {
			assertTrue(true);
		}
	}

	public void verifyMapResultsIsVisible() {
		GlobalUtils.hardWait(1500);
		WebElement mapResultElement = GlobalUtils.fluentWaitForElement(driver, By.xpath("//a[contains(text(),'Map Results')]"));
		if (null != mapResultElement) {
			assertTrue(true);
		}
	}

	public void userClicksOnRequestProposalButton() {
		try {
			driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='search-property-cta']")));
			List<WebElement> elements = driver.findElements(By.xpath("//a[@class='search-property-cta']"));
			WebElement element  = elements.get(0);
			element.click();
		}
		catch (StaleElementReferenceException e) {
			System.out.println("Unable to click on the request proposal button: " + e.getMessage());
		}
	}

	public void verifyRequestProposalPageDisplayed() {
		try {
			driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(), 'Thanks for your interest in PHG Meetings!')]")));
			WebElement element = driver.findElement(By.xpath("//h3[contains(text(), 'Thanks for your interest in PHG Meetings!')]"));
			if (null != element) {
				assertTrue(element.isDisplayed());
			}
		} catch (StaleElementReferenceException e) {
			System.out.println("Unable to find request proposal page " + e.getMessage());
		}
	}

	public void selectFeatureTypeFromRefineSearch(String featureType) {
		try {
			WebElement element = null;
			if (!browserName.contains("android") && !browserName.contains("iphone")) {
				driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='Golf']")));
				element = driver.findElement(By.xpath("//input[@id='Golf']"));
			} else {
				WebElement refSearchElement = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(), ' Refine Search')]")));
				if (refSearchElement.isDisplayed()) {
					refSearchElement.click();
				}
				element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='Golf']")));
			}
			if (null != element) {
				element.click();
			}

		} catch (StaleElementReferenceException e ) {
			System.out.println("Unable to select feature type element");
		}
	}

	public void selectMultipleFeatureTypeInRefineSearch(String s1, String s2) {
		try {
			WebElement golfElm, spaElm = null;
			if (!browserName.contains("android") && !browserName.contains("iphone")) {
				driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='Golf']")));
				golfElm = driver.findElement(By.xpath("//input[@id='Golf']"));
				if (null != golfElm) {
					golfElm.click();
				}
				GlobalUtils.hardWait(500);
				driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='Spa']")));
				spaElm = driver.findElement(By.xpath("//input[@id='Spa']"));
				if (null != spaElm) {
					spaElm.click();
				}
			} else {
				WebElement refSearchElement = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(), ' Refine Search')]")));
				if (refSearchElement.isDisplayed()) {
					refSearchElement.click();
				}
				golfElm = driver.findElement(By.xpath("//input[@id='Golf']"));
				if (null != golfElm) {
					golfElm.click();
				}
				spaElm = driver.findElement(By.xpath("//input[@id='Spa']"));
				if (null != spaElm) {
					spaElm.click();
				}
			}
		} catch (StaleElementReferenceException e) {
			System.out.println("Unable to select multiple feature type element");
		}
	}

	public void selectNumOfGuestRoomsInRefineSearch() {
		try {
			GlobalUtils.hardWait(500);
			WebElement gstRoomsElm = null;
			if (!browserName.contains("android") && !browserName.contains("iphone")) {
				driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='guest-rooms-400']")));
				gstRoomsElm = driver.findElement(By.xpath("//input[@id='guest-rooms-400']"));
				if (null != gstRoomsElm) {
					gstRoomsElm.click();
				}
			} else {
				WebElement refSearchElement = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(), ' Refine Search')]")));
				if (refSearchElement.isDisplayed()) {
					refSearchElement.click();
				}
				gstRoomsElm = driver.findElement(By.xpath("//input[@id='guest-rooms-400']"));
				if (null != gstRoomsElm) {
					gstRoomsElm.click();
				}
			}
		} catch (StaleElementReferenceException e) {
			System.out.println("Unable to select number of guest room type element");
		}
	}

	public void selectLargestMeetingRoomInRefineSearch() {
		try {
			GlobalUtils.hardWait(500);
			WebElement roomSzElm = null;
			if (!browserName.contains("android") && !browserName.contains("iphone")) {
				driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='largest-room-7500']")));
				roomSzElm = driver.findElement(By.xpath("//input[@id='largest-room-7500']"));
				if (null != roomSzElm) {
					roomSzElm.click();
				}
			} else {
				WebElement refSearchElement = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(), ' Refine Search')]")));
				if (refSearchElement.isDisplayed()) {
					refSearchElement.click();
				}
				roomSzElm = driver.findElement(By.xpath("//input[@id='largest-room-7500']"));
				if (null != roomSzElm) {
					roomSzElm.click();
				}
			}
		} catch (StaleElementReferenceException e) {
			System.out.println("Unable to select largest room type element");
		}
	}

	public void selectRegionInRefineSearch() {
		try {
			GlobalUtils.hardWait(500);
			WebElement regnElm = null;
			if (!browserName.contains("android") && !browserName.contains("iphone")) {
				driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='region-North America']")));
				regnElm = driver.findElement(By.xpath("//input[@id='region-North America']"));
				if (null != regnElm) {
					regnElm.click();
				}
			} else {
				WebElement refSearchElement = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(), ' Refine Search')]")));
				if (refSearchElement.isDisplayed()) {
					refSearchElement.click();
				}
				regnElm = driver.findElement(By.xpath("//input[@id='region-North America']"));
				if (null != regnElm) {
					regnElm.click();
				}
			}
		} catch (StaleElementReferenceException e) {
			System.out.println("Unable to select region type element");
		}
	}

	public void selectCeilingHeightInRefineSearch() {
		try {
			GlobalUtils.hardWait(500);
			WebElement ceilHtElm = null;
			if (!browserName.contains("android") && !browserName.contains("iphone")) {
				driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='ceiling-height-30']")));
				ceilHtElm = driver.findElement(By.xpath("//input[@id='ceiling-height-30']"));
				if (null != ceilHtElm) {
					ceilHtElm.click();
				}
			} else {
				WebElement refSearchElement = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(), ' Refine Search')]")));
				if (refSearchElement.isDisplayed()) {
					refSearchElement.click();
				}
				ceilHtElm = driver.findElement(By.xpath("//input[@id='ceiling-height-30']"));
				if (null != ceilHtElm) {
					ceilHtElm.click();
				}
			}
		} catch (StaleElementReferenceException e) {
			System.out.println("Unable to select ceiling height type element");
		}
	}

	public void selectFromGuestRoomsDropDown(String numRooms) {
		try {
			GlobalUtils.hardWait(1500);
			driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='min-guest-rooms']")));
			WebElement guestSelect = driver.findElement(By.xpath("//select[@id='min-guest-rooms']"));
			Select dropDown = new Select(guestSelect);
			List<WebElement> optionList = dropDown.getOptions();
			//System.out.println("options list size: " + optionList.size());
			int idx = 0;
			for(WebElement option : optionList) {
				//System.out.println("option value: " + option.getText());
				if(option.getText().trim().contains(numRooms)) {
					//System.out.println("idx value: " + idx);
					break;
				}
				idx++;
			}
			dropDown.selectByIndex(idx);
		} catch (StaleElementReferenceException e) {
			System.out.println("Unable to select guest room from drop down");
		}
	}

	public void selectFromLargestRoomSizeDropDown(String roomSize) {
		try {
			GlobalUtils.hardWait(1500);
			driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='min-meeting-room-size']")));
			WebElement sizeSelect = driver.findElement(By.xpath("//select[@id='min-meeting-room-size']"));
			Select dropDown = new Select(sizeSelect);
			List<WebElement> optionList = dropDown.getOptions();
			//System.out.println("options list size: " + optionList.size());
			int idx = 0;
			for(WebElement option : optionList) {
				//System.out.println("option value: " + option.getText());
				if(option.getText().trim().contains(roomSize)) {
					//System.out.println("idx value: " + idx);
					break;
				}
				idx++;
			}
			dropDown.selectByIndex(idx);
		} catch (StaleElementReferenceException e) {
			System.out.println("Unable to select room size from drop down");
		}
	}

	public void selectFromCeilingHeightDropDown(String ceilingHeight) {
		try {
			GlobalUtils.hardWait(1500);
			driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='min-ceiling']")));
			WebElement ceilHeightSelect = driver.findElement(By.xpath("//select[@id='min-ceiling']"));
			Select dropDown = new Select(ceilHeightSelect);
			List<WebElement> optionList = dropDown.getOptions();
			System.out.println("options list size: " + optionList.size());
			int idx = 0;
			for(WebElement option : optionList) {
				//System.out.println("option value: " + option.getText());
				if(option.getText().trim().contains(ceilingHeight)) {
					System.out.println("idx value: " + idx);
					break;
				}
				idx++;
			}
			dropDown.selectByIndex(idx);
		} catch (StaleElementReferenceException e) {
			System.out.println("Unable to select ceiling height from drop down");
		}
	}

	public void enterTextInAmenityTextField(String amenity) {
		try {
			GlobalUtils.hardWait(1500);
			driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Amenity/Activity:(separatew/comma)")));
			WebElement textField = driver.findElement(By.id("Amenity/Activity:(separatew/comma)"));
			textField.sendKeys(amenity);
		} catch (StaleElementReferenceException e) {
			System.out.println("Unable to enter amenity in text field");
		}
	}

	public void enterSearchData(String data) {
		searchTxtInput.sendKeys(data);
	}

}
