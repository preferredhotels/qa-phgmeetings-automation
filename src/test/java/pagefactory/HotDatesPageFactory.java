package pagefactory;

import constants.PHGConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.GlobalUtils;

import java.time.Duration;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HotDatesPageFactory  extends TestBase   {

    @FindBy(id="region")
    WebElement regionDropdown;

    @FindBy(id="country")
    WebElement countryDropdown;

    @FindBy(id="state")
    WebElement stateDropdown;

    @FindBy(id="hotel")
    WebElement hotelDropdown;

    @FindBy(id="city")
    WebElement cityDropdown;

    @FindBy(id="date")
    WebElement dateDropdown;

    @FindBy(xpath="//input[@class=\'hot-dates-container-search\']")
    WebElement searchField;

    @FindBy(id="sort")
    WebElement sortDropdown;


    public HotDatesPageFactory() {
        driverWait =  new WebDriverWait(driver,  Duration.ofSeconds(20));
        PageFactory.initElements(driver, this);
    }

    public void selectRegion(String region){
        Select element = new Select(regionDropdown);
        element.selectByVisibleText(region);

    }
    public WebElement getRegionDropdown() {
        return regionDropdown;
    }

    public void selectCountry(String country){
        Select element = new Select(countryDropdown);
        element.selectByVisibleText(country);
    }

    public WebElement getCountryDropdown() {
        return countryDropdown;
    }

    public void selectState(String state){
        Select element = new Select(stateDropdown);
        element.selectByVisibleText(state);
    }

    public WebElement getStateDropdown() {
        return stateDropdown;
    }

    public void selectHotel(String hotel){
        Select element = new Select(hotelDropdown);
        element.selectByVisibleText(hotel);
    }

    public WebElement getHotelDropdown() {
        return hotelDropdown;
    }

    public void selectCity(String city){
        Select element = new Select(cityDropdown);
        element.selectByVisibleText(city);
    }

    public WebElement getCityDropdown() {
        return cityDropdown;
    }

    public void selectDate(String date){
        Select element = new Select(dateDropdown);
        element.selectByVisibleText(date);
    }

    public WebElement getDateDropdown() {
        return dateDropdown;
    }

    public void setSelectSort(String sort){
        Select element = new Select(sortDropdown);
        element.selectByVisibleText(sort);
    }

    public WebElement getSortDropdown() {
        return sortDropdown;
    }

    public WebElement getSearchField() {
        return searchField;
    }

    public void verifyHotdatesResults(){
        List<WebElement> results = driver.findElements(By.xpath("//div[@class='hot-dates-property-listing']//div[@class='card search-card']"));
        for(WebElement ele : results){
            assertTrue(ele.findElement(By.xpath("//div[@class='card-body card-body-hot-dates']")).isDisplayed());
            break;
        }
    }

    public void verifyUserIsOnPhgMeetingsPage() {
        //System.out.println("User is on phg meetings page to begin search");
        GlobalUtils.hardWait(1500);
        driver.navigate().to(phgConstantsInstance.getPageAddress());
        GlobalUtils.hardWait(2000);
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
           // driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'Accept All Cookies')]")));
            driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='onetrust-accept-btn-handler']")));
            driver.findElement(By.xpath("//*[@id='onetrust-accept-btn-handler']")).click();
        } catch (StaleElementReferenceException se){
            System.out.println("Unable to find the cookie element: " + se.getMessage());
            GlobalUtils.hardWait(2000);
            driver.findElement(By.xpath("//*[@id='onetrust-accept-btn-handler']")).click();
        }
    }

    public String getPageTitle() {
        //System.out.println("getTitle called: " + driver.getTitle());
        return driver.getTitle();
    }

    public void verifyPageIsDisplayed() {
        //System.out.println("news page is displayed");
        try {
            GlobalUtils.hardWait(1500);
            driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@class='hot-dates-title' and contains(text(),'Search Hot Dates')]")));
            WebElement element = driver.findElement(By.xpath("//h3[@class='hot-dates-title' and contains(text(),'Search Hot Dates')]"));
            if (null != element) {
                String actualTitle = getPageTitle();
                String expectedTitle = PHGConstants.PHG_PAGE_TITLES.HOT_DATES_PAGE_TITLE;
                assertEquals(expectedTitle, actualTitle);
            }
        } catch (StaleElementReferenceException e) {
            System.out.println("unable to verify hotdates page");
        }
    }

public void selectValueFromDropDown(WebElement dropDown, String value) {
    System.out.println("selectValueFromDropDown: " + dropDown + " / " + value);
    dropDown.click();
    List<WebElement> options = dropDown.findElements(By.tagName("option"));
    for (WebElement elm : options) {
        if (elm.getText().equalsIgnoreCase(value)) {
            elm.click();
            break;
        }
    }
}
}