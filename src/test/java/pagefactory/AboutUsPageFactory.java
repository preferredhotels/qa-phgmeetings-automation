package pagefactory;

import constants.PHGConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.GlobalUtils;

import static org.junit.Assert.assertEquals;

public class AboutUsPageFactory extends TestBase {

    //Add all your elements FindBy annotations here
    @FindBy(xpath = "//div[@class='about-us-header']")
    WebElement PHRHeader;

    public String getAboutusElementHeader() {
        return aboutusElementHeader;
    }

    //page header
    String aboutusElementHeader = "//div[@class='about-us-header']";

    @FindBy(xpath = "//img[@alt='phr logo']")
    WebElement PHRLogo;

    @FindBy(xpath = "(//div[@class='about-us-item-body'])[1]")
    WebElement PHRText;


    @FindBy(xpath = "//img[@alt='legend logo']")
    WebElement legendLogo;

    @FindBy(xpath = "(//div[@class='about-us-item-body'])[2]")
    WebElement legendText;

    @FindBy(xpath = "//img[@alt='LVX logo']")
    WebElement LVXLogo;

    @FindBy(xpath = "(//div[@class='about-us-item-body'])[3]")
    WebElement lVXText;

    @FindBy(xpath = "//img[@alt='lifestyle logo']")
    WebElement lifestyleLogo;

    @FindBy(xpath = "(//div[@class='about-us-item-body'])[4]")
    WebElement lifestyleText;

    @FindBy(xpath = "//img[@alt='connect logo']")
    WebElement connectLogo;

    @FindBy(xpath = "(//div[@class='about-us-item-body'])[5]")
    WebElement connectText;

    @FindBy(xpath = "//img[@alt='Residence logo']")
    WebElement residenceLogo;

    @FindBy(xpath = "(//div[@class='about-us-item-body'])[6]")
    WebElement residenceText;

    public boolean isAboutusHeaderPresent() {
        WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath(getAboutusElementHeader()));
        if (null != element) {
            return element.isDisplayed();
        }
        return false;
    }

    public boolean isPHRSectionPresent() {

        GlobalUtils.focusElement(driver, PHRLogo);
        GlobalUtils.focusElement(driver, PHRText);
        GlobalUtils.unFocusElement(driver, PHRLogo);
        GlobalUtils.unFocusElement(driver, PHRText);
        return PHRLogo.isDisplayed() && PHRText.isDisplayed();

    }

    public boolean isLegendSectionPresent() {

        GlobalUtils.focusElement(driver, legendLogo);
        GlobalUtils.focusElement(driver, legendText);
        GlobalUtils.unFocusElement(driver, legendLogo);
        GlobalUtils.unFocusElement(driver, legendText);
        return legendLogo.isDisplayed() && legendText.isDisplayed();
    }

    public boolean isLVXSectionPresent() {

        GlobalUtils.focusElement(driver, LVXLogo);
        GlobalUtils.focusElement(driver, lVXText);
        GlobalUtils.unFocusElement(driver, LVXLogo);
        GlobalUtils.unFocusElement(driver, lVXText);
        return LVXLogo.isDisplayed() && lVXText.isDisplayed();
    }

    public boolean isLifestyleSectionPresent() {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
        GlobalUtils.focusElement(driver, lifestyleLogo);
        GlobalUtils.focusElement(driver, lifestyleText);
        GlobalUtils.unFocusElement(driver, lifestyleLogo);
        GlobalUtils.unFocusElement(driver, lifestyleText);
        return lifestyleLogo.isDisplayed() && lifestyleText.isDisplayed();
    }

    public boolean isConnectSectionPresent() {

        GlobalUtils.focusElement(driver, connectLogo);
        GlobalUtils.focusElement(driver, connectText);
        GlobalUtils.unFocusElement(driver, connectLogo);
        GlobalUtils.unFocusElement(driver, connectText);
        return connectLogo.isDisplayed() && connectText.isDisplayed();
    }

    public boolean isResidenceLogoSectionPresent() {

        GlobalUtils.focusElement(driver, residenceLogo);
        GlobalUtils.focusElement(driver, residenceText);
        GlobalUtils.unFocusElement(driver, residenceLogo);
        GlobalUtils.unFocusElement(driver, residenceText);
        return residenceLogo.isDisplayed() && residenceText.isDisplayed();
    }

    public AboutUsPageFactory() {
        PageFactory.initElements(driver, this);
    }

    public String getPageTitle() {
        System.out.println("getTitle called: " + driver.getTitle());
        return driver.getTitle();
    }

    public void isPageDisplayed() {
        System.out.println("AboutUs page is displayed");
        WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath(getAboutusElementHeader()));
        if (null != element) {
            String actualTitle = getPageTitle();
            String expectedTitle = PHGConstants.PHG_PAGE_TITLES.ABOUT_US_PAGE_TITLE;
            GlobalUtils.focusElement(driver, element);
            assertEquals(expectedTitle, actualTitle);
            GlobalUtils.unFocusElement(driver, element);
        }
    }
}







