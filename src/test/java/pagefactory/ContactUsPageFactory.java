package pagefactory;

import constants.PHGConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.GlobalUtils;

import java.time.Duration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ContactUsPageFactory   extends TestBase {

    String contactUsElementHeader = "//h1[contains(text(), 'Contact Us')]";
    String leaderShipSection = "//h1[contains(text(), 'Contact Us')]";

    //Add all your elements FindBy annotations here
    @FindBy(xpath="//h1[contains(text(), 'Contact Us')]")
    WebElement contactUsHeader;

    @FindBy(xpath="//h2[contains(text(), 'Leadership')]")
    WebElement leadershipHeader;

    @FindBy(xpath="//h2[contains(text(), 'Sales Directors, Europe')]")
    WebElement salesEuropeHeader;

    @FindBy(xpath="//h2[contains(text(), 'Sales Directors, North America:')]")
    WebElement salesAmericaHeader;

    @FindBy(xpath="//h2[contains(text(), 'Sales Directors, Asia Pacific &')]")
    WebElement salesAsiaPacificHeader;

    @FindBy(xpath="//h2[contains(text(), 'Middle East')]")
    WebElement salesMiddleeastHeader;

    @FindBy(xpath="//h2[contains(text(), 'Group Sales Support')]")
    WebElement groupSalesHeader;

    @FindBy(xpath="//a[@class='contact-us-email' and contains(text(),'@preferredhotels.com')]")
    WebElement allEmails;

    public String getContactUsElementHeader() {
        return contactUsElementHeader;
    }

    public String getLeaderShipSection() {
        return leaderShipSection;
    }

    public ContactUsPageFactory() {
        PageFactory.initElements(driver, this);
    }

    //    @FindBy(xpath="//a[@class='contact-us-email' and contains(text(),'@preferredhotels.com')]")
//    @CacheLookup
//    WebElement AllPhoneNumbers;

    public boolean isContactUsHeaderPresent() {
        GlobalUtils.focusElement(driver, contactUsHeader);
        GlobalUtils.unFocusElement(driver, contactUsHeader);
        return contactUsHeader.isDisplayed();
    }

    public boolean isLeadershipHeaderPresent() {

        GlobalUtils.focusElement(driver, leadershipHeader);
        GlobalUtils.unFocusElement(driver, leadershipHeader);
        return leadershipHeader.isDisplayed();
    }

    public boolean isSalesEuropeHeaderPresent() {

        GlobalUtils.focusElement(driver, salesEuropeHeader);
        GlobalUtils.unFocusElement(driver, salesEuropeHeader);
        return salesEuropeHeader.isDisplayed();
    }

    public boolean isSalesAmericaHeaderPresent() {
       // ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", salesAmericaHeader);
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,600)");
        GlobalUtils.focusElement(driver, salesAmericaHeader);
        GlobalUtils.unFocusElement(driver, salesAmericaHeader);
        return salesAmericaHeader.isDisplayed();
    }

    public boolean isSalesAsiaPacificHeaderPresent() {

        GlobalUtils.focusElement(driver, salesAsiaPacificHeader);
        GlobalUtils.unFocusElement(driver, salesAsiaPacificHeader);
        return salesAsiaPacificHeader.isDisplayed();
    }

    public boolean isSalesMiddleeastHeaderPresent() {

        GlobalUtils.focusElement(driver, salesMiddleeastHeader);
        GlobalUtils.unFocusElement(driver, salesMiddleeastHeader);
        return salesMiddleeastHeader.isDisplayed();
    }

    public boolean isgroupSalesHeaderPresent() {
        //((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", groupSalesHeader);
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1000)");
        GlobalUtils.focusElement(driver, groupSalesHeader);
        GlobalUtils.unFocusElement(driver, groupSalesHeader);
        return groupSalesHeader.isDisplayed();
    }

    public boolean isallEmailsPresent() {

        GlobalUtils.focusElement(driver, allEmails);
        GlobalUtils.unFocusElement(driver, allEmails);
        return allEmails.isDisplayed();
    }

    public String getPageTitle() {
        //System.out.println("getTitle called: " + driver.getTitle());
        return driver.getTitle();
    }


    public void navigateToPage() {
        //System.out.println("user is on PHG Meetings page to test contactus page");
        driver.navigate().to(phgConstantsInstance.getPageAddress());
        try {
            //Give explicit wait for "Accept All Cookies" button, as this frame is shown after sometime
            WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='onetrust-accept-btn-handler']")));
            driver.findElement(By.xpath("//*[@id='onetrust-accept-btn-handler']")).click();
        } catch (TimeoutException te) {
            System.out.println("Try Accept All Cookies as it is different in browserstack");
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
           // wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Accept All Cookies')]")));
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='onetrust-accept-btn-handler']")));
            driver.findElement(By.xpath("//*[@id='onetrust-accept-btn-handler']")).click();
                                                //
        } catch (Exception e){
            System.out.println("Unable to find the cookie element: " + e.getMessage());
        }
    }

    public void isPageDisplayed() {
        //System.out.println("ContactUs page is displayed");
        WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath(getContactUsElementHeader()));
        if (null != element) {
            String actualTitle = getPageTitle();
            String expectedTitle = PHGConstants.PHG_PAGE_TITLES.CONTACT_US_PAGE_TITLE;
            assertEquals(expectedTitle,actualTitle);
        }
    }


    public void isLeadershipSectionPresent() {
        WebElement element = GlobalUtils.fluentWaitForElement(driver, By.xpath(getLeaderShipSection()));
        if (null != element) {
            assertTrue("Leadership section is displayed", isLeadershipHeaderPresent());
        }
    }
}
