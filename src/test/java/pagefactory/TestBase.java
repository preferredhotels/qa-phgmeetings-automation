package pagefactory;

import com.browserstack.local.Local;
import constants.PHGConstants;
import org.apache.http.util.TextUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
public class TestBase {

    public static WebDriver driver;
    public static WebDriverWait driverWait;
    public static String selectedHotelName;

    // Change flag to true to run on browserstack
    public static boolean useBrowserStack = false;
    //public boolean useBrowserStack = true;
    public Hashtable<String, String> capsHashtable = null;
    public static final String CHROME_BROWSER = "chrome";
    public static final String EDGE_BROWSER = "edge";
    public static final String SAFARI_BROWSER = "safari";
    public static final String ANDROID_11_BROWSER = "android_11";
    public static final String ANDROID_10_BROWSER = "android_10";
    public static final String ANDROID_9_BROWSER = "android_9";
    public static final String IPHONE_11_BROWSER = "iphone_11";
    public static final String IPHONE8_8_BROWSER = "iphone_8";
    public static final String IOS_BROWSER = "ios";


    // change browser name to test on a particular browser
   public String browserName = CHROME_BROWSER;
  //public String browserName = EDGE_BROWSER;
  //public String browserName = SAFARI_BROWSER;
   //public String browserName = IPHONE_11_BROWSER;
    //public String browserName = ANDROID_10_BROWSER;

   // public static final String USERNAME = "ithelp2";
   //public static final String AUTOMATE_KEY = "B1vc7MZVnqnQpCmT7ScA";
    public static final String USERNAME = "ralugunti_wgbtO4";
    public static final String AUTOMATE_KEY = "Li6ABM2xVZDUSof1Wu3t";
    public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

    public PHGConstants phgConstantsInstance;
    private Local bsLocal;

    public TestBase() {
        //System.out.println("create phgcontants instance");
        phgConstantsInstance = PHGConstants.getInstance();
    }

    public void openBrowser(String scenarioName) throws IOException {
        System.out.println("openBrowser called: " + scenarioName);

        String browserNm = System.getProperty("browser_name");
        Boolean browserStack  = false;
        if (System.getProperty("browser_stack") != null) {
            browserStack = System.getProperty("browser_stack").equalsIgnoreCase("true") ? true : false;
        }
        String address =  System.getProperty("browser_address");
        System.out.println("browserName / browserStack : " + browserNm + " / " + browserStack + " / " + address);

        if (!TextUtils.isEmpty(browserNm) && browserStack != null && !TextUtils.isEmpty(address) ) {
            //useBrowserStack = browserStack.booleanValue();
            useBrowserStack = browserStack.booleanValue();
            switch (browserNm){
                case "chrome":
                    browserName = CHROME_BROWSER;
                    break;
                case "ios":
                    browserName = IOS_BROWSER;
                    break;
                case "safari":
                    browserName = SAFARI_BROWSER;
                    break;
                case "edge":
                    browserName = EDGE_BROWSER;
                    break;
                case "android":
                    browserName = ANDROID_10_BROWSER;
                    break;
                default:
                    browserName = CHROME_BROWSER;
            }
            if (null != phgConstantsInstance) {
                if (address.equalsIgnoreCase("prod")) {
                    phgConstantsInstance.setPageAddress(PHGConstants.PHG_HOME_PAGE.PROD);
                }else if (address.equalsIgnoreCase("stage")) {
                    phgConstantsInstance.setPageAddress(PHGConstants.PHG_HOME_PAGE.STAGE);
                } else if (address.equalsIgnoreCase("qa")) {
                    phgConstantsInstance.setPageAddress(PHGConstants.PHG_HOME_PAGE.QA);
                }
            }
            System.out.println("Environment: " + "browser: " + browserName + " / browserstack: " + browserStack + " / address: " + phgConstantsInstance.getPageAddress());
        }

        if (useBrowserStack) {
            setupCapabilities();
            String key;
            DesiredCapabilities caps = new DesiredCapabilities();
            // Iterate over the hashtable and set the capabilities
            Set<String> keys = capsHashtable.keySet();
            for (String s : keys) {
                key = s;
                if (key.equalsIgnoreCase("name")) {
                    caps.setCapability(key, scenarioName);
                } else {
                    caps.setCapability(key, capsHashtable.get(key));
                }
            }
            try {
                System.out.println("URL IS: " + URL.toString());
                driver = new RemoteWebDriver(new java.net.URL(URL), caps);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            configureDriverPath();
            if (browserName.equalsIgnoreCase(CHROME_BROWSER)) {
                ChromeOptions options  = new ChromeOptions();
                options.addArguments("--remote-allow-origins=*");
                driver = new ChromeDriver(options);
                //driver = new ChromeDriver();
            } else if(browserName.equalsIgnoreCase(EDGE_BROWSER)) {

                EdgeOptions options  = new EdgeOptions();
                options.addArguments("--remote-allow-origins=*");
                driver = new EdgeDriver(options);

            } else if (browserName.equalsIgnoreCase(SAFARI_BROWSER)) {
                driver = new SafariDriver();
            }
        }
        if(!(browserName.contains("iphone") || browserName.contains("android"))) {
            driver.manage().window().setPosition(new Point(0, 0));
            setWindowWidth();
        }
    }

    private void setWindowWidth() {
        System.out.println("setWindowWidth called");
        Dimension d = new Dimension(
                PHGConstants.PHG_WIDTH_HEIGHT.PAGE_WIDTH,
                PHGConstants.PHG_WIDTH_HEIGHT.PAGE_HEIGHT);
        driver.manage().window().setSize(d);
    }

    private void setupCapabilities() {
        capsHashtable = new Hashtable<>();
        if (phgConstantsInstance.getPageAddress().contains("stage") || phgConstantsInstance.getPageAddress().contains("qa")  ) {
            System.out.println("inside setup local");
            bsLocal = new Local();
            HashMap<String, String> bsLocalOptions = new HashMap<String, String>();
            bsLocalOptions.put("key", AUTOMATE_KEY);
            bsLocalOptions.put("forcelocal", "true");
            bsLocalOptions.put("onlyAutomate", "true");
            capsHashtable.put("browserstack.local", "true");
            try {
                System.out.println("start local");
                bsLocal.start(bsLocalOptions);
                System.out.println("is local running:" +bsLocal.isRunning());
            } catch (Exception e) {
                System.out.println( e.getMessage());
            }
        }
        if (browserName.equalsIgnoreCase(CHROME_BROWSER)){
            capsHashtable.put("browser", "chrome");
            capsHashtable.put("browser_version", "97.0");
            capsHashtable.put("os", "Windows");
            capsHashtable.put("os_version", "10");
        } else if(browserName.equalsIgnoreCase(EDGE_BROWSER)) {
            capsHashtable.put("browser", "edge");
            capsHashtable.put("browser_version", "94.0");
            capsHashtable.put("os", "Windows");
            capsHashtable.put("os_version", "10");
        } else if(browserName.equalsIgnoreCase(SAFARI_BROWSER)) {
            capsHashtable.put("browser", "safari");
            capsHashtable.put("browser_version", "latest");
            capsHashtable.put("os", "OS X");
            capsHashtable.put("os_version", "Big Sur");
        } else if(browserName.equalsIgnoreCase(ANDROID_11_BROWSER)) {
            capsHashtable.put("device", "Samsung Galaxy S21 Ultra");
            capsHashtable.put("os_version", "11.0");
            capsHashtable.put("browserName", "android");
            capsHashtable.put("realMobile", "true");
        } else if(browserName.equalsIgnoreCase(ANDROID_10_BROWSER)) {
            capsHashtable.put("device", "Samsung Galaxy S20");
            capsHashtable.put("os_version", "10.0");
            capsHashtable.put("browserName", "android");
            capsHashtable.put("realMobile", "true");
        } else if(browserName.equalsIgnoreCase(ANDROID_9_BROWSER)) {
            capsHashtable.put("device", "Samsung Galaxy S9 Plus");
            capsHashtable.put("os_version", "9.0");
            capsHashtable.put("browserName", "android");
            capsHashtable.put("realMobile", "true");
        } else if(browserName.equalsIgnoreCase(IPHONE_11_BROWSER)) {
            capsHashtable.put("device", "iphone 11");
            capsHashtable.put("os_version", "14");
            capsHashtable.put("browserName", "ios");
            capsHashtable.put("realMobile", "true");
        } else if(browserName.equalsIgnoreCase(IPHONE8_8_BROWSER)) {
            capsHashtable.put("os_version", "15");
            capsHashtable.put("device", "iPhone 8");
            capsHashtable.put("real_mobile", "true");
            capsHashtable.put("browserstack.local", "false");
        }
        capsHashtable.put("browserstack.use_w3c", "true");
        capsHashtable.put("build", "browserstack-build-1");
        capsHashtable.put("name", "Thread 1");
    }

    private void configureDriverPath(){
        System.out.println("configureDriverPath called");
        String projectPath = System.getProperty("user.dir");
        System.out.println("project path: " + projectPath);
        if (browserName.equalsIgnoreCase(CHROME_BROWSER)) {
            System.setProperty("webdriver.chrome.driver", projectPath + "/src/test/resources/drivers/chromedriver.exe");
        } else if (browserName.equalsIgnoreCase(EDGE_BROWSER)) {
            System.setProperty("webdriver.edge.driver", projectPath + "/src/test/resources/drivers/msedgedriver.exe");
        }
    }

    public boolean isElementExist(WebElement element){
        boolean isExist = false;
        try{
            isExist = element.isDisplayed();
        }
        catch(NoSuchElementException ex){
            isExist = false;
        }
        return isExist;
    }

    public int getRandomOption(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    public Local getBsLocal() {
        return bsLocal;
    }
}
