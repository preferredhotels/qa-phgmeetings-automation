package pagefactory;


import constants.PHGConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.GlobalUtils;

import java.util.List;

public class HotelInfoPageFactory extends  TestBase{

	@FindBy(xpath="//div[@class=\"add_to\"]")
	WebElement  requestProposalButton;
	
	@FindBy(xpath="//i[@class=\"fa fa-twitter\"]")
	WebElement twitterButton;
	
	@FindBy(xpath="//i[@class=\"fa fa-facebook\"]")
	WebElement facebookButton;
	
	@FindBy(xpath="//div[@id=\"units\"]")
	WebElement viewInMetersButton;
	
	@FindBy(xpath="//div[@id=\"map_preview\"]")
	WebElement viewMapButton;

	@FindBy(id="x")
	WebElement hotelTitle;
	
	@FindBy(xpath="//a[text()=\"prev\"]")
	WebElement prevButton;
	
	@FindBy(xpath="//a[text()=\"next\"]")
	WebElement nextButton;
	
	String mHeroHotelTitle = "";
	
	public HotelInfoPageFactory() {
		PageFactory.initElements(driver, this);
	}
	
	public List<WebElement> getSubNavigationElements() {
		return  driver.findElements(By.xpath("//div[@id=\"subnav\"]"));
	}
	
	public WebElement getElementByText(String str) {
		WebElement element = null;

		for (int x = 0 ; x < getSubNavigationElements().size(); x++) {
			WebElement elmt = getSubNavigationElements().get(x);
			if (elmt.getText().equalsIgnoreCase(str)) {
				element = elmt;
				break;
			}
		}
		return element;
	}
	
	public List<WebElement> getHotelInfoHeaderElements() {
		return driver.findElements(By.xpath("//td[@id=\"td_prop_main\"]"));
	}
	
	public String getHotelInfoTitle() {
		WebElement element = driver.findElement(By.xpath("//td[@id=\"td_prop_main\"]/div/div/h1"));
		return element.getText();
	}
	
	public WebElement getViewInMetersButton() {
		return viewInMetersButton;
	}
	
	public WebElement getRequestProposalButton() {
		return requestProposalButton;
	}
	
	public WebElement getViewMapButton() {
		return viewMapButton;
	}
	
	public WebElement getFacebookButton() {
		return facebookButton;
	}
	
	public WebElement getTwitterButton() {
		return twitterButton;
	}
	
	public WebElement getNextButton() {
		return nextButton;
	}
	
	public WebElement getSpecialOffersButton() {
		return getElementByText(PHGConstants.HOTEL_INFO.SPECIAL_OFFERS);	
	}
	
	public WebElement getHotDatesButton() {
		return getElementByText(PHGConstants.HOTEL_INFO.HOT_DATES);	
	}
	
	public WebElement getNewsButton() {
		return getElementByText(PHGConstants.HOTEL_INFO.NEWS);	
	}
	
	public WebElement getPrintableButton() {
		return getElementByText(PHGConstants.HOTEL_INFO.PRINTABLE);	
	}
	
	public WebElement getPreviousButton() {
		return prevButton;
	}

    public void verifyHotelInfoDisplayed(String heroHotelTitle) {
		System.out.println("verifyHoteInfoDisplayed: " + heroHotelTitle + " / " + driver);
		WebElement propertyInfo = null;
		try {
			propertyInfo =  driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='property-title']")));
		} catch (StaleElementReferenceException se) {
			GlobalUtils.hardWait(1000);
			propertyInfo=  driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='property-title']")));
		}
		if (propertyInfo.isDisplayed()) {
			System.out.println("TODO: match the hotel title and the displayed hotel title");
			System.out.println("Hotel Info Title: " + propertyInfo.getText());
			//assertEquals(heroHotelTitle, propertyInfo.getText()); // hard to match text as web page displays different text
		}
    }

	public void verifyPreferredHotelMatches(String expetedTitle) {

		WebElement propertyInfo = null;
		try {
			propertyInfo =  driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='property-title']")));
		} catch (StaleElementReferenceException se) {
			GlobalUtils.hardWait(1000);
			propertyInfo=  driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='property-title']")));
		}
		if (propertyInfo.isDisplayed()) {
			System.out.println("TODO: match the hotel title and the displayed hotel title");
			System.out.println("featured Hotel Info Title: " + propertyInfo.getText());
			//assertEquals(heroHotelTitle, propertyInfo.getText()); // hard to match text as web page displays different text
		}
	}
}
