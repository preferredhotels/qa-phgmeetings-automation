package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = false,
        features = "src/test/resources/features",
        glue = "stepdefinitions",
        dryRun = false,
        plugin = {
                "pretty",
                "html:target/HtmlReports/PropertyDetailsReports.html",
                "json:target/JsonReports/PropertyDetailsReports.json",
                "junit:target/JUnitReport/PropertyDetailsReports.xml"
        },
       //tags = "@PDP6"
        tags = "@PDPsmoke"
)


public class TestPropertyDetailsTCRunner {

}
