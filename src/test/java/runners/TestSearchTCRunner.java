package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = false,
        features = "src/test/resources/features",
        glue = "stepdefinitions",
        dryRun = false,
        plugin = {
                "pretty",
                "html:target/HtmlReports/SearchReports.html",
                "json:target/JsonReports/pref_hotel.json",
                "junit:target/JUnitReport/pref_hotel.xml"
        },
       tags = "@searchsmoke"
       //tags= "@S17" // or @S14 or @S26"
)

public class TestSearchTCRunner {

}
