package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = false,
        features = "src/test/resources/features",
        glue = "stepdefinitions",
        //dryRun = false,
        plugin = {
                "pretty",
                "html:target/HtmlReports/HotDatesReports.html",
                "json:target/JsonReports/pref_hotel_hotdates.json",
                "junit:target/JUnitReport/pref_hotel_hotdates.xml"
        },
        tags = "@hotdatessmoke"
        //tags = "@HD3"// or @HD9"
)

public class TestHotDatesTCRunner {

}
