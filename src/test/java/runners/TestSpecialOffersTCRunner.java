package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = false,
        features = "src/test/resources/features",
        glue = "stepdefinitions",
        //dryRun = false,
        plugin = {
                "pretty",
                "html:target/HtmlReports/SpecialOffersReports.html",
                "json:target/JsonReports/pref_hotel_specialoffers.json",
                "junit:target/JUnitReport/pref_hotel_specialoffers.xml"
        },
        tags = "@specialofferssmoke"
      //tags = "@SP3" // or @SP3 or @SP4"
)

public class TestSpecialOffersTCRunner {

}
