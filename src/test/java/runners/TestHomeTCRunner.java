package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		monochrome = false,
		features = "src/test/resources/features", 
		glue = "stepdefinitions",
		plugin = {
				"pretty", 
				"html:target/HtmlReports/HomeReports.html",
				"json:target/JsonReports/HomeReports.json",
				"junit:target/JUnitReport/HomeReports.xml"
			},
		tags =  "@homesmoke"
		//tags = "@H10"
		//tags = "@H15"

		)

//tags = "@Regression,@JunitScenario,@TestngScenario" tags = "@smoke"

public class TestHomeTCRunner {

}
