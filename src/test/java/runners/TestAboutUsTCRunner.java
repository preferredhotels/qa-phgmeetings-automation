package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = false,
        features = "src/test/resources/features",
        glue = "stepdefinitions",
        plugin = {
                "pretty",
                "html:target/HtmlReports/AboutUsreports.html",
                "json:target/JsonReports/pref_hotel.json",
                "junit:target/JUnitReport/pref_hotel.xml"
                //default-report: FIG_NEWTON_FILE=default.yml --color --format html --out results/<%= Time.now.strftime("%Y%m%d-%H%M%S") %>-report.html
        },
        tags = "@aboutussmoke"
)

public class TestAboutUsTCRunner {

}
