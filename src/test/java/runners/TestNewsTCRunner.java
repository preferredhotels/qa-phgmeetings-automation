package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = false,
        features = "src/test/resources/features",
        glue = "stepdefinitions",
        plugin = {
                "pretty",
                "html:target/HtmlReports/NewsReports.html",
                "json:target/JsonReports/pref_hotel.json",
                "junit:target/JUnitReport/pref_hotel.xml"
        },
        tags =  "@newssmoke" //"@N5"
       //tags =  "@N6"
)

public class TestNewsTCRunner {

}
