package utils;

import com.google.common.base.Function;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GlobalUtils {

	public static WebElement fluentWaitForElement(WebDriver driver, final By locator) {
		//System.out.println("fluentwaitforelement");
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(Duration.ofSeconds(30))
				.pollingEvery(Duration.ofSeconds(5))
				.ignoring(NoSuchElementException.class);

		WebElement element = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				   return driver.findElement(locator);
			}
		});
		
		//System.out.println("return fluent wait element:" +element);
		return element;
	}

	public static boolean verifyimgActive(String img) {
		boolean valid=false;
		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpGet httpget = new HttpGet(img);
			HttpResponse httpresponse = httpclient.execute(httpget);
			//System.out.println("response code for image: " + httpresponse.getStatusLine().getStatusCode());
			if ( httpresponse.getStatusLine().getStatusCode() == 200) {
				//System.out.println(" Image found");
				valid = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valid;
	}

	public static boolean verifyDropdownElementExists(String pglement, WebDriverWait driverWait) {
		boolean elmtExists = false;
		try {
			hardWait(2000); // need to wait for the page to load, otherwise the following code will use the button on the previous page
			WebElement element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(pglement)));
			if (null != element) {
				elmtExists =  element.isDisplayed();
			}
		} catch (StaleElementReferenceException se) {
			hardWait(1000);
			WebElement element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(pglement)));
			if (null != element) {
				elmtExists =  element.isDisplayed();
			}
		}
		return elmtExists;
	}

	public static void hardWait(int timeOut) {
		try {
			Thread.sleep(timeOut);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static WebDriver focusElement(WebDriver driver, WebElement element){
		if (driver instanceof JavascriptExecutor) {
			((JavascriptExecutor)driver).executeScript("arguments[0].style.border='4px solid green'", element);
		}
		hardWait(1200);
		return driver;
	}
	public static WebDriver unFocusElement(WebDriver driver, WebElement element){
		if (driver instanceof JavascriptExecutor) {
			((JavascriptExecutor)driver).executeScript("arguments[0].style.border=''", element);
		}
		hardWait(1200);
		return driver;
	}
}
