package constants;

public class PHGConstants {

	// default page address
	private String pageAddress = PHG_HOME_PAGE.PROD;
	//private String pageAddress = PHG_HOME_PAGE.STAGE;
	//private String pageAddress = PHG_HOME_PAGE.QA;

	private static PHGConstants phgConstantsInstance = null;

	public PHGConstants() { }

	// Create a singleton
	public static PHGConstants getInstance() {
		if (phgConstantsInstance == null) {
			phgConstantsInstance = new PHGConstants();
		}
		return phgConstantsInstance;
	}

	// return page address set
	public String getPageAddress() {
		return pageAddress;
	}

	// used when user passes stage, qa, prod when running batch file
	public void setPageAddress(String pageAddress) {
		this.pageAddress = pageAddress;
	}

	public interface PHG_PAGE_TITLES {
		String HOME_PAGE_TITLE = "Preferred Meetings | PHG Meetings";
		String SEARCH_PAGE_TITLE = "New Search | PHG Meetings"; //"Preferred Meetings - Search";
		String SEARCH_RESULTS_TITLE = "Search Results | PHG Meetings";
		String ABOUT_US_PAGE_TITLE = "phg about us PHGMeetings.com | PHG Meetings";
		String PREFER_PLANNER_PAGE_TITLE = "Preferred Meetings - About Us";
		String SPECIALS_OFFER_PAGE_TITLE = "Preferred Meetings - Special Offers | PHG Meetings";
		String HOT_DATES_PAGE_TITLE = "Preferred Meetings - Hot Dates | PHG Meetings";
		String NEWS_PAGE_TITLE = "Preferred Meetings - News | PHG Meetings";
		String CONTACT_US_PAGE_TITLE = "phg contact us | PHG Meetings";
        String PDP_PAGE_TITLE = "| PHG Meetings | PHG Meetings";
        //String COOKIES_AND_PRIVACY_TITLE = "_PRIVACY_POLICY_ | PHG Meetings";
		String COOKIES_AND_PRIVACY_TITLE = "Preferred Travel Group Privacy Policy | PHG Meetings";
		String PROFILE_EDITOR = "Logged Out | PHG Meetings";

	}


	public interface PHG_HOME_PAGE {
		String STAGE = "https://stage.phgmeetings.com/";
		String PROD = "https://phgmeetings.com/";
		String QA = "https://qa.phgmeetings.com/";
	}
	
	public interface PHG_LINKS {
		String HOME_LINK_TEXT = "HOME";
		String NEW_SEARCH_LINK_TEXT = "SEARCH";
		String ABOUT_US_LINK_TEXT = "ABOUT US";
		String I_PREFER_PLANNER_LINK_TEXT = "I PREFER PLANNER";
		String SPECIAL_OFFERS_LINK_TEXT = "SPECIAL OFFERS";
		String HOT_DATES_LINK_TEXT = "HOT DATES";
		String NEWS_LINK_TEXT = "NEWS";
		//String CONTACT_US_LINK_TEXT = "CONTACT US";
		String CONTACT_US_LINK_TEXT = "Contact Us";
		String PRIVACY_LINK_TEXT = "Cookies & Privacy Policy";
		String PROFILE_EDITOR_LINK_TEXT = "Profile Editor";
	}
	
	public interface PHG_WIDTH_HEIGHT {
		int PAGE_WIDTH = 1908;
		int PAGE_HEIGHT = 1070;
	}
	
	public interface PHG_WAIT_ELEMENT {
		String HOME_PAGE_ELEMENT = "form_home";
		String SEARCH_PAGE_ELEMENT = "map_new_search";
		String START_NEW_SEARCH_ELEMENT = "//h1[contains(text(),'Search Results')]";
		String NEW_SEARCH_LINK_TEXT = "Search";
		String HERO_IMAGE_SELECTOR = "#property > a > img";
		String NEWS_PAGE_ELEMENT = "News";
		String CONTACTUS_PAGE_ELEMENT = "contact us";
	}
	
	public interface BY_MAP {
		String US_CANADA ="United States & Canada";
		String MEXICO_CARIBBEAN = "Mexico & Caribbean";
		String CENTRAL_SOUTH_AMERICA = "Central & South America";
		String EUROPE = "Europe";
		String MIDDLE_EAST_AFRICA = "Middle East & Africa";
		String ASIA_AUSTRALIA = "Asia & Australia";		
	}
	
	public interface HERO_IMAGE {
		String HERO_IMAGE_SELECTOR = "#property > a > img";
		String HERO_IMAGE_LINK = "homepage-masthead-link";
	}
	
	public interface HOTEL_INFO {
		String SPECIAL_OFFERS = "Special Offers";
		String HOT_DATES = "Hot Dates";
		String NEWS = "News";
		String PRINTABLE = "Printable";
	}
	

}
