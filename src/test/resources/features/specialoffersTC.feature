Feature: special offers functionality for PHG Meetings

  Background:
    Given user is on PHG Meetings page to view specialoffers
    And user clicks specialoffers link from header
    Then specialoffers page is displayed
    And verify specialoffers property results is displayed

  @specialofferssmoke @SP1
  Scenario: view specialoffers page fields
    And specialoffers page displays region dropdown
    And specialoffers page displays country dropdown
    And specialoffers page displays state dropdown
    And specialoffers page displays city dropdown
    And specialoffers page displays hotel dropdown
    And specialoffers page displays date dropdown
    And specialoffers page displays sort dropdown
    And specialoffers page displays search text box

  @specialofferssmoke @SP2
  Scenario: verify property details link and selection
    And link to specialoffers property details is displayed
    And user selects specialoffers property details link
    Then specialoffers property details page is displayed

  @specialofferssmoke @SP3
  Scenario: verify property name link and selection
    And link to specialoffers property name is displayed
    And user selects specialoffers property name link
    Then specialoffers property details page is displayed

  @specialofferssmoke  @SP4
  Scenario: verify property image link and selection
    And link to specialoffers property image is displayed
    And user selects specialoffers property image link
    Then specialoffers property details page is displayed

  @specialofferssmoke @SP5
  Scenario Outline: search specialoffers properties by region
    And select <region> region from specialoffers region dropdown
    And verify specialoffers property results is displayed

    Examples:
      | region |
      | Europe |

  @specialofferssmoke @SP6
  Scenario Outline: search specialoffers properties by country
    And select <country> country from specialoffers country dropdown
    And verify specialoffers property results is displayed

    Examples:
      | country |
      | United Kingdom |

  @specialofferssmoke @SP7
  Scenario Outline: search specialoffers properties by state
    And select <state> state from specialoffers state dropdown
    And verify specialoffers property results is displayed

    Examples:
      | state |
      | California |

  @specialofferssmoke @SP8
  Scenario Outline: search specialoffers properties by city
    And select <city> city from specialoffers city dropdown
    And verify specialoffers property results is displayed

    Examples:
      | city |
      | San Diego |

  @specialofferssmoke @SP9
  Scenario Outline: search specialoffers properties by hotel name
    And select <hotelname> hotelname from specialoffers hotelname dropdown
    And verify specialoffers property results is displayed

    Examples:
      | hotelname |
      | Hotel Du Pont |

  @specialofferssmoke @SP10
  Scenario Outline: search specialoffers properties by date
    And select <date> date from specialoffers date dropdown
    And verify specialoffers property results is displayed

    Examples:
      | date |
      | Upcoming Year |

  @specialofferssmoke  @SP11
  Scenario Outline: search specialoffers properties by free text
    And enter <freetext> freetext to filter specialoffers
    And verify specialoffers property results is displayed

    Examples:
      | freetext |
      | hotel    |

  @specialofferssmoke @SP12
  Scenario Outline: sort hotdates properties by value
    And sort specialoffers by <hotelname_sort> value
    And verify specialoffers property results is displayed

    Examples:
      | hotelname_sort |
      | Destination |
      | Sort Date |
