Feature: Home Page functionality for PHG Meetings

  Background:
   Given user is on PHG Meetings page to test home page
   And user clicks home link from header
   Then home page is displayed

  @homesmoke @H1
  Scenario: Find All Hotels
    When user clicks on search button
    Then search results is displayed

  @homesmoke @H2
  Scenario Outline: Find specific hotel
    When user enters <hotelname> to search
    And user clicks on search button
    Then search results with <hotelname> is displayed

    Examples:
      | hotelname |
      | mumbai    |
      | canada    |
      | china     |

  @homesmoke @H3
  Scenario Outline: Find hotels by number of guest rooms
    When user chooses <numberofrooms> in Guest Rooms list box
    And user clicks on search button for guest rooms
    Then minimum guest rooms results with text <searchType> and <numberofrooms> is displayed

    Examples:
       | searchType | numberofrooms |
      | Minimum Guest Rooms: | 500+ |

  @homesmoke @H4
  Scenario Outline: Find hotels by meeting room in square foot
    When user chooses <squarefoot> in meeting room list box
    And user clicks on search button for room size
    Then largest meeting room results with text <searchType> and <squarefoot> is displayed

	Examples:
	 | searchType | squarefoot |
	 | Largest Meeting Room: | 5,000+ |

  @homesmoke @H5
  Scenario Outline: Find hotels by meeting room in meters
    When user clicks on switch to meters link
    And user selects <squaremeters> in meeting room list box
    And user clicks on search button for room in meters
    Then largest meeting room in meters with text <searchType> and <squaremeters> is displayed

	Examples:
      | searchType | squaremeters |
	  | Largest Meeting Room: | 1500+ |

  @homesmoke @H6
  Scenario Outline: Find hotels by using map
    When user search by map feature is visible
    Then user clicks on search by map
    And verify all the world map images are displayed
    And user clicks on the <selectedmap> image
    Then search results by <selectedmap> is displayed

	Examples:
		| selectedmap |
		| North America |

  @homesmoke @H7
  Scenario: Home page displays a hero hotel image
    Then verify hero hotel image is displayed

  @homesmoke @H8
  Scenario: Display hotel information of the hero hotel
    When user clicks the hero hotel image
    Then verify hotel information displayed matches hero hotel

  @homesmoke @H9
  Scenario: Home page displays three preferred property images
    And verify three preferred property images are displayed
    And verify the images are not broken

  @homesmoke @H10
  Scenario: Display preferred hotel information
    And user clicks on preferred hotel image
    And verify hotel information displayed matches preferred hotel

  @homesmoke @H11
  Scenario: Top destinations section is displayed
    And verify top destinations is displayed

  @homesmoke @H12
  Scenario Outline: Display all hotels in selected Top destination
    When user clicks on <destinationcity> as top destination
    Then verify preferred hotels in top destination is displayed

    Examples:
      | destinationcity |
      | London          |

  @homesmoke @H13
  Scenario: Display PHG meetings page on click of preferred meetings
    When user clicks on preferred meetings link on top of page
    Then verify the web page link display phgmeetings

  @homesmoke @H14
  Scenario: Welcome section is displayed in home page
    Then welcome section is displayed

  @homesmoke @H15
  Scenario: Display Admin login page
    When user clicks on the profile editor link
    Then verify admin login page is displayed
