Feature: hotdates Page functionality for PHG Meetings

  Background:
    Given user is on PHG Meetings page to view hotdates
    And user clicks hotdates link from header
    Then hotdates page is displayed
    And verify hotdates property results is displayed

  @hotdatessmoke @HD1
  Scenario: view hotdates page fields
    And hotdates page displays region dropdown
    And hotdates page displays country dropdown
    And hotdates page displays state dropdown
    And hotdates page displays city dropdown
    And hotdates page displays hotel dropdown
    And hotdates page displays date dropdown
    And hotdates page displays sort dropdown
    And hotdates page displays search text box

  @hotdatessmoke @HD2
  Scenario: verify property details link and selection
    And link to property details is displayed
    And user selects property details link
    Then hotdates property details page is displayed

  @hotdatessmoke @HD3
  Scenario: verify property name link and selection
    And link to property name is displayed
    And user selects property name link
    Then hotdates property details page is displayed

  @hotdatessmoke @HD4
  Scenario: verify property image link and selection
    And link to property image is displayed
    And user selects property image link
    Then hotdates property details page is displayed

  @hotdatessmoke @HD5
  Scenario Outline: search hotdates properties by region
    And select <region> region from region dropdown
    And verify hotdates property results is displayed

    Examples:
    | region |
    | North America |

  @hotdatessmoke @HD6
  Scenario Outline: search hotdates properties by country
    And select <country> country from country dropdown
    And verify hotdates property results is displayed

    Examples:
      | country |
      | United States |

  @hotdatessmoke @HD7
  Scenario Outline: search hotdates properties by state
    And select <state> state from state dropdown
    And verify hotdates property results is displayed

    Examples:
      | state |
      | California |

  @hotdatessmoke @HD8
  Scenario Outline: search hotdates properties by city
    And select <city> city from city dropdown
    And verify hotdates property results is displayed

    Examples:
      | city |
      | San Diego |

  @hotdatessmoke @HD9
  Scenario Outline: search hotdates properties by hotel name
    And select <hotelname> hotelname from hotelname dropdown
    And verify hotdates property results is displayed

    Examples:
      | hotelname |
      | Hotel Du Pont |

  @hotdatessmoke @HD10
  Scenario Outline: search hotdates properties by date
    And select <date> date from date dropdown
    And verify hotdates property results is displayed

    Examples:
      | date |
      | Upcoming Year |

  @hotdatessmoke @HD11
  Scenario Outline: search hotdates properties by free text
    And enter <freetext> freetext to filter hotdates
    And verify hotdates property results is displayed

    Examples:
    | freetext |
    | hotel    |

  @hotdatessmoke @HD12
  Scenario Outline: sort hotedates properties by value
    And sort hotdates by <hotelname_sort> value
    And verify hotdates property results is displayed

    Examples:
    | hotelname_sort |
    | Hotel Name A-Z |
    | Upcoming Dates |
