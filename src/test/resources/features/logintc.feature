# Author - RAlugunti
#Feature: Login funcionality for PHG Meetings
#  User has to be able to login to PHG Meetings website.

  #Background: 
    #Given browser is open
    #And user is on the login page for PHG Meetings
#
  #@smoke
  #Scenario: Login Page is displayed
    #Then login page has email textbox
    #And login page has password textbox
#
  #@smoke
  #Scenario: Remember Me checkbox is displayed
    #Then login page has optional remember me checkbox
#
  #@smoke
  #Scenario: Submit button is displayed
    #Then Submit button is displayed
#
  #@smoke
  #Scenario: Forgot Password is displayed
    #Then forgot Password Link is displayed
#
  #@smoke
  #Scenario Outline: Check Login is Successful with Valid credentials
    #When user enters <username>
    #And user enters <password>
    #And user clicks on the Login button
    #Then I verify the <status> in step
    #

    #Examples: 
      #| username                  | password | status  |
      #| ralugunti@preferredhotels | xxxxxx   | success |
      #| admin2@phgdev.com         | xxxxxx   | success |
#
  #@smoke
  #Scenario: unsuccessful login with invalid user name and valid password
    #
    #When user enters "Email" with "admin2@phgdev.com"
    #And user enters "password" with "abcdef"
    #And user clicks on the Login button
    #Then error message displayed
    #And user returns back on login page
#
  #@smoke
  #Scenario: unsuccessful login with valid user name and invalid password
    #When user enters "Email" with "admin2@phgdev.com"
    #And user enters "password" with "abcdef"
    #And user clicks on the Login button
    #Then error message displayed
    #And user returns back on login page
#
  #@smoke
  #Scenario: Forgot Password link is clickable
    #When user clicks on forgot passsword link
    #Then user is redirected to the retrieve password page
