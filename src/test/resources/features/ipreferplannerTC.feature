Feature: I Prefer Planner Page functionality for PHG Meetings

  Background:
    Given user is on PHG Meetings page to test iPreferPlanner
    And user clicks I prefer planner link from header

  @prefersmoke @P1
  Scenario: view I Prefer Planner page
    Then I Prefer Planner page is displayed

  @prefersmoke @P2
  Scenario: view I Prefer Planner page fields
    And I Prefer Planner page displays an image
    And I Prefer Planner page displays rewards section
    And rewards section body text is displayed
    And I Prefer Planner displays recognition section
    And recognition section body text is displayed
    And I Prefer Planner displays membership section
    And Membership section body text is displayed





