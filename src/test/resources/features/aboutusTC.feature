Feature: aboutus Page functionality for PHG Meetings

  Background:
    Given user is on PHG Meetings page to test aboutus
    And user clicks AboutUs link from header
    Then about us page is displayed

  @aboutussmoke @A1
  Scenario: view  About Us page logos and content
    And  aboutus header is displayed
    And  preferred hotels logo and text is displayed
    And  legend logo and text is displayed
    And  lvx logo and text is displayed
    And  lifestyle logo and text is displayed
    And  connect logo and text is displayed
    And  residences logo and text is displayed



