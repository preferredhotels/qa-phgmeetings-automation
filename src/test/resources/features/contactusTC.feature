Feature: contact tus Page functionality for PHG Meetings

  Background:
    Given user is on PHG Meetings page to test contactus
    And user clicks on contactus link  from header
    Then contactus page is displayed

  @contactussmoke @C1
  Scenario: view contactus page
    And leadership section is present
    And Sales Directors Europe is present
    And Sales Directors North America is present
    And Sales Directors Asia Pacific Middle East is present
    And Group Sales Support Asia Pacific Europe & North America is present
    And emails are present
    And phone numbers are present

