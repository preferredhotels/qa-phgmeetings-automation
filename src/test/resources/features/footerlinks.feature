Feature: footer links functionality for PHG Meetings

  Background:
    Given browser is open for search page test
    And user is on PHG Meetings page

  Scenario: footer links are displayed
    When user is on the page
    Then preferred hotels logo is displayed
    And  beyond green logo is displayed
    And  preferred hotels address is displayed
    And  ph phone number link is present
    And  cookies and privacy policy link is present
    And  profile editor link is present
    And  do not sell my personal information link is present


