Feature: property details page

  Background:
    Given user is on PHG Meetings page to view PDP
    And user clicks on hotdates link from header
    And user selects property details link
    Then property details page is displayed

  @PDPsmoke @PDP1
  Scenario: property details info
    When user is on property details page
    Then page contains hotel demographics
    And image carousel is being displayed
    And page contains at a glance section
    And page contains hotel description
    And page displays airport proximity
    And page displays social media links
    And page displays amenities
    And page displays dining info
    And page displays meeting space info
    And page displays latest news
    And page displays footer links section

  @PDPsmoke  @PDP2
  Scenario: property demographics section
    Then hotel name is displayed in bold
    And displays hotel address
    And page displays request proposal link

  @PDPsmoke @PDP3
  Scenario: at a glance section
    Then contains a logo
    And contains transportation section
    And social media logos or links

  @PDPsmoke @PDP4
  Scenario: hotel description section
    Then contains three links
    And first sentence is bold
    And page contains map view of hotel

  @PDPsmoke @PDP5
  Scenario: Footer links section
    And footer links displays logos
   # And footer links displays address
    # And footer links displays phone number
    And footer links displays cookies and privacy link
    And footer links displays profile editor link
    And footer links displays personal info link

  @PDPsmoke  @PDP6
  Scenario: Request Proposal page
    When user clicks on request proposal button
    Then user lands on request proposal page
    And hotel name is prepopulated
    And  user enters the form details and submits the form
