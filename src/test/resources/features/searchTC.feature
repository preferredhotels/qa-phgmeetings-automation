#Author: ralugunti@preferredhotels.com
Feature: search Page functionality for PHG Meetings

  Background:
    Given user is on PHG Meetings page to begin search
    And click search link from header
    Then search page is displayed

  @searchsmoke @S1
  Scenario: view search page fields
    Then search page displays fields to enter search criteria

  @searchsmoke @S2
  Scenario Outline: Find specific hotel in keyword search
    When user enters <locationorhotelname> location or hotelname in keyword search
    And click search button in keyword search section
    Then search results page has <locationorhotelname> location or hotelname match
#
    Examples:
      | locationorhotelname |
      | mumbai    |

  @searchsmoke @S3
  Scenario Outline: search by number of guest rooms in keyword search
    When user selects <numguestrooms> number of rooms from guestrooms dropdown
    And click search button in keyword search section
    Then search results page is displayed
#
    Examples:
      | numguestrooms |
      | 500+    |

  @searchsmoke @S4
  Scenario Outline: search by largest meeting room in keyword search
    When user selects <roomsize> room size from largest room dropdown
    And click search button in keyword search section
    Then search results page is displayed
#
    Examples:
      | roomsize |
      | 5,000+   |

  @searchsmoke @S5
  Scenario Outline: search by minimum ceiling height meeting in keyword search
    When user selects <ceilingheight> from ceiling height dropdown
    And click search button in keyword search section
    Then search results page is displayed
#
    Examples:
      | ceilingheight |
      | 20 feet    |

  @searchsmoke @S6
  Scenario Outline: search by amenity in keyword search
    When user enters <amenity> in amenity text search
    And click search button in keyword search section
    Then search results page is displayed
#
    Examples:
      | amenity |
      | golf,beach   |

  @searchsmoke @S7
  Scenario: Map Results link is displayed
    When click search button in keyword search section
    Then search results page is displayed
    And map results button is visible
    And every property has a map link that is clickable

##

  @searchsmoke @S8
  Scenario: Map Results View is displayed
    When click search button in keyword search section
    Then search results page is displayed
    And map results button is visible
    And user clicks on map results button
    Then map view is displayed

#
  @searchsmoke @S9
  Scenario: Satellite Map View is displayed
    When click search button in keyword search section
    Then search results page is displayed
    And map results button is visible
    And user clicks on map results button
    Then map view is displayed
    And user clicks on street map button
    Then street map view is displayed

#
  @searchsmoke @S10
  Scenario: Street Map View is displayed
    When click search button in keyword search section
    Then search results page is displayed
    And map results button is visible
    And user clicks on map results button
    Then map view is displayed
    And user clicks on satellite button
    Then satellite map view is displayed

#
  @searchsmoke @S11
  Scenario: Display search results in square meters
    When click search button in keyword search section
    Then search results page is displayed
    And user click view in meters button in results section
    Then properties are listed with details in square meters

#
  @searchsmoke @S12
  Scenario Outline: Display search results sorted by city name
    When click search button in keyword search section
    Then search results page is displayed
    And user selects <sortoption> city from the dropdown
    Examples:
      | sortoption |
      | City |

#
  @searchsmoke @S13
  Scenario Outline: Display search results sorted by country name
    When click search button in keyword search section
    Then search results page is displayed
    And user selects <sortoption> country from the dropdown
    Examples:
      | sortoption |
      | Country/Region |

#
  @searchsmoke @S14
  Scenario Outline: Display search results sorted by number of rooms high to low
    When click search button in keyword search section
    Then search results page is displayed
    And user selects <sortoption> number of rooms high to low from the dropdown

    Examples:
      | sortoption |
      | Number of Rooms (high-low) |

#
  @searchsmoke @S15
  Scenario Outline: Display search results sorted by number of rooms low to high
    When click search button in keyword search section
    Then search results page is displayed
    And user selects <sortoption> number of rooms low to high from the dropdown

    Examples:
      | sortoption |
      | Number of Rooms (low-high) |

#
  @searchsmoke @S16
  Scenario Outline: Display search results sorted by largest meeting rooms high to low
    When click search button in keyword search section
    Then search results page is displayed
    And user selects <sortoption> largest meeting rooms high to low from the dropdown

    Examples:
      | sortoption |
      | Largest Meeting Room (high-low) |

  @searchsmoke @S17
  Scenario Outline: Display search result sorted by largest meeting rooms low to high
    When click search button in keyword search section
    Then search results page is displayed
    And user selects <sortoption> largest meeting rooms low to high from the dropdown

    Examples:
      | sortoption |
      | Largest Meeting Room (low-high) |

  @searchsmoke @S18
  Scenario Outline: Display search results sorted by best match
    When click search button in keyword search section
    Then search results page is displayed
    And user selects <sortoption> best match from the dropdown

    Examples:
      | sortoption |
      | Best Match |

  @searchsmoke @S19
  Scenario Outline: Display search results sorted by property
    When click search button in keyword search section
    Then search results page is displayed
    And user selects <sortoption> property from the dropdown

    Examples:
      | sortoption |
      | Property |

  @searchsmoke @S20
  Scenario: Display request proposal page
    When click search button in keyword search section
    Then search results page is displayed
    And user clicks on the request proposal button
    Then request proposal page is displayed

  #could enhance to select the first property result
  #scroll down to the amenities
  #check if golf is one of the amenities in the available list

  @searchsmoke @S21
  Scenario Outline: Filter search results by feature type
    When click search button in keyword search section
    Then search results page is displayed
    And user selects <featuretype> feature type in refine search

    Examples:
      | featuretype  |
      | Golf  |

  @searchsmoke @S22
  Scenario: Refine search results by multiple feature type
    When click search button in keyword search section
    Then search results page is displayed
    And user selects 'Golf' and 'Spa' features in refine search

  @searchsmoke @S23
  Scenario: Refine search results by number of guest rooms
    When click search button in keyword search section
    Then search results page is displayed
    And user selects number of guest rooms in refine search

  @searchsmoke @S24
  Scenario: Refine search results by largest meeting room
    When click search button in keyword search section
    Then search results page is displayed
    And user selects largest meeting room size in refine search

  @searchsmoke @S25
  Scenario: Refine search results by minimum ceiling height
    When click search button in keyword search section
    Then search results page is displayed
    And user selects ceiling height in refine search

  @searchsmoke @S26
  Scenario: Refine search results by region
    When click search button in keyword search section
    Then search results page is displayed
    And user selects region in refine search

