Feature: news Page functionality for PHG Meetings

  Background:
    Given user is on PHG Meetings page to view hotel news
    And user clicks on news link from header
    Then news page is displayed

  @newssmoke @N1
  Scenario: view news page fields
    And news page displays date filter
    And news page displays hotel filter
    And news page displays search text field

  @newssmoke @N2
  Scenario: News page shall display hotel details
    And news page displays hotel image
    And news page displays hotel news headings
    And news page displays link to the hotel news

  @newssmoke @N3
  Scenario: date filter functionality
    When user selects option in the date filter dropdown
    Then news page displays results by date

  @newssmoke @N4
  Scenario: hotel filter functionality
    When user selects option in the hotel filter dropdown
    Then news page displays results by hotel

  @newssmoke @N5
  Scenario: search box functionality
    When user enters search text in search box
    Then news page displays results by search text

  @newssmoke @N6
  Scenario Outline: news page functionality
    When user chooses <selected_date> last six months from date filter
    And  user chooses <selected_hotel> from hotel filter
    Then displays hotel news

    Examples:
      | selected_date | selected_hotel |
      | Last Twelve Months |The Benson Hotel & Faculty Club|
  @newssmoke @N7
  Scenario: verify property news is displayed
    When user selects link to the hotel news
    Then property page is displayed with news section
