#Author: ralugunti@preferredhotels.com
#Feature: Keyword Search Page functionality for PHG Meetings
#
#  Background:
#    Given browser is open
#    And user is on PHG Meetings page
#    And Click search link from header
#
#   Scenario: Search page fields are displayed
#    When Click Search link from header
#    Then Search page displays Enter location or hotel name text box is editable
#    And  view in feet and meteres toggles
#    And  number of guest rooms drop down is selectable
#    And  Largest meeting room drop down is present
#    And miminum ceiling height is present
#    And check boxes for golf spa beachski center exist
#    And text box for activity amenity is present
#    And search button is present
#
#   Scenario: click with empty search o keyword search page redirects to refine search page
#   	When user clicks search button on the keyword search page
#   	Then user is take to the refine search page
#
#   Scenario: Map search is responsive
#   	When user hovers on the map icons
#   	Then map icons are interactive
#
#   Scenario: Map search North America
#   	When user clicks on the north america map icon
#   	Then properties in northa america are displayed
#
#   Scenario: Map search Europe
#   	When user clicks on the Europe map icon
#   	Then properties in Europe are displayed
#
#   Scenario: Map search central and south america
#   	When user clicks on the central and south america map icon
#   	Then properties in central and south america are displayed
#
#   	Scenario: Map search carribean
#   	When user clicks on the carribean map icon
#   	Then properties in carribean are displayed
#
#   Scenario: Map search middle east
#   	When user clicks on the middle east map icon
#   	Then properties in middle east are displayed
#
#   Scenario: Map search africa
#   	When user clicks on the africa map icon
#   	Then properties in africa are displayed
#
#   Scenario: Map search asia and pacific
#   	When user clicks on the asia and pacific map icon
#   	Then properties in asia and pacific are displayed
#
#
#
#
#
#