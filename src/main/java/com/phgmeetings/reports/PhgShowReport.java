package com.phgmeetings.reports;

import org.apache.http.util.TextUtils;
import java.awt.*;
import java.net.URI;
import java.net.URL;

public class PhgShowReport {

    public PhgShowReport() { }

    public static void main(String[] args) {
        PhgShowReport phgShowReport = new PhgShowReport();
        String reportName = "home";
        if (args != null) {
            if (args.length > 0) {
                reportName = args[0];
                System.out.println("Report name: " + reportName);
            }
        }
        System.out.println("Before call showTestReport");
        phgShowReport.showTestReport(reportName);
    }

    public void showTestReport(String reportName) {
        System.out.println("user dir " + System. getProperty("user.dir"));
        String url = "file:\\\\" + System. getProperty("user.dir") + "\\target\\HtmlReports\\";
        if (!TextUtils.isEmpty(reportName)) {
            try {
                switch (reportName) {
                    case "aboutus":
                        url = url + "AboutUsreports.html";
                        break;
                    case "contactus":
                        url = url + "ContactUsreports.html";
                        break;
                    case "hotdates":
                        url = url + "HotDatesReports.html";
                        break;
                    case "news":
                        url = url + "NewsReports.html";
                        break;
                    case "pdp":
                        url = url + "PDPReports.html";
                        break;
                    case "preferplanner":
                        url = url + "PreferPlannerReports.html";
                        break;
                    case "search":
                        url = url + "SearchReports.html";
                        break;
                    case "specialoffers":
                        url = url + "SpecialOffersReports.html";
                        break;
                    case "home":
                    default:
                        url = url + "HomeReports.html";
                        break;
                }
                System.out.println("url is: " + url);
                URI uri = new URL(url).toURI();
                Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE))
                    desktop.browse(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
