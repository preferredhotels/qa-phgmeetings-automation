echo "================================================"
echo "    Start Home page test cases .....            "
echo "================================================"
set PATH=%PATH%;C:\Users\ralugunti\apache-maven-3.8.4\bin
set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_281
echo "================================================"
echo "Running About Us page test cases on Chrome browser.."
echo "================================================"
call runAboutUsPage-Chrome.bat
echo "================================================"
echo "Running Contact Us page test cases on Chrome browser.."
echo "================================================"
call runContactUsPage-Chrome.bat
echo "================================================"
echo "Running Home page test cases on Chrome browser.."
echo "================================================"
call runHomePage-Chrome.bat
echo "================================================"
echo "Running Hot Dates test cases on Chrome browser.."
echo "================================================"
call runHotdatesPage-Chrome.bat
echo "================================================"
echo "Running news page test cases on Chrome browser.."
echo "================================================"
call runNewsPage-Chrome.bat
echo "================================================"
echo "Running Property Details page test cases on Chrome browser.."
echo "================================================"
call runPropertyDetailsPage-Chrome.bat
echo "================================================"
echo "Running Prefer Planner page test cases on Chrome browser.."
echo "================================================"
call runPreferPlannerPage-Chrome.bat
echo "================================================"
echo "Running Search page test cases on Chrome browser.."
echo "================================================"
call runSearchPage-Chrome.bat
echo "================================================"
echo "Running Special Offers page test cases on Chrome browser.."
echo "================================================"
call runSpecialOffersPage-Chrome.bat
echo "================================================"
echo "  All CHROME Test Cases completed      "
echo "================================================"
echo "Running About Us page test cases on Edge browser...."
echo "================================================"
call runAboutUsPage-Edge.bat
echo "================================================"
echo "Running Home page test cases on Edge browser...."
echo "================================================"
call runHomePage-Edge.bat
echo "================================================"
echo "Running Contact Us page test cases on Edge browser...."
echo "================================================"
call runContactUsPage-Edge.bat
echo "================================================"
echo "Running Hot Dates test cases on Edge browser...."
echo "================================================"
call runHotdatesPage-Edge.bat
echo "================================================"
echo "Running news page test cases on Edge browser...."
echo "================================================"
call runNewsPage-Edge.bat
echo "================================================"
echo "Running Property Details page test cases on Edge browser...."
echo "================================================"
call runPropertyDetailsPage-Edge.bat
echo "================================================"
echo "Running Prefer Planner page test cases on Edge browser...."
echo "================================================"
call runPreferPlannerPage-Edge.bat
echo "================================================"
echo "Running Search page test cases on Edge browser...."
echo "================================================"
call runSearchPage-Edge.bat
echo "================================================"
echo "Running Special Offers page test cases on Edge browser...."
echo "================================================"
call runSpecialOffersPage-Edge.bat
echo "================================================"
echo "  All EDGE Test Cases completed      "
echo "================================================"
