**PHGMeetings Automation Script Usage**

**Get latest code from Bitbucket repository**
1. Open the IntellijIDE and open the PHGMeetings project
2. From the menu, click on "Git" > Fetch
3. The project will be updated with the latest code
![img_1.png](img_1.png)

**Change the target site to be tested QA / STAGE / PRODUCTION**   
From the project root, navigate to **\qa-phgmeetings-automation\src\test\java\constants\phgconstants.java**
![img_2.png](img_2.png)

**Change browser selection AND toggle between native browser and Browserstack**   
qa-phgmeetings-automation\src\test\java\pagefactory\TestBase.java
![img_3.png](img_3.png)

**Update Drivers**
1. Chrome / Edge Driver update:
   1. Download the latest driver to match the browser ![img_6.png](img_6.png) ![img_7.png](img_7.png)
   2. Delete the existing Driver from location ![img_5.png](img_5.png)
   3. Extract the zip file to the above location



**Running Tests**

1. From the Intellij studio: From the project root, navigate to **\qa-phgmeetings-automation\src\test\java\runners
2. Right click on any of the runner file and select the green arrow Run
   ![img.png](img.png)
**Running test using batch files**

1. Note the user can now run the batch scripts from command line of the project by passing the following 3 parameters
2. 1st parameter is the browser name. Valid values are **ios**, **android**, **chrome**, **edge**, **safari**
3. 2nd parameter is **true** or **false**. That is whether the tests can be run on browserstack
4. 3rd parameter is environment where the test must be run. Valid values are **prod**, **qa**, **stage**.

**REPORTS:**
qa-phgmeetings-automation\target\HtmlReports
**Usage**

Change directory to the root folder of the project and run the following command. For e.g.
to run tests related to NewsPage...

**runNewsPageTests.bat ios false prod**

**runAboutUsPageTests.bat**

**runAllTests.bat**   ( To run all test on both Chrome and Edge sequentially. First Chrome and second Edge)

**runContactUsPageTests.bat**

**runHomePageTests.bat**

**runHotdatesPageTests.bat**

**runIPreferPlannerPageTests.bat**

**runNewsPageTests.bat**

**runPropertyDetailsPageTests.bat**

**runSearchPageTests.bat**

**runSpecialOffersPageTests.bat**

**Running the Report**

List of batch Files:
![img_4.png](img_4.png)

Once the tests are complete the user can launch a report to view the test results.

**Usage**

Change directory to the root folder of the project and run the following command. For e.g.
to run test results report for aboutus...

**showReport.bat aboutus**

**showReport.bat**

Following are valid report names for **phgmeetings**

**aboutus**

**contactus**

**hotdates**

**news**

**pdp**

**preferplanner**

**search**

**specialoffers**

**home**

