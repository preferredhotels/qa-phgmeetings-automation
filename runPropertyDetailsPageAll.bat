echo "================================================"
echo "    Start Property Details page test cases .....            "
echo "================================================"
set PATH=%PATH%;C:\Users\ralugunti\apache-maven-3.8.4\bin
set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_281
echo "================================================"
echo "Running Property Details page test cases on Chrome browser.."
echo "================================================"
call runPropertyDetailsPage-Chrome.bat
echo "================================================"
echo "Running Property Details page test cases on Edge browser...."
echo "================================================"
call runPropertyDetailsPage-Edge.bat
echo "================================================"
echo "            Property Details Page Test Cases completed      "
echo "================================================"
