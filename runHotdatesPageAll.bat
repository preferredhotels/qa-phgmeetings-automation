echo "================================================"
echo "    Start Hot Dates page test cases .....            "
echo "================================================"
set PATH=%PATH%;C:\Users\ralugunti\apache-maven-3.8.4\bin
set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_281
echo "================================================"
echo "Running Hot Dates test cases on Chrome browser.."
echo "================================================"
call runHotdatesPage-Chrome.bat
echo "================================================"
echo "Running Hot Dates test cases on Edge browser...."
echo "================================================"
call runHotdatesPage-Edge.bat
echo "================================================"
echo "            Hot Dates Test Cases completed      "
echo "================================================"
